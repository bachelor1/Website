This compendium is taken from Erik Hjelmås' DCSG1005 course.

## About this Compendium

TBA, huskeliste for now:

  - Textbook for the course DCSG1005 infrastructure: Secure Core
    Services

  - Focus is sysadm and security of company internal Windows-based
    networks

  - Alignment with NSM

  - The slides I use in lectures are the figures in this compendium.

  - Thanks to Jan, Peder, Tor Ivar, Ernst, Wikimedia-figures

  - Sequence of chapters and dependencies

  - Key problems are highly relevant for exams

  - Read chapter, do lab tutorial, exercises

  - I assume PowerShell throughout so increasing difficulty level in
    chapter code examples

  - Exam questions will assume you have done all the practical exercises
    (this is captured in some of the key problems as well)

  - License?

# Introduction: OpenStack and Windows Server

The goal of this course is for you to primarily learn two topics:
*Windows infrastructure: theory and practice* and *How to get the basics
right*. Based on my gut feeling I claim that 90% of servers you use on
the Internet are Linux-servers while 90% of important servers on the
internal networks of companies are Windows-servers. These percentages
are of course not accurate, but my point is that most organizations who
have their own IT-infrastructure base it on a Windows infrastructure
with Active Directory at its core. Modern IT-infrastructures are
commonly implemented in private or public clouds, so we need to start
out by learning a bit about cloud computing as well. We will soon move
on the "Getting the basics right" which in our context (which can be
considered a technical subcontext of NSM’s "Grunnprinsipper for
IKT-sikkerhet 2.0" ) means studying how to do the following in a
cloud-based Windows infrastructure:

  - Backup and restore

  - Identity management and access control

  - Configuration management

  - Software package management

  - Logging and monitoring

  - Fast (automatic) redeploy/installation/provisioning

## Cloud Computing

IaaS, PaaS, SaaS in
figure [1.1](#fig:intro:9d89f7005cdc4604ab7b4675f3db6fcb).

![IaaS, PaaS,
SaaS.<span label="fig:intro:9d89f7005cdc4604ab7b4675f3db6fcb"></span>](/home/erikhje/office/kurs/secsrv/01-intro/tex/../img/Cloud_computing.pdf)  
<span>[CC-BY-SA-3.0](https://creativecommons.org/licenses/by-sa/3.0) By
Sam Johnston</span>

Cloud computing is about offering computing services on the Internet
without us knowing exactly which server is offering the service (it’s
just somewhere in "the cloud"). Cloud computing services were offered
firstly by Amazon Web Services (IaaS) in 2006, by Google App Engine in
2008 (PaaS) and Microsoft Azure in 2010, and in general we categorize
the services as

  - IaaS  
    Infrastructure as a Service: compute (virtual machines), network and
    storage

  - PaaS  
    Platform as a Service: run-time environments (e.g. Kubernetes),
    databases, web servers, object storage (files access over http, not
    "virtual hard disk" like in IaaS)

  - SaaS  
    Software as a Service: e.g. email (Gmail), monitoring (Grafana
    cloud), LMS (Blackboard Learn SaaS)

Sometimes you will see the term *serverless computing* which means that
a cloud computing customer does not manage or directly use any virtual
machines, the customer only uses services that mostly are categorized as
PaaS. When a cloud computing customer makes full use of modern cloud
computing and uses all accompanying modern software development and
management practices and processes (Agile, DevOps, DevSecOps, etc), the
customer is said to be *cloud native* . The "opposite" of being cloud
native is when you just copy your on-premise data center into the cloud
(just replicating your network and servers with the same setup of
services without taking true advantage of what the cloud offers).

### Characteristics

Cloud Computing Characteristics in
figure [\[fig:intro:157fbb521cea4ed7acb25ac2186d371e\]](#fig:intro:157fbb521cea4ed7acb25ac2186d371e).

  - Dynamic  
    you can create and destroy/delete infrastructures as needed

  - Self-service  
    you manage everything yourself in software (instead of buying
    hardware)

  - Pay-as-you-go  
    you only pay for the resources you need and use

Cloud computing is based on virtualization and shared resources (shared
physical servers, networks and storage). The key characteristics of
cloud computing that have lead to this paradigm shift in information
technology is dynamic, self-service and pay-as-you-go.

### Security and Privacy

There is not much special about security and privacy when moving to the
cloud. Mostly it is the same concerns as when you are running your
IT-systems on-premise, but many times you have to pay particular
attention to where you end up storing data due to legal reasons (are you
allowed to store your data in the country where the cloud computing
servers are located?).

Security and Privacy in
figure [\[fig:intro:33c7a5359d1b4bd988daa7aa0cf956f1\]](#fig:intro:33c7a5359d1b4bd988daa7aa0cf956f1).

  - Privacy: storing data on someone else’s computers

  - All issues related to outsourcing (possibly loosing control)

  - Inventory overview (asset management) can be challenging in a very
    dynamic environment

  - Fast DevOps-style replace of services can be beneficial for patching

  - Otherwise mostly same issues as on-premise
    (patch/update,backups,access control,logs,monitoring)

Something you have probably already learned, but is always worth
repeating, is how to use cryptography in practice for protecting a
simple file. This technique can also be used for a large file tree if
you package/zip the file tree into one file before encrypting it. Nice
cross-platform tools with open implementations of open widely used
crypto algorithms are [7-zip](https://www.7-zip.org) and
[OpenSSL](https://www.openssl.org). Example using the crypto-algorithm
AES-256 (example from a Linux command line session):

    # 7-zip:
    $ dpkg -S $(which 7z)      # which package installs 7-zip?
    p7zip-full: /usr/bin/7z
    $ 7z a -p confidential.txt.7z confidential.txt # encrypt
    $ 7z e confidential.txt.7z                     # decrypt
    
    # OpenSSL:
    $ dpkg -S $(which openssl) # which package installs OpenSSL?
    openssl: /usr/bin/openssl
    $ openssl enc -aes-256-cbc -a -pbkdf2 \
       -in confidential.txt -out confidential.txt.enc    # encrypt
    $ openssl enc -aes-256-cbc -a -pbkdf2 \
       -d -in confidential.txt.enc -out confidential.txt # decrypt

## OpenStack

The original announcement of OpenStack included the mission statement:

> To produce the ubiquitous Open Source Cloud Computing platform that
> will meet the needs of public and private clouds regardless of size,
> by being simple to implement and massively scalable.

The open source project OpenStack is a collection of software components
which can be composed into a public or private cloud. An organization
can choose to implement only the components that it needs. NTNU has a
few different OpenStack implementations. We will use the largest
implementation which is called
[SkyHiGh](https://www.ntnu.no/wiki/display/skyhigh)\[1\]

OpenStack Components in
figure [1.2](#fig:intro:1c7b19ea3ae744638d1c8a70f1e3992c).

![OpenStack
Components.<span label="fig:intro:1c7b19ea3ae744638d1c8a70f1e3992c"></span>](/home/erikhje/office/kurs/secsrv/01-intro/tex/../img/OpenStack_map.pdf)  
<span>[CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0) via
Wikimedia Commons</span>

Demo: Horizon, Heat, Cinder, Swift

SkyHiGh implements the components (might be expanded by the time you
read this) Horizon, Magnum, Heat, Nova, Swift, Cinder, Neutron, Keystone
and Glance. Each of these components can run on their own set of
physical servers. We say "set of physical servers" because in a secure
core infrastructure we need all components to be redundant. *Redundancy*
means that you have \(N\) installations of the same component, where
\(N>1\). Each component offers one or more *services* which are
reachable on a specific *port number* and IP address. E.g. when you want
to create a virtual disk (to attach to your virtual machine) in SkyHiGh,
and you click on "Volumes" and "Create Volume", what is actually
happening is that the web application (Horizon) sends a command ("create
a virtual hard disk of size ...") to port number 8776  on one of the
physical servers who have Heat installed. Which commands we can send to
a service is decided by the API of that service. *The API (Application
Programming Interface)* defines what you can ask a service about, it
basically defines which "commands" you can use towards a service.

### SkyHiGh

SkyHiGh is two racks full of physical servers with redundant
installations of many of the available OpenStack open source components.

A SkyHiGh Rack in
figure [1.3](#fig:intro:3e7d4092bf584f7095dd4c397f085d72).

![A SkyHiGh
Rack.<span label="fig:intro:3e7d4092bf584f7095dd4c397f085d72"></span>](/home/erikhje/office/kurs/secsrv/01-intro/tex/../img/skyhigh.jpg)

When we use SkyHiGh in this course, we are interacting with the
different components in the following way:

  - Horizon  
    is the application that we use when we log in on
    <https://skyhigh.iik.ntnu.no>.

  - Heat  
    is the *orchestration* service that we use when we click on
    "Orchestration". We use Heat for creating a *stack* which is
    OpenStack terminology for an infrastructure composed of different
    cloud components (virtual machine instances, networks, security
    groups, routers, etc). It is called a stack because we can treat it
    (create/delete) as one unit (one integrated stack) even though it
    has several separate cloud components.

  - Cinder  
    (an example of IaaS) is the *block storage* service that we use when
    we want to create a virtual disk.

  - Swift  
    (an example of PaaS) is the *Object storage* service that we use
    when we want to store public or private files.

These are the only components we will be using directly since we will
not create virtual machines or networks manually, only through
orchestration (Heat). We will of course interact with the other
components as well, but only to retrieve information, e.g. when we click
on "Compute", Horizon will ask the Nova service for information about
the running virtual machine instances we have created through Heat.

SkyHiGh is only available to employees and students at NTNU: it is a
*private cloud*. OpenStack is also used for *public clouds*  similar to
Amazon Web Services (AWS), Microsoft Azure or Google Cloud, but the
largest OpenStack installation is probably still the private cloud at
CERN with more than 300K CPU cores  .

## Windows Server

> *Note: in the rest of this chapter, we illustrate the theory with
> examples from PowerShell. We will learn PowerShell in the next chapter
> so don’t worry if you don’t understand the exact syntax used in the
> examples. The author is fully aware that some PowerShell-constructions
> you will see in this chapter might look a bit Greek to you. You should
> return to this chapter and study these examples later when you learn
> more about PowerShell.*

Computers can often be divided into the categories *client* and
*server*. This terms also apply to computer programs: e.g. a web browser
is a client program that communicates with a web server. For operating
systems running on physical hardware ("bare metal") or running in a
virtual machine, a client is characterized by having a user present who
is using interactive programs (like you do when you use your laptop,
tablet or phone). A server is characterized by offering services to
clients or other servers. Windows 10 is a version of the Windows
operating system configured to be a used as a client, while Windows
Server is a version of the Windows operating system configured to be,
you guessed it, a server. A client also has services running, but these
are mostly for internal use.

### Process and Service

Process, Thread, Service in
figure [1.4](#fig:intro:0fead0a256664c05a12a507b18ae9020).

![Process, Thread,
Service.<span label="fig:intro:0fead0a256664c05a12a507b18ae9020"></span>](/home/erikhje/office/kurs/secsrv/01-intro/tex/../img/procthreadsrv.pdf)

When you execute a program on an operating system, the running program
is called a *process*. E.g. when you run Notepad you execute the code in
the file notepad.exe, and you get a running process called notepad (btw
the following code is PowerShell which you will learn more about soon):

    PS> notepad.exe
    PS> Get-Process notepad -IncludeUserName
    
    Handles      WS(K)   CPU(s)     Id UserName               ProcessName
    -------      -----   ------     -- --------               -----------
        216      20352     0.14   6288 DC1\Admin              notepad

Notepad is a typical *interactive process* meant to be used directly by
a user. We can also see that a process has an *owner*, in this example
with notepad it is owned by me since I am the user `Admin` logged in on
the *host* `dc1` (a computer with an operating system is many times
called a *host* since it is the host of many processes, files, users,
etc.).

A process can be single-threaded or multithreaded, meaning it can have
one or more *threads*. All the programs you have written so far in your
studies (and probably all the ones you will write this semester) are
single-threaded. In other words, when you don’t actively create any
threads in your code you end up with what is called a single-threaded
program (when executed it is called a single-threaded process). A thread
is a lightweight process living inside a process. You will learn much
more about this in the Operating Systems course, but for now it is good
to know that the concept exists, and that a process which would like to
use more than one CPU-core at the same time has to be multithreaded. The
operating system allocates the CPU-cores to threads, not processes. You
can see how many threads each process has with the following PowerShell
command line:

    Get-Process | 
      Select-Object -Property Name, ID, `
        @{Name='ThreadCount';Expression ={$_.Threads.Count}} | 
      Sort-Object -Property ThreadCount -Descending

Opposed to an interactive process, a *service* or a *service process* is
a process running in the "background" and typically started as part of
the boot-up process of the computer. A service runs independently of any
logged-in users. Services are mostly owned by separate system accounts.
We can roughly divide services into *host-internal services* and
*network services*:

  - host-internal service  
    is a service that is supporting the operating system or doing some
    kind of local system maintenance, e.g Windows Update, Task
    Scheduler. Some of the really important host-internal services we
    call *system processes* (these are core to the functioning of the
    Windows operating system).

  - network service  
    is a service that is listening on a port for incoming connections
    from a client, e.g. Remote Desktop Services who is listening by
    default on port 3389.

Seeing which service processes are listening for connection from the
network is many times an interesting exercise both for troubleshooting
and for security/forensics:

    Get-NetTCPConnection |
      Select-Object -Property LocalAddress,LocalPort,State,OwningProcess,`
      @{Name='ProcessName';Expression={(Get-Process `
        -Id $_.OwningProcess).ProcessName}},`
      @{Name='ServiceName';Expression={(Get-CimInstance -ClassName `
        Win32_Service -Filter "ProcessID=$($_.OwningProcess)").Name}} | 
      Format-Table -AutoSize

Service processes have to comply with the protocol/rules of the *Service
Control Manager* (a special *system process* called `services`). This
means that all service processes have to be able to be maintained by
accepting messages from the Service Control Manager such as Stop, Start,
Suspend, Resume, etc. We can use PowerShell to ask the service control
manager to send these messages:

    PS> Get-Command *service | Format-Table -Property Name
    
    Name
    ----
    Get-Service
    New-Service
    Remove-Service
    Restart-Service
    Resume-Service
    Set-Service
    Start-Service
    Stop-Service
    Suspend-Service

Service processes can be stand-alone processes, or they can be "wrapped"
by themselves or together with other services in a *service host
process* with process name `svchost`. The easiest way of seeing all the
services and which services are hidden in a service host process is by
using the old cmd-program `tasklist /svc`. When a service is wrapped in
a service host process it means that the service is loaded into the
service host process from a DLL (Dynamic Link Library), so the service
does not exist a separate process, it only exists inside the service
host process. The reason for having these service host processes was to
save resources since each process takes up a significant amount of
memory on Windows. With modern 64-bit Windows running on 64-bit hardware
memory is not such a big issue anymore, and most service host processes
only contain a single service nowadays .

### Accounts

Accounts in
figure [\[fig:intro:58691d103f0d41c2a8474b9d72344f3f\]](#fig:intro:58691d103f0d41c2a8474b9d72344f3f).

  - User accounts

  - System accounts

As mentioned previously, a process is owned by an account on the system.
To get a quick overview in PowerShell (must be run as Administrator):

    Get-Process -IncludeUserName |
      Select-Object -Property Name,UserName,ID |
      Sort-Object -Property UserName,ID

Files and directories are also owned by accounts on the system. We need
to have ownership of all objects on the system if we are going have any
kind of access control/security. To see which local accounts that are
present on your Windows (we can have "network accounts" as well, but we
will learn about that later):

    # List user accounts
    Get-LocalUser
    # or
    Get-CimInstance Win32_UserAccount
    # List system accounts
    Get-CimInstance Win32_SystemAccount

We can also use PowerShell to see a list of differences between User and
System accounts. The difference is basically that user accounts have
several password-fields since they are meant for interactive login
sessions while system accounts are used for services. Notice how the
"SideIndicator" points towards the right for the "DifferenceObject"
which is the user account in the variable `$u`. This means that this
property (the "InputObject") only exists in `$u` and not in `$s`:

    PS> $s = Get-CimInstance Win32_SystemAccount | Get-Member
    PS> $u = Get-CimInstance Win32_UserAccount | Get-Member
    PS> Compare-Object -ReferenceObject $s -DifferenceObject $u
      
      InputObject                                 SideIndicator
      -----------                                 -------------
      uint AccountType {get;}                     =>
      bool Disabled {get;set;}                    =>
      string FullName {get;set;}                  =>
      bool Lockout {get;set;}                     =>
      bool PasswordChangeable {get;set;}          =>
      bool PasswordExpires {get;set;}             =>
      bool PasswordRequired {get;set;}            =>
      PSStatus {Status, Caption, PasswordExpires} =>
      PSStatus {Status, SIDType, Name, Domain}    <=

### Configuration Data

#### Registry

The Registry  is a database that stores most of the configuration on a
Windows host. The purpose is to avoid configuration stored in files many
places in the file system.

The Registry in
figure [\[fig:intro:63c227c7570f4919a364321c2553660f\]](#fig:intro:63c227c7570f4919a364321c2553660f).

  - HKEY\_LOCAL\_MACHINE (HKLM)  
    Config for local computer, SAM, SECURITY,  
    SOFTWARE and SYSTEM, files in `%SystemRoot%\System32\config`

  - HKEY\_CURRENT\_CONFIG (HKCC)  
    link to  
    `HKLM\System\CurrentControlSet\`  
    `Hardware Profiles\Current`

  - HKEY\_USERS (HKU)  
    each user profile actively loaded on the machine

  - HKEY\_CURRENT\_USER (HKCU)  
    link to currently logged on user in `HKU`

  - HKEY\_CLASSES\_ROOT (HKCR)  
    a compilation of `HKCU\Software\Classes` and  
    `HKLM\Software\Classes`

<!-- end list -->

  - HKLM contains the sub keys SAM (user account database), SECURITY,
    SOFTWARE and SYSTEM which have corresponding files on disk. It also
    contains the sub key HARDWARE which is generated at runtime (similar
    to the `proc` filesystem on Linux).

  - HKCC just links into HKLM.

  - HKU is user specific config data for each active user profile (note:
    a user profile can be active without the user being logged in).

  - HKCU just links into HKU.

  - HKCR stores file associations and is just linked into HKLM and HKCU.

Edit the registry using the GUI regedit.exe or with command-line
PowerShell:  
`Get-ItemProperty` and `Set-ItemProperty`.

*On a bigger scale/in production, you almost never change the registry
directly, instead you change it indirectly using config management tools
such as group policy.* Some examples of retrieving data from the
registry using PowerShell:

    # List installed software that can be uninstalled:
    Get-ChildItem `
      HKLM:\software\microsoft\windows\currentversion\uninstall |
      ForEach-Object {Get-ItemProperty $_.PSPath} | 
      Format-Table -Property DisplayName
    # List all local firewall rules (messy)
    # (combine into one line):
    Get-ItemProperty HKLM:\System\CurrentControlSet\Services\
      SharedAccess\Parameters\FirewallPolicy\FirewallRules

#### WMI

Windows Management Instrumentation in
figure [\[fig:intro:f97d9fdd47aa439080c7b107b2c5916c\]](#fig:intro:f97d9fdd47aa439080c7b107b2c5916c).

  - CIM (Common Information Model)

  - WMI: objects with properties and methods grouped into namespaces

  - Mostly *config data from registry* and *performance* data (from the
    operating system)

The CIM standard defines a large set of management objects/tables which
is supposed to represent “everything that can be managed on a host” in a
standardized way. These CIM objects have properties and methods. WMI is
Microsoft’s implementation of CIM with WMI providers (COM objects
implemented as DLLs, approx 100 in total) which provides access to these
CIM-based objects. Since the number of CIM objects/tables is extremely
large, they are grouped into *namespaces*.

Note: much of the information that can be retrieved and manipulated
through WMI comes from the registry (or “can also be found in the
registry”). But you can also access information like performance
counters (from the operating systems data structures) through WMI.

    # Show the list of all namespaces:
    Get-CimInstance -Namespace root -ClassName __Namespace
    
    # List all classes in namespace:
    Get-CimClass -Namespace root/CIMV2
    
    # List all instances (objects) of class Win32_Processor:
    Get-CimInstance Win32_Processor
    
    # List all properties of a specific instance (object):
    Get-CimInstance Win32_Processor -Filter "DeviceID='CPU0'" | 
      Select-Object -Property *
    
    # Show a specific property of a specific instance (object):
    (Get-CimInstance Win32_Processor `
      -Filter "DeviceID='CPU0'").LoadPercentage

Make sure you become comfortable with the terminology *namespace*,
*class* and *object*. You will encounter these terms many times in
different courses this semester:

  - Namespace  
    When you have a high number of classes/objects/variables, and you
    need to group them together you introduce a namespace, in other
    words just a high-level category to separate groups of
    classes/objects/variables from each other. An analogy can be postal
    codes (postnummer): e.g. 2843 is the postal code for Eina but only
    in the *namespace Norway*, in other countries the postal code 2843
    represents something else if it is defined.

  - Class  
    A class is a *definition* of an object. It is abstract. When you
    learned programming in C you had different data types (int, float,
    char, etc). A data type is similar to a class, it is just a
    definition of something, e.g. an int is a number that uses four
    Bytes of space in memory.

  - Object  
    An object is when you have to declare a concrete instance of a
    class. It is something that actually exists, and you use, similar to
    when you declare a variable in C, e.g. `int i;` (int is similar to a
    class, i is similar to an object).

Notice also that we use the word *instance* in many situations when we
talk about the concrete realization of something. In the text above we
talk about the objects as "instances of a class" and when we use clouds
like SkyHiGh we call the virtual machines "instances of an image" (or
mostly we just say "instance", the "image" refers to the disk image of a
base operating system that we create our virtual machine from, e.g.
Ubuntu 20.04 or Windows Server 2022).

When studying computer science you have to deal with the fact that some
terms have different meanings dependent on the context they are used in.
Many times we use the different terms for the same thing: e.g. in this
course (and later in the study program) you will see that when referring
to a virtual machine in SkyHiGh, we will say *instance*, *VM*, *host*,
*server* or specify the name we have given the virtual machine, e.g.
*DC1*.

### Using Windows Server

When you log into a Windows Server you are automatically greeted with
the application *Server Manager*. We will not use Server Manager much
(we will later use the new Windows Admin Center instead), but we will
use it to get a quick overview of server status and to quickly find the
tools we are looking (see the menu *Tools*).

Server Manager in
figure [1.5](#fig:intro:2c014f163ce046c0928e985cc7b6eb82).

![Server
Manager.<span label="fig:intro:2c014f163ce046c0928e985cc7b6eb82"></span>](/home/erikhje/office/kurs/secsrv/01-intro/tex/../img/servermgr.png)  

Notice that one of the first things Server Manager proposes that we do
is to add *Roles* and *Features*:

  - Role  
    A server role is a set of software programs which make up a specific
    function for multiple users or other computers within a network.

  - Feature  
    Features are software programs that, although they are not directly
    parts of roles, can support or augment the functionality of one or
    more roles, or improve the functionality of the server, regardless
    of which roles are installed.

  - Capability  
    Note quite sure, but seems like a special kind of software
    installation, e.g OpenSSH.client is an installed capability.

Right Click Start-button in
figure [1.6](#fig:intro:8cb436b390784a359df61cf12ae58d92).

![Right Click
Start-button.<span label="fig:intro:8cb436b390784a359df61cf12ae58d92"></span>](/home/erikhje/office/kurs/secsrv/01-intro/tex/../img/rightclickstart.png)  

It is also worth noting that many of the same GUI-tools for management
of a Windows Server can be accesses by right-clicking on the
Start-button.

#### Sysinternals

Sysinternals in
figure [\[fig:intro:8dcf8dfd586d41c3ae9ec7b109e2b8c6\]](#fig:intro:8dcf8dfd586d41c3ae9ec7b109e2b8c6).

  - File and Disk (`AccessChk`)

  - Networking (`AD Explorer`, `TCPview`)

  - Process (`Process Explorer`)

  - Security (`SysMon`, `SDelete`, `AccessChk`)

  - System info (`coreinfo`, `RAMMap`)

  - Misc (`BgInfo`, `ZoomIt`)

Sysinternals  is a collection of very useful tools created and updated
over the last 25 years mostly by Mark Russinovich, let’s install the
entire collection and use some of them in the exercises:

    # if first time installing with chocolatey:
    Set-ExecutionPolicy Bypass -Scope Process -Force
    [System.Net.ServicePointManager]::SecurityProtocol =
      [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
        iex ((New-Object System.Net.WebClient).
          DownloadString('https://chocolatey.org/install.ps1'))
    # always check the status of a package before installing, if ok:
    choco install -y sysinternals

If the package web page says e.g. "Some Checks Have Failed or Are Not
Yet Complete", make sure you know what it means. Read the text under
"Details" and also take a look at [Chocolatey Community Package
Repository](https://docs.chocolatey.org/en-us/information/security#chocolatey-community-package-repository).

If you really want to dig deep into how Windows works (and use the
sysinternals utilities to do it), Mark Russinovich has co-authored the
book "Windows Internals"  for you.

### Windows Command Line Interface (CLI)

Before 2006 Windows only had the good old command line interface (CLI)
`cmd` (which is still present on all versions of Windows). In 2006
PowerShell arrived, and has since been continuously growing in
popularity and is now the standard CLI on Windows hosts. Most of what we
need to do on a Windows host, we can do with PowerShell, but sometimes
PowerShell do not offer the same functionality as the traditional
[Windows
Commands](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/windows-commands)
from the `cmd` CLI, fortunately these work just as well in PowerShell as
in `cmd`.

CLI is Power\! in
figure [1.7](#fig:intro:4b1daab834194ce685e7b28cc8958c2b).

![CLI is
Power\!.<span label="fig:intro:4b1daab834194ce685e7b28cc8958c2b"></span>](/home/erikhje/office/kurs/secsrv/01-intro/tex/../img/sudo.png)

GUI vs CLI in
figure [\[fig:intro:5db109ebff9b411b8de9a94bde234829\]](#fig:intro:5db109ebff9b411b8de9a94bde234829).

*“I need to do something `N` times on `M` hosts”*

  - `N=1, M=1`  
    Use a GUI

  - `N>1, M=1`  
    Use CLI

  - `N=1, M>1`  
    Use CLI

  - `N>1, M>1`  
    Use CLI

<!-- end list -->

  - In many situations you do not want a GUI

  - With CLI you can script your tasks with *consistency\!*

*Next topic, let’s go PowerShell\!*

Any Relevant Jobs? in
figure [\[fig:intro:e69e3095962647d88cd33085bad7a00e\]](#fig:intro:e69e3095962647d88cd33085bad7a00e).

[Vil du hacke Norge i landets mest spennende miljø for
IT-sikkerhet?](https://arbeidsplassen.nav.no/stillinger/stilling/3ed907e9-8fbd-404d-ae59-08ff0e6e5e9e)

## Review questions and problems

1.  In which time period did AWS EC2, Google App Engine, Microsoft Azure
    and OpenStack appear?
    
    1.  1996-2000.
    
    2.  2001-2005.
    
    3.  2006-2010.
    
    4.  2011-2015.

2.  What do you consider as a major *privacy* concern/threat when you
    use a public cloud? Give an example including a technical measure (a
    technical solution) to prevent it.

3.  What is *orchestration*? Which component in OpenStack provides this
    service?

4.  What is a *service* in Windows? Describe briefly.

5.  What is a *WMI* (Windows Management Instrumentation? Describe
    briefly.

6.  Let’s explore the topic of roles and features:
    
    1.  Use Server Manager to install the *role* "Web Server (IIS)".
    
    2.  Which *feature* is required during installation of this role?

7.  Let’s explore the topic of processes and services:
    
    1.  Install Sysinternals. Start Process Explorer \[2\].
    
    2.  Sort processes by CPU-usage, which process consumes the most CPU
        and why do you think that is?
    
    3.  How much memory (RAM, Working Set) does PowerShell (`pwsh`) use?
        How much of that memory is private only for PowerShell?
    
    4.  Apparently, processes are arranged in a hierarchy
        (tree-structure) where some processes are parents of others.
        Click three times on the Process-tab to switch between views.
        Which process is the parent of PowerShell? Describe what that
        process is.
    
    5.  Right-click on the Explorer-process, choose "Properties", how
        many threads does this process have?
    
    6.  Which command-line have started the Explorer-process?
    
    7.  Start the Sysinternals tool TCPview. Which process is listening
        on port 80?
    
    8.  Go back to Process Explorer, right click and "Properties" on the
        process you found who is listening on port 80. Choose the
        TCP/IP-tab, check and uncheck the box "Resolve addresses" to see
        two different views. We do this so you can confirm the
        information you saw with TCPview. Do you get a good gut-feeling
        about what you have found out about the process who is listening
        on port 80?
    
    9.  Add the "Command line"-column to Process Explorer (View, Select
        Columns, Process Image), expand the width of the column so you
        see the entire text, can you find a command line containing
        `iissvcs`? Which services does this svchost-process provide?

8.  (**KEY PROBLEM**) Explain how the Registry, WMI, services and the
    Sysinternals tools relate to each other.

## Lab tutorials

1.  Do the [Basic Infrastructure
    Orchestration](https://gitlab.com/erikhje/dcsg1005/-/blob/master/heat-labs.md)
    exercise with the template  
    [single\_windows\_server.yaml](https://gitlab.com/erikhje/heat-mono/-/blob/master/single_windows_server.yaml).
    You have completed the exercise when you have logged in to the host,
    done this weeks practical parts of "Review questions and problems",
    and then deleted the stack. Note: the way you are supposed to work
    in this course is:
    
    1.  to create an infrastructure (a stack) like you do here
    
    2.  document/backup anything you do of value in your infrastructure
    
    3.  delete the infrastructure (the stack)
    
    Sometimes you will do these three items for just one week like now,
    and sometimes you will let your infrastructure exist for a couple of
    weeks. *Always be prepared for quickly deleting and recreating your
    stack, that’s how we take advantage of what the cloud offers us, and
    helps us get used to modern DevOps thinking: frequent changes.*

# PowerShell

## PowerShell

PowerShell in
figure [\[fig:powershell:a881557f372b4d2baebcb537b302cb46\]](#fig:powershell:a881557f372b4d2baebcb537b302cb46).

  - See [separate document on
    PowerShell](https://gitlab.com/erikhje/dcsg1005/blob/master/powershell.md),
    for this week and the following two weeks we focus on [PowerShell 1:
    Standalone
    Host](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#powershell-1-standalone-host)

Note: this PowerShell-document is meant to cover everything we need to
know about PowerShell throughout the semester. It contains more material
than what is possible to learn in just one week. Read everything in
[PowerShell 1: Standalone
Host](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#powershell-1-standalone-host)
this week to get the initial overview, and focus on the lab tutorial and
review questions and problems. For now, you just need to practice on a
single host. When we get to chapter five (week five), you will need to
start studying the second part of the PowerShell-document called
[PowerShell 2: Domain-joined hosts and
Remoting](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#powershell-2-domain-joined-hosts-and-remoting).

## Review questions and problems

1.  Find out which PowerShell cmdlet are mapped to the aliases `cd`,
    `echo`, `cat`, `cp`, `rm` and `sort`. Try all these six cmdlets.

2.  Write a command line which recursively outputs all directories in
    your home directory. It should not list files, only directories  
    (hint: `Get-Help -Online Get-ChildItem`).

3.  Create a variable `$golf` with the value `Viktor Hovland`. Use
    `Write-Output` to print "My golf hero Viktor Hovland" using this
    variable. Advanced: Now try to use the `Split()` method of the
    variable to print only "My golf hero Hovland" (hint: you can use the
    `Split()`-method just like this without any arguments).

4.  Use `Get-Member` to list all properties and methods in the objects
    you get from `Get-ChildItem`. Pipe to `more` to page-by-page (by
    hitting space) or line-by-line (by hitting enter). Repeat with
    `Get-Process` instead of `Get-ChildItem`.

5.  Write a PowerShell command line that will list all files in the
    directory `C:\Windows` that are larger than 10KB. Then add to the
    pipeline:
    
    1.  Make the output sorted based on file size (Length)
    
    2.  Make the output show only the three largest files
    
    3.  Make the output show only file name, file size, last access time
        and last write time

6.  Using
    [splatting](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_splatting),
    write a command line which recursively outputs all files (not
    directories, only files) in  
    `C:\Windows\System32\LogFiles` and also have the parameter
    ErrorAction set to SilentlyContinue.

7.  (**KEY PROBLEM**) Write a command line which prints the name of all
    directories (from your current directory) which contain at least 10
    files/subdirectories.

8.  (**KEY PROBLEM**) Write a command line which outputs all processes
    which have the property  
    `StartTime` within the last hour.

## Lab tutorials

1.  Study and work through all exercises included in [PowerShell 1:
    Standalone
    Host](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#powershell-1-standalone-host).

# Storage, Backup, Restore

## Storage and Windows

File-level vs Block-level in
figure [3.1](#fig:storage:aec4e1defaee4daa91345aefc5b35492).

![File-level vs
Block-level.<span label="fig:storage:aec4e1defaee4daa91345aefc5b35492"></span>](/home/erikhje/office/kurs/secsrv/03-storage/tex/../img/filevsblock.pdf)

We are used to dealing with the concept of a *file*.

  - A File  
    has two components: metadata and data. What the metadata component
    consists of depends on the file system, but most file system
    supports owner, permissions, timestamps and size (size is sometimes
    called length since the file contents can be treated as an array of
    Bytes and this array has a length). In addition, the metadata
    contains information about how to reach the data (the actual file
    contents). In the simplest form this can be to just have all the
    block addresses of the data blocks stored in the metadata.

Many of us have used a storage device like a USB-harddrive, memory-stick
or memory-card. A storage device consists of blocks. Before we can use
it for files, it needs to have a file system written to it. When we work
with data storage, backup and restore, we sometimes work with files at
the *file-level* and sometimes we work at the *block-level*. It is
important to understand this difference, especially in the cloud and
other virtualized infrastructures. When you use a virtual machine, the
block-level storage device is commonly a file. This means that you are
using a *storage stack* where the file you save in the virtual machine
is stored in a file system which is on a block device, and the block
device is actually just a file on a physical server which is stored on a
file system on a block device (and the block device might be the actual
hard drive on the server or a block device accessed over the network
from a storage solution like Ceph  which is the one used in SkyHiGh).

The following is a summary of key concepts (including corresponding
PowerShell cmdlets and command lines) we need to know about:

Some Terminology in
figure [\[fig:storage:4a9ed5a9f06441c8afc41d4b9ffbe7c6\]](#fig:storage:4a9ed5a9f06441c8afc41d4b9ffbe7c6).

|                    |                           |
| :----------------- | :------------------------ |
| Disk               | `Get-Disk`                |
| Partition table    | `Initialize-Disk`         |
| Partition          | `Get-Partition`           |
| Volume             | `Get-Volume`              |
| File system        | `ntfsinfo` (Sysinternals) |
| File               | `Get-ChildItem`           |
| File: ACL with ACE | `Get-Acl`                 |

  - Demo: Disk management GUI-tool

<!-- end list -->

  - Disk  
    (`Get-Disk`) What Windows perceives as a physical disk (SSD or HDD).
    In our case this is what we create in OpenStack (in the OpenStack
    component called “Cinder”) as a “Volume” and attach to a server. In
    other words: inside our server this is seen as a physical disk and
    we treat it as a physical disk, but outside our server (in our
    cloud) this is a volume.

  - Partition table  
    (`Initialize-Disk`) On a new physical disk we create a partition
    table. This is written in the first part of the disk, and is either
    the old-style “Master Boot Record (MBR)” or the newer (and what we
    typically use today) “GUID Partition Table (GPT)”.

  - Partition  
    (`Get-Partition`) A partition is a part of a disk (it is defined in
    the partition table).

  - Volume  
    (`Get-Volume`) A volume is unfortunately not a term that is clearly
    defined. Sometimes it is the same as a partition, sometimes it is
    multiple partitions joined together, sometimes we talk about a
    physical and a logical volume, etc..., it depends on the context.
    For now, just think of it as either a partition or a group of
    partitions treated as one unit. *A volume is something we can create
    a file system on.* (We can also create a file system directly on a
    partition, but we typically have a volume layer between the file
    system and the partition).

  - File system  
    (`Get-Volume | Format-Table -Property DriveLetter,FileSystemType`) A
    file system is the data structure we write to a volume (or a
    partition in some cases) that allows us to store files and
    directories/folders. On Windows the most common file system is NTFS.

  - File  
    (`Get-ChildItem`) A file consists of metadata (owner, permissions,
    timestamps, etc.) and data (the actual contents of the file). A
    directory is just a special kind of file.

  - File Security: ACL with ACE  
    (`(Get-Acl file).Access`) All files and folders have and Access
    Control List (ACL). An ACL is a list of Access Control Entries
    (ACE). Each ACE defined a user or group and what permission (read,
    write, execute, append, etc.) they have. Each entry can be of type
    allow or deny (deny can be used to exclude a permission that a user
    otherwise would have). ACLs are scanned by the operating system
    (Windows) in order and the first ACE that match the access attempted
    is used.

## Backup: Why?

Chapter 2.9 "Etabler evne til gjenoppretting av data" of NSM’s
"Grunnprinsipper for IKT-sikkerhet 2.0"  is worth reciting in full. We
will address most of these measures and submeasures in the rest of this
chapter.

Measure 2.9.1:

> Legg en plan for regelmessig sikkerhetskopiering av alle
> virksomhetsdata. En slik plan bør som minimum beskrive
> 
> 1)  Hvilke data som skal sikkerhetskopieres.
> 
> 2)  Regelmessighet på sikkerhetskopiering av ulike data, basert på
>     verdi.
> 
> 3)  Ansvar for sikkerhetskopiering av ulike data.
> 
> 4)  Prosedyrer ved feilet sikkerhetskopiering.
> 
> 5)  Oppbevaringsperiode for sikkerhetskopier.
> 
> 6)  Logiske og fysiske krav til sikring av sikkerhetskopier.
> 
> 7)  Krav til gjenopprettingstid for virksomhetens ulike systemer og
>     data (se prinsipp 4.1 - Forbered virksomheten på håndtering av
>     hendelser).
> 
> 8)  Godkjenningsansvarlig(e) for planen.

Measure 2.9.2:

> Inkluder sikkerhetskopier av programvare for å sikre gjenoppretting.
> Dette inkluderer (som minimum)
> 
> 1)  sikkerhetskonfigurasjon ref. prinsipp 2.3 - Ivareta en sikker
>     konfigurasjon og
> 
> 2)  maler for virtuelle maskiner og
> 
> 3)  "master-images" av operativsystemer og c)
>     installasjonsprogramvare.

Measure 2.9.3:

> Test sikkerhetskopier regelmessig ved å utføre gjenopprettingstest for
> å verifisere at sikkerhetskopien fungerer.

Measure 2.9.4:

> Beskytt sikkerhetskopier mot tilsiktet og utilsiktet sletting,
> manipulering og avlesning.
> 
> 1)  Sikkerhetskopier bør være separert fra virksomhetens
>     produksjonsmiljø. Se bl.a. prinsipp 2.1 - Ivareta sikkerhet i
>     anskaffelses- og utviklingsprosesser.
> 
> 2)  Tilgangsrettigheter til sikkerhetskopier bør begrenses til kun
>     ansatte og systemprosesser som skal gjenopprette data.
> 
> 3)  Det bør jevnlig tas offline sikkerhetskopier som ikke kan nås via
>     virksomhetens nettverk. Dette for å hindre tilsiktet/utilsiktet
>     sletting eller manipulering.
> 
> 4)  Sikkerhetskopier bør beskyttes med kryptering når de lagres eller
>     flyttes over nettverket. Dette inkluderer ekstern
>     sikkerhetskopiering og skytjenester.

Backup System = Data Restoration System in
figure [\[fig:storage:6759b4e40a084275a1fa6e2827c3814a\]](#fig:storage:6759b4e40a084275a1fa6e2827c3814a).

  - Why?
    
      - Archival purposes
    
      - Disk failure
    
      - Theft
    
      - Accidental deletion
    
      - Ex-employees (insider threats)
    
      - Malware: *ransomware*
    
      - Natural disasters, bomb, el.magn.pulse., etc

A Backup system should really be called a "Data restoration system"
since backups are useless if we cannot do restore of backed up data. The
backup system plays an integral part in the overall Disaster Recovery
Plan (in the NSM-document  this is in section four and called "planverk
for hendelseshåndtering som ivaretar behovet for virksomhetskontinuitet
ved beredskap og krise."). It is important to think about why we should
do backups because it directly influences which strategy we should
choose. If the most common reason for restore requests is accidential
deletion, then we should put some effort into making sure users have
easy access to basic file restore. This can for instance be to have a
local backup on each users laptop and educate users on how they can
restore files from it.

### Ransomware

The Evolution of Ransomware in
figure [3.2](#fig:storage:40b6776a99024e7f88e5d33c00ff2ad8).

![The Evolution of
Ransomware.<span label="fig:storage:40b6776a99024e7f88e5d33c00ff2ad8"></span>](/home/erikhje/office/kurs/secsrv/03-storage/tex/../img/ransomware.png)  
<span>[F-Secure by Mikael
Albrecht](https://blog.f-secure.com/ransomware-timeline-2010-2017/)</span>

Punchline: *Be prepared. What will be the biggest threat in 2030?*

Ransomware is one of the biggest cybersecurity threats. Ransomware is
highly relevant for us since it is a threat to business continuity in at
least two ways: 1. it disrupts production by encrypting data and 2. it
steals confidential data (company secrets) and threatens to disclose it.
This means that reliable backups with fast restore procedures are
necessary to protect against threat number one, and protection of
backups is one of the measure to counter threat number two.

## Backup: What?

Data Analysis in
figure [\[fig:storage:5be8a9d510f74b1099cea814a1c5e3e6\]](#fig:storage:5be8a9d510f74b1099cea814a1c5e3e6).

from Æleen Frisch 

  - *Which files* need to be backed up? (which files or file types
    should be *ignored*)?

  - Where, when and under what conditions should backups be performed?

  - How often do these files change? What is *a change*?

NSM measure 2.9.2: Remember *configs, software, OS images, installation
software*

It is important to carefully estimate the value of the information you
have stored in your files. Some files are more important than others,
e.g. source code is much more important than a compiled executable file.
When you know which files to back up, you should analyze how much space
these files occupy and how frequently they change. This analysis builds
the ground for designing the backup system. We will not go into the
exact computations, value estimations and risk assessments needed to
build a complete backup system in this course. We focus on the basic
understanding of how backups work and what we need to do to have the
necessary basic protection.

### Exclude/Ignore files

Not every file should be backup up, so it is important to set up a list
of what we want to exclude/ignore from the backup. This list can be
files, directories or file/directory names that match certain patterns.
A simple algorithm for doing this is:

1.  Create a basic list of what is the most obvious to exclude.

2.  Do a backup. Look for log messages of errors or warnings. Examine
    the backup and look for unwanted files.

3.  Modify your list of what to exclude.

4.  Repeat until you are happy.

A typical list to start with of you are setting up a full backup of a
Windows-host, would look something like this:

    C:$$Recycle.Bin
    C:\pagefile.sys
    C:\hiberfil.sys
    C:\swapfile.sys
    C:\ProgramData\Microsoft\Windows Defender\Definition Updates\Backup\*
    C:\ProgramData\Microsoft\Windows Defender\Support\*.log
    C:\Windows\Temp\MpCmdRun.log
    C:\Windows\SoftwareDistribution\DataStore\Logs\*
    C:\Users\*\AppData\Local\Temp\Diagnostics\*
    C:\Windows\SoftwareDistribution\Download\*
    C:\ProgramData\Microsoft\Network\Downloader*
    C:\Windows\system32\LogFiles\WMI\RtBackup\*.*
    C:\Windows\memory.dmp
    C:\System Volume Information\*

### File Change Detection

File Change Detection in
figure [\[fig:storage:1a33512e68ee4345958cf4996e85ab8c\]](#fig:storage:1a33512e68ee4345958cf4996e85ab8c).

  - Actual content changed? (scanning for this takes time...)

  - Only scan content if a "B-MAC" change?
    
      - *B*irth (`CreationTime`)
    
      - *M*odified (`LastWriteTime`)
    
      - *A*ccess (`LastAccessTime`)
    
      - *C*hange (only change in metadata)

*Read it without updating AccessTime?*

Always scanning files for content changes is very expensive for large
file collections. Sometimes it is necessary to do it, and to be
absolutely certain of detecting any changes it must be done. Most of the
time it is sufficient to only scan for changes in Modified time. Restic
on other platforms than Windows checks if Modified time, Change time,
File Size and "File-ID" all match the previous version of the file if
Restic is to consider the file as not changed . For Windows environment
the Restic documentation states:

> On Windows, a file is considered unchanged when its path, size and
> modification time match …

## Backup: How?

### Tools

Restic.net in
figure [3.3](#fig:storage:03c3824332424623a492af0b07735316).

![Restic.net.<span label="fig:storage:03c3824332424623a492af0b07735316"></span>](/home/erikhje/office/kurs/secsrv/03-storage/tex/../img/restic.png)

There are many backup tools available. We have chosen to Restic in the
exercises since it is widely used, cross-platform and supports using
OpenStack Object Storage as a storage repository.

### Architecture

From NRK beta :

> \- Jeg tenkte "det her kan sikkert løses med backup", sier Jacobsen i
> dag. Han ler nervøst. Slik gikk det dessverre ikke. Selskapet hadde en
> sikkerhetskopi av selskapets viktigste filer, men også den hadde blitt
> kryptert.

Architecture for Ransomware Protection in
figure [3.4](#fig:storage:d79124baef92434dac4e9e5cf82f39d6).

![Architecture for Ransomware
Protection.<span label="fig:storage:d79124baef92434dac4e9e5cf82f39d6"></span>](/home/erikhje/office/kurs/secsrv/03-storage/tex/../img/arch.pdf)

A key problem we need to solve when setting up a backup system
architecture is that backups should be *immutable*, meaning they should
not be possible to tamper with. If we are attacked by malware (e.g.
ransomware) and the malware is able to run with the permissions of the
backup account, the threat is that the malware will overwrite or encrypt
our backups. To protect against this threat we have at least three
different architectural options:

  - Air gap  
    means to have an "air gap" (and no wireless connection either of
    course) between our data and the backup storage, e.g. when you have
    your backup on a hard drive that is not connected in any way to your
    computer.

  - Intermediary server  
    means to have a dedicated backup server which a local backup process
    on your computer can write to, but a separate process on the backup
    server will copy/move, verify and secure your backup as soon as you
    have written to it. This way the backup server can move a copy out
    of reach for any malware on your computer.

  - Immutable storage  
    means for the backup process on your computer to have "append only"
    permission to the backup storage. In this way, malware can add
    encrypted data (or malware) to the backups but not influence
    previously backed up data.

In the exercises we will set up backup similar to "Immutable storage"
when we store backups in the object store of SkyHiGh.

### Full or Partial

Full or Partial in
figure [\[fig:storage:be6b455ab3a04e83b9f337b954009fd8\]](#fig:storage:be6b455ab3a04e83b9f337b954009fd8).

  - Full (level 0)  
    Copy all files

  - Partial  
    Two concepts sometimes referred to as the same:
    
      - Differential (level 1)  
        All new or modified files since last full backup
    
      - Incremental (level \(n\))  
        All new or modified files since last full *or partial* backup
    
    <!-- end list -->
    
      - Difference between differential and incremental sometimes
        referred to as *(dump) level \(n\)* where \(n\in{0,1,2,\ldots}\)

Dump levels refer to how many backups are needed to do a full restore.
If the most recent backup performed was a level three, then you would
need to do the following for a full restore:

1.  Restore last full backup (level 0)

2.  Restore last level 1 incremental backup

3.  Restore last level 2 incremental backup

4.  Restore last level 3 incremental backup

Doing a full backup takes much more time than doing a partial backup, so
"dump level strategy" is based on a trade-off between backup-time and
restore-time. These concepts (differential and incremental) are mostly
relevant when talking about tape backups, since reading from multiple
tapes can be quite time-consuming. When backing up to hard drive based
storage solutions (including cloud storage) we talk about *snapshots*
instead of increments. Snapshots are in principle increments, but they
are implemented in a way that is transparent to the user when doing
restore. A snapshot is a copy of your data at a certain point in time
(although the mechanism behind the scene is similar to an increment).

### 3-2-1 plan

3-2-1 Backup Plan in
figure [3.5](#fig:storage:c9efa6554490487d9921419b4a9f578c).

![3-2-1 Backup
Plan.<span label="fig:storage:c9efa6554490487d9921419b4a9f578c"></span>](/home/erikhje/office/kurs/secsrv/03-storage/tex/../img/3-2-1.pdf)

The 3-2-1 backup plan was coined by photographer Peter Krogh in the book
"The DAM Book: Digital Asset Management for Photographers"  and is a
general concept for having what should be a minimum of protection for
your data. It states that there should be three copies of your data
(typically one in production and two backups), two different storage
media should be in use (e.g. hard drive and magnetic tape) and one copy
should be off-site (in case of fire or similar incidents). We should
think of this rule as a minimum, e.g. the author practices a 4-3-2
version with production data in home directory, local backup on same
SSD, [remote backup on NTNU’s file
server](https://i.ntnu.no/wiki/-/wiki/English/File+Backup) and NTNU has
a backup system for the file server (probably a "tape robot" of some
kind).

### Schedule

[NTNU’s web page on
backups](https://i.ntnu.no/wiki/-/wiki/English/File+Backup) state "A
backup is taken of every home directory, every night, so that files can
be restored if corrupted or deleted". This is considered the off-site
secure backup. In addition, you probably want to protect against
accidental deletion and have a local backup that takes a
snapshot/increment every half hour (or even more often).

### Backup user

Measure 2.6.5 "Minimer rettigheter på drifts-kontoer" of NSM’s
"Grunnprinsipper for IKT-sikkerhet 2.0"  is worth reciting in full:

1)  Etabler ulike kontoer til de ulike drifts-operasjonene (selv om det
    kanskje er samme person som reelt sett utfører oppgavene), slik at
    kompromittering av en konto ikke gir fulle rettigheter til hele
    systemet. Dvs. forskjellige drifts-kontoer for backup,
    brukeradministrasjon, klientdrift, serverdrift, mm.

2)  Begrens bruken av kontoer med domeneadmin rettigheter til kun et
    minimum av virksomhetens drifts-operasjoner. Spesielt bør kontoer
    med domene-admin rettigheter aldri benyttes interaktivt på klienter
    og servere (reduserer konsekvensene av "pass the hash" angrep).

3)  Unngå bruk av upersonlige kontoer ("backup\_arne" er bedre enn bare
    "backup") slik at man har god sporbarhet og lettere kan deaktivere
    kontoer når noen slutter. Hvis det er vanskelig å unngå å ha en
    upersonlig konto bør man først logge seg inn med en personlig bruker
    for å ivareta sporbarhet.

This means we should use a dedicated account to run the backup service
and this account should have a minimum of privileges.

Backup Account/User in
figure [\[fig:storage:bc3588fa57ce42d49533e35c92532faf\]](#fig:storage:bc3588fa57ce42d49533e35c92532faf).

  - Dedicated account
    
      - *Minimize risk* if compromised
    
      - *Traceability*

  - Minimum privileges
    
      - Must be able to *read all files*
    
      - Must be able to *read "files in use"*

On Linux this is most easily done by creating an account with the
password field set to an "invalid character" like `!` or `*`) and the
account’s login shell set to `/usr/sbin/nologin`. In addition, the
backup binary executable that will run in the security context of this
account can get a special privilege that allows it to read all files in
the file system. This can be done for e.g. the Restic-binary with
something like  
`setcap cap_dac_read_search=+ep /usr/bin/restic`  
(a problem still exists with this approach in that this `cap_dac_read`
capability must be set any time the Restic-binary is updated).

On Windows this is unfortunately a bit harder. The parallel to Linux
would be to create a user that is disabled and without a password:  
`New-LocalUser -Name BackupMysil -Disabled -NoPassword`  
seBackupPrivilege in
figure [3.6](#fig:storage:13a8c8b4263342419f40473b5953ef9a).

![seBackupPrivilege.<span label="fig:storage:13a8c8b4263342419f40473b5953ef9a"></span>](/home/erikhje/office/kurs/secsrv/03-storage/tex/../img/seBackupPrivilege.png)

The user `BackupMysil` should then be given a special *privilege* called
*seBackupPrivilege* to be able to read all files on the file system. On
Windows there exists by default a group called "Backup Operators":

    PS> Get-LocalGroup -Name 'Backup Operators' | Select-Object -Property *
        
    Description     : Backup Operators can override security restrictions for 
                      the sole purpose of backing up or restoring files
    Name            : Backup Operators
    SID             : S-1-5-32-551
    PrincipalSource : Local
    ObjectClass     : Group

This group also has the privilege *seRestorePrivilege* which basically
is the right to write anywhere in the file system and we don’t want
that. If an attacker is able to compromise a process running under an
account with the *seRestorePrivilege*, they will be able to own the
system (take control of everything, become Administrator). We can verify
which groups and users have which privileges with the Sysinternals tool
`AccessChk` (notice how BackupMysil only has seBackupPrivilege while the
Backup Operators group also have the seRestorePrivilege):

    PS> accesschk.exe -a seBackupPrivilege
      
              BUILTIN\Backup Operators
              BUILTIN\Administrators
              DC1\BackupMysil
    
    PS> accesschk.exe -a seRestorePrivilege
        
              BUILTIN\Backup Operators
              BUILTIN\Administrators
      
    # can also verify with
    secedit /export /areas USER_RIGHTS /cfg OUTFILE.CFG
    # and a logged in user can check its own privileges with
    whoami /PRIV

On Windows there also exists a special service that can be used for
reading "files in use". This service is called [Volume Shadow Service
(VSS)](https://docs.microsoft.com/en-us/windows-server/storage/file-server/volume-shadow-copy-service)
and [from the
documentation](https://docs.microsoft.com/en-us/windows/win32/vss/security-considerations-for-writers)
it should be possible to let an account use it by adding the account
name to the registry with a value of `1`:

    PS> Get-Item HKLM:\SYSTEM\CurrentControlSet\Services\VSS\VssAccessControl\
    
    Name                           Property
    ----                           --------
    VssAccessControl               NT Authority\NetworkService : 1
                                   DC1\mysil                   : 1

Turning theory into practice is sometimes too hard. Unfortunately your
teacher have not been able to get this setup to work\[3\]. As you will
see in the exercises, we run the backup process under the `NT
Authority\SYSTEM` account which has a [full set of permissions to all
files](https://docs.microsoft.com/en-us/windows/security/identity-protection/access-control/local-accounts#system)
in the file system.

### Important features

#### Encryption

Encryption in
figure [\[fig:storage:1f764c79088742c4a40809ba45e3a194\]](#fig:storage:1f764c79088742c4a40809ba45e3a194).

    PS> Get-Content confidential.txt 
    Some company secrets...

    PS> openssl enc -aes-256-cbc -a -pbkdf2 -in `
      confidential.txt -out confidential.txt.enc 
    enter aes-256-cbc encryption password:
    Verifying - enter aes-256-cbc encryption password:

    PS> Get-Content confidential.txt.enc 
    U2FsdGVkX1/1fC3YTahayHjr4Yy3G1byPrli5icqTwo4LHF...

Some of the data we need to back up are confidential, and we need to
protect it during transit and in rest (in storage). The best way to do
this is to encrypt it as early as possible, meaning encrypt it on the
client side before transmitting it over the network (although the
network connection is probably encrypted as well).

#### Compression

Compression in
figure [\[fig:storage:6b542599e10c4105b82e8467ccc1858a\]](#fig:storage:6b542599e10c4105b82e8467ccc1858a).

  - Lossless compression (not lossy like jpeg-images)

  - *Compress these 32 bits (4 Bytes) into 1 Byte?*  
    `10111011 10111011 10111011 10111011`

  - Same information content with Run-length encoding: `10001011`

This is just a simple example of compression, so we know how it is
possible to compress data. All files (unless already compressed) contain
repeating patterns of some kind that can be encoded more efficiently
that what is done in regular file storage.

#### Deduplication

Deduplication in
figure [3.7](#fig:storage:0f138834515b4b7196cbd1fc20ec67fa).

![Deduplication.<span label="fig:storage:0f138834515b4b7196cbd1fc20ec67fa"></span>](/home/erikhje/office/kurs/secsrv/03-storage/tex/../img/dedup.pdf)

Deduplication is similar to compression but on a much higher level.
Deduplication identifies high level data structures, typical entire data
blocks (or in very primitive implementations: entire files), and only
store one copy of each unique data structure.

## Secure Storage and Restore

### Verifying

Verifying in
figure [\[fig:storage:e2b0f8a63130489a96cb0d12a80df856\]](#fig:storage:e2b0f8a63130489a96cb0d12a80df856).

  - Structural consistency and integrity
    
      - `restic -r restic-repo check`

  - Integrity of the actual data
    
      - `restic -r restic-repo check --read-data`

A backup copy is not much worth if we cannot trust it. If the actual
storage device where the backup is located has been damaged (e.g. a
faulty RAID-volume) we can detect this by checking structural
consistency which means to check all the files used by the backup system
itself (index files and other data structures). If we are really unlucky
it can happen that an attacker have gained access to the backup storage
and written malware into the actual files (hoping that we will do a
restore and thereby spread the malware). To protect against this we have
to actually check the contents of the files (if it has been changed and
does not match e.g. hash sums stored in the metadata, and of course this
metadata can also be compromised in the attack…). This can also include
actual "virus scanning" of the backed up files. E.g. when you are
involved in recovering from a ransomware attack, how do you really know
that the ransomware is not present in the backup (which of the snapshots
do you trust? How far back in time do you have to go?).

### Restore

Restore in
figure [\[fig:storage:9e8103e1e6ab45859a4cb3e93d207ce3\]](#fig:storage:9e8103e1e6ab45859a4cb3e93d207ce3).

  - Restore everything?

  - Restore subdirectories only?

  - Browse and select files to restore?

Doing a full restore can be very expensive (time-consuming). Make sure
you use a backup system that provides flexible restore options.

## Review questions and problems

1.  (**KEY PROBLEM**) Consider the setup we have done with Restic in the
    lab tutorial. This does not follow the ideal setup with respect to
    user accounts and permissions/privileges. In what way is our setup
    bad? In what we is our setup good?

2.  Write a command line that will show all volumes that have more than
    5GB size remaining.

3.  Write a command line that will output the following (note: sorted by
    size)
    
        DiskNumber PartitionNumber        Size
        ---------- ---------------        ----
                 1               3       66048
                 1               1    16693760
                 2               1    16759808
                 2               2  4294967296
                 2               3  6424625152
                 1               2 10719592448
                 0               1 32210157568

4.  (**KEY PROBLEM**) Before initiating a backup scheme, we need to know
    a bit about the data we would like to back up. Create PowerShell
    command lines to answer the following questions:
    
    1)  How much space does your home directory use including all files
        and directories?
    
    2)  Which file is the largest? Which is the smallest?
    
    3)  How many files are there? What is the average file size?
    
    4)  How many files changed and how much space did those files use in
        total, during the last hour? last day? last week? (hint: see
        [examples in the Sort-Object section of the PowerShell
        tutorial](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#sort-object))
    
    5)  (Advanced) Which of the last seven days had the largest change?
        (largest meaning total space used by all files that was changed
        during those 24 hours)
    
    (tip: remember that PowerShell is cross-platform, so maybe try this
    out on your own laptop to have a more realistic setting than what we
    have in our virtual machines.)

5.  **Automate backup**. Do the lab tutorial. The goal of this exercise
    is to wrap Restic in a PowerShell script and let it run every half
    hour by using Task Scheduler.
    
    1)  Create a new folder `scripts` in your home directory and in this
        folder create
        
          - a file `excludes.txt` with all the file patterns you want to
            exclude from your backup (see example earlier in this
            chapter)
        
          - a file `mybackup.ps1` where you set all environment
            variables and run the backup command from the lab tutorial  
            (hint: use `$env:RESTIC_PASSWORD` to set the repository
            password)
    
    2)  At the end of your script, add commands for deleting the
        environment variables containing secrets (think "just in time",
        these secrets are only needed when restic is executed).
    
    3)  Schedule the script to run every half hour with the following
        commands (must be in a PowerShell as Administrator):
        
            $taskName = "MyBackup"
            $taskDescription = "Restic backup of Users data"
                
            $taskAction = New-ScheduledTaskAction `
              -Execute 'C:\Program Files\PowerShell\7\pwsh.exe' `
              -Argument '-File C:\Users\Admin\scripts\mybackup.ps1'
                
            $taskTrigger = New-ScheduledTaskTrigger `
              -Once -At (Get-Date) `
              -RepetitionInterval (New-TimeSpan -Minutes 30) `
              -RepetitionDuration (New-TimeSpan -Days (7))
            
            # Note: Really should use another account, but
            # too hard to get it to work...
            $taskPrincipal = New-ScheduledTaskPrincipal `
              -UserId "NT AUTHORITY\SYSTEM" `
              -LogonType ServiceAccount
                
            Register-ScheduledTask `
              -TaskName $taskName `
              -Description $taskDescription `
              -Action $taskAction `
              -Trigger $taskTrigger `
              -Principal $taskPrincipal
    
    4)  Run your registered task manually once using
        [Start-ScheduledTask](https://docs.microsoft.com/en-us/powershell/module/scheduledtasks/start-scheduledtask).
    
    5)  Study the [Restic
        manual](https://restic.readthedocs.io/en/latest/040_backup.html#comparing-snapshots)
        and run a command to show the difference between two of your
        snapshots.
    
    6)  Use
        [Get-ScheduledTaskInfo](https://docs.microsoft.com/en-us/powershell/module/scheduledtasks/get-scheduledtaskinfo)
        to print only TaskName, LastTaskResult and NextRunTime for your
        registered task.
    
    7)  Use
        [Get-ScheduledTask](https://docs.microsoft.com/en-us/powershell/module/scheduledtasks/get-scheduledtask)
        to print only the path to pwsh.exe for your registered task
        (hint: this is the property Execute "under" the
        Actions-property, use Get-Member to see that the
        Actions-property is not just a string).
    
    8)  Open PowerShell as Administrator, create the directory  
        `C:\restoretest` and [do a restore with
        restic](https://restic.readthedocs.io/en/latest/050_restore.html)
        of your latest snapshot to this directory.

## Lab tutorials

1.  **Attach a block device on D:**. This exercise is not mandatory, but
    if you are unsure what a block device is and how disks and file
    systems show up in Windows, then do it.
    
    1.  Login to SkyHiGh, click "Volumes", "Volumes", "Create Volume",
        give it any name you want and leave it to 1GB in size.
    
    2.  To the right of your newly created volume click "Edit Volume",
        "Manage Attachments", in "Attach to Instance" choose the server
        you would like to attach the volume to.
    
    3.  On the server do the following to make this volume accessible on
        `D:`:
        
            # Find disk number (in PowerShell):
            Get-Disk
            # Initialize
            Initialize-Disk DISK_NUMBER
                
            # Partition and format
            New-Partition -DiskNumber DISK_NUMBER -UseMaximumSize `
              -AssignDriveLetter | Format-Volume
                
            # View
            Get-PSDrive
    
    4.  If you want to "safely remove" the attached volume, do
        
            # Unmount (if Drive has letter 'D')
            Get-Volume -Drive D | Get-Partition |
              Remove-PartitionAccessPath -AccessPath D:\

2.  **Backup to (immutable) object storage**. Let’s try to set up the
    backup software Restic on a Windows Server and use OpenStack’s
    object storage (Swift) as backend (storage repository for the
    backups).
    
    1.  If you don’t already have a Windows Server to use, create a new
        stack and log in to the server (see lab tutorial in week one).
    
    2.  Create the object store container where the backups will be
        stored. Login to SkyHiGh, click "Object Store", "Containers",
        "+Container", name it `mysil` (you can name it whatever you
        want, but if you name it mysil you can copy and paste more of
        the text later).
    
    3.  Many times when we need to automate tasks, we need to have
        authentication, which typically means putting a password into a
        script. Password in scripts or configuration files is not
        something we want, but we cannot always avoid this, so the best
        we can do is use a separate account with minimum privileges. For
        the current task, let’s create a separate account which is valid
        only for a short time period (e.g. the month of January). In
        SkyHiGh, click "Identity", "Application Credentials", "Create
        Application Credential":
        
          - Choose a name, e.g. "backupcreds"
        
          - Set expiration date to last day of January.
        
          - Click on role "\_member\_".
        
          - Click "Create Application Credentials"
        
          - Click "Download openrc file". In this file you will find the
            ID and the Secret (the "password").
    
    4.  Inspect [Restic
        package](https://community.chocolatey.org/packages/restic) (does
        it look safe to install?), Open PowerShell as Administrator,
        install Restic with  
        `choco install -y restic`.
    
    5.  Open PowerShell (not as Administrator), set the necessary
        environment variables (replace `YOUR_ID` and `YOUR_SECRET`):
        
            $env:OS_AUTH_TYPE='v3applicationcredential'
            $env:OS_AUTH_URL='https://api.skyhigh.iik.ntnu.no:5000/v3'
            $env:OS_IDENTITY_API_VERSION='3'
            $env:OS_REGION_NAME="SkyHiGh"
            $env:OS_INTERFACE='public'
            $env:OS_APPLICATION_CREDENTIAL_ID='YOUR_ID'
            $env:OS_APPLICATION_CREDENTIAL_SECRET='YOUR_SECRET'
    
    6.  Initialize your backup repository (you will now need to provide
        a password of your choice, which is used for encrypting your
        backups to protect them) with  
        `restic -r swift:mysil:/ init`
    
    7.  Confirm that your backup repository is empty (it does not have
        any snapshots) with  
        `restic -r swift:mysil:/ snapshots`
    
    8.  Open PowerShell as Administrator (so we can use the Volume
        Shadow Service (VSS) to also backup files that are in use),
        create a backup of your home directory with  
        `restic -r swift:mysil:/ backup $HOME --use-fs-snapshot`  
        (remember that you have to set our environment variables
        whenever you open a new PowerShell session)
    
    9.  Confirm that your backup repository now have one snapshot with  
        `restic -r swift:mysil:/ snapshots`

# Git, Markdown and CI/CD

## TL;DR

TL;DR in
figure [\[fig:git:bbd0af0803074bee9f20ae7f08cdc4f9\]](#fig:git:bbd0af0803074bee9f20ae7f08cdc4f9).

  - Watch the 20-minute [Git-Lecture from
    IfI,UiO](https://youtu.be/vWdmXunGQC8) (from the course
    [IN3110](https://uio-in3110.github.io))

  - Study the [Git Cheat
    Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)

  - Study the [Markdown Cheat
    Sheet](https://www.markdownguide.org/cheat-sheet) and browse the
    [GitLab Flavored
    Markdown](https://docs.gitlab.com/ee/user/markdown.html)

  - Figure out what
    [.gitlab-ci.yml](https://gitlab.com/erikhje/heat-mono/-/blob/master/.gitlab-ci.yml)
    is used for

Note: from the cheat sheet (and the video), you need to learn properly:

  - 01 Git configuration

  - 02 Starting A Project

  - 03 Day-To-Day Work (not `git stash`)

  - 05 Review your work (only `git log`)

  - 08 Synchronizing repositories (not `git fetch`)

  - D The zoo of working areas (don’t worry about stash or branches)

  - theory of file stages

  - and later how to deal with conflicts

## Version Control with Git

It’s not a joke in
figure [4.1](#fig:git:25b99da7641449d7bdb560d7d7aa952a).

![It’s not a
joke.<span label="fig:git:25b99da7641449d7bdb560d7d7aa952a"></span>](/home/erikhje/office/kurs/secsrv/04-git/tex/../img/git.png)

Everyone in IT uses git, but very few really know the details. You do
memorize a few commands and look up the details when you need them. Note
that Git was created in 2005 by Linus Torvalds together with other Linux
kernel developers with the purpose of having a better version control
system for the source code of the Linux operating system kernel .

### Repository

An Empty but Initialized Repo in
figure [4.2](#fig:git:3f074971502b400b9241cfcf251fa1ff).

![An Empty but Initialized
Repo.<span label="fig:git:3f074971502b400b9241cfcf251fa1ff"></span>](/home/erikhje/office/kurs/secsrv/04-git/tex/../img/gittree.png)

Git is two things: 1. the Git repository (repo) and 2. the Git program.
The Git repo is just a directory with one special hidden directory:
`.git`. This is the directory where all the git data structures are
stored, and we typically never edit these files manually, they are
changed by using the Git program.

### File States

Four States of a File in
figure [4.3](#fig:git:fd2f260255584a699b3f7aafeff89a12).

![Four States of a
File.<span label="fig:git:fd2f260255584a699b3f7aafeff89a12"></span>](/home/erikhje/office/kurs/secsrv/04-git/tex/../img/gitstates.pdf)

You can have a file in the git repo that git does not know about. This
is called an *Untracked* file. For the files that have been added to git
(the *tracked* files) they can be *Unmodified* like `README.md` and
`test.ps1` in the figure, or *Modified but not staged* like `b.txt`, or
they can be newly added or modified and *Staged*.

Modified but not staged means they will not be part of the next commit
if you do `git commit -m "msg"`, but they will be added to the staged
state and committed if you do `git commit -am "msg"`. The manual of
git-commit states about the option `-a` or `–all`:

> Tell the command to automatically stage files that have been modified
> and deleted, but new files you have not told Git about are not
> affected.

All files that are *Staged* will be part of the next commit (the next
version of the repo).

### Workflow

Typical Workflow in
figure [\[fig:git:cf93b98b50294180b4a0a432c389a9f7\]](#fig:git:cf93b98b50294180b4a0a432c389a9f7).

1.  `git pull origin main`

2.  edit a file

3.  `git commit -am "docs: spellcheck Foo"`  
    *[conventionalcommits.org](https://www.conventionalcommits.org/)\!*

4.  `git push origin main`

When you work on a project you pull the latest version, make small
changes, commit and push the changes into the shared repo. Note: it is
important to understand that there is nothing special about the
repository in GitLab or GitHub. They are just the same git repo as you
have locally. Git is fully decentralized. It is just convenient to have
one repository that we share and can push to.

Always think that others will at some point in time take over your code.
Try to write readable high quality code and documentation, and also try
to put some effort into making sensible commit messages: browse the web
page  
[https://www.conventionalcommits.org](https://www.conventionalcommits.org/)  
and try to follow that standard when you write commit messages.

### Conflicts

Conflicts in
figure [\[fig:git:2edaa1e2de0e4fca8c72dddd9b0a111b\]](#fig:git:2edaa1e2de0e4fca8c72dddd9b0a111b).

  - *Avoid them by working on separate files if possible.*

  - Browse [Merge
    conflicts](https://docs.gitlab.com/ee/user/project/merge_requests/conflicts.html)
    if git does not resolve them automatically

It can happen that two or more people make changes to the same files,
and then try to push these changes. Git then either resolves this if the
changes are in different parts of the file, but if they are overlapping
changes you have to deal with them manually.

## Markdown

The Magic of "View Source" in
figure [4.4](#fig:git:57341576b22849b3a3f1182ea00c1e0c).

![The Magic of "View
Source".<span label="fig:git:57341576b22849b3a3f1182ea00c1e0c"></span>](/home/erikhje/office/kurs/secsrv/04-git/tex/../img/mdsource.png)

Markdown is something you learn by spending 10 minutes reading the
[Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/) (the
"Basic Syntax" takes you a long way), looking up [GitLab Flavored
Markdown](https://docs.gitlab.com/ee/user/markdown.html) when you want
to do something fancy, and otherwise just studying the source code of
others. Markdown is much simpler than other markup languages, and is to
go-to language for any quick notes or documentation you need to write
(especially in context of a git repo).

## CI/CD

Waterfall (OLD\!) in
figure [4.5](#fig:git:6c0590e57fac46319e7b30f03545ce53).

![Waterfall
(OLD\!).<span label="fig:git:6c0590e57fac46319e7b30f03545ce53"></span>](/home/erikhje/office/kurs/secsrv/04-git/tex/../img/waterfall.pdf)  
<span>Peter Kemp / Paul Smith, [CC
BY 3.0](https://creativecommons.org/licenses/by/3.0), via Wikimedia
Commons</span>

When the author was taught software engineering in 1993, Waterfall was
the dominant model. Waterfall is a sequential model with the steps
Requirement, Design, Implementation, Verification, Maintenance (or some
variation thereof). In other words, it leads to careful and slow
development processes which would lead to a final product being
implemented in production maybe once or twice per year. It did not
facilitate close cooperation between *Developers* (software engineers
and programmers) and *Operations* (the system and network
administrators).

DevOps (TODAY\!) in
figure [4.6](#fig:git:303f01db385448f4b06264933f0e550c).

![DevOps
(TODAY\!).<span label="fig:git:303f01db385448f4b06264933f0e550c"></span>](/home/erikhje/office/kurs/secsrv/04-git/tex/../img/devops.pdf)  
<span>Kharnagy, [CC
BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0), via
Wikimedia Commons</span>

The DevOps model which we use today facilitates close cooperation
between *Developers* and *Operations*. The model is possible much due to
virtualization and cloud computing software, since they allow for fast
and frequent changes in production. It is common today that we make
changes in production software every week or even more than once per day
in some cases. Since security has been an undervalued topic for too many
years, some people refer to *DevSecOps* as a separate model, but it is
really just about incorporating security into every step of the
DevOps-model which is something we should be doing anyway.

### Pipeline

CI/CD Pipeline in
figure [4.7](#fig:git:115afaa0e92b41488c877a2cb5ab5a0c).

![CI/CD
Pipeline.<span label="fig:git:115afaa0e92b41488c877a2cb5ab5a0c"></span>](/home/erikhje/office/kurs/secsrv/04-git/tex/../img/pipeline.jpg)  
<span>Jouasse, [CC
BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0), via
Wikimedia Commons</span>

See our example
[.gitlab-ci.yml](https://gitlab.com/erikhje/heat-mono/-/blob/master/.gitlab-ci.yml)

To make DevOps happen, with frequent changes in production software, we
have to automate everything that we can automate. This applies
specifically to testing in all shapes and sizes. We should automate
testing of code quality (code comments, code style, best practice
checks), bugs and bad code, security and maybe even do performance
testing. Testing is done in a sequence of steps, this is called a *CI/CD
pipeline*. CI is short for *Continuous Integration* while CD is short
for either *continuous delivery* (mostly used by developers) or
*continuous deployment* (mostly used by operations). This is a quite
complicated topic, and you will learn more about it in later courses.
The basics we stick with now is that we set up a test (a CI/CD-pipeline
with just one step) that runs every time we push our git changes to the
central git repository. The only thing this pipeline does is check if
our PowerShell scripts passes the
[PSScriptAnalyzer](https://github.com/PowerShell/PSScriptAnalyzer) (a
*static code checker* for PowerShell) without it triggering any
warnings.

## Review questions and problems

1.  The file `file.md` is in state *Unmodified* in a git-repo. What
    happens when you run the command `git rm file.md`?

2.  (**KEY PROBLEM**) Create a new directory, change into it and create
    an empty git repo with  
    `git init`  
    Create the following status in the git repo (in other words: create
    the four files and run git commands so the status ends up being just
    like this):
    
        PS> Get-ChildItem *.txt | Format-Table -Property Name
        
        Name
        ----
        a.txt
        b.txt
        c.txt
        d.txt
        
        PS> git status
        
        On branch main
        Your branch is ahead of 'origin/main' by 1 commit.
        (use "git push" to publish your local commits)
        
        Changes to be committed:
        (use "git restore --staged <file>..." to unstage)
              modified:   a.txt
              new file:   c.txt
        
        Changes not staged for commit:
        (use "git add <file>..." to update what will be committed)
        (use "git restore <file>..." to discard changes in working directory)
              modified:   b.txt
        
        Untracked files:
        (use "git add <file>..." to include in what will be committed)
              d.txt

## Lab tutorials

1.  Install git with `choco install -y git` (as always, [check if
    package is OK
    first](https://community.chocolatey.org/packages/git)). Do "01 Git
    configuration" from the [Git Cheat
    Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf).

2.  **Shared and Local Repo**.
    
    1.  Log in on <https://gitlab.stud.idi.ntnu.no> and create a new git
        repository with "New Project", "Create blank project", name it
        mysil, (leave it private, and leave it to init with README),
        "Create project". You now have a remote git repo which you want
        to have a copy of locally AND be able to make changes and upload
        them *without using a password every time*. The following adds a
        ssh public-key to your account. If you want to have a different
        setup, e.g. a separate key for just your repo, then [read the
        docs](https://docs.gitlab.com/ee/ssh/).
    
    2.  (Do this on your laptop or on a Windows host in SkyHiGh)  
        `ssh-keygen -t rsa -b 2048 -C "gitlab.idi projects"`  
        Name the file as proposed (if you use another name, you have to
        setup a config file) and leave a blank password (this means that
        if anyone gets hold of your private key, they will have write
        access to your repo).  
        `Get-Content $HOME\.ssh\id_rsa.pub | Set-Clipboard`
    
    3.  (Do this in GitLab) Click your avatar (top right corner),
        "Preferences", "SSH keys" and paste your SSH public key which
        you just put on your clipboard.
    
    4.  (Do this on your laptop or on a Windows host in SkyHiGh)  
        
            git clone git@gitlab.stud.idi.ntnu.no:YOUR_NTNU_USERNAME/mysil.git
            cd mysil
            git status
            Write-Output "A small edit" >> README.md
            Write-Output "# Script TBA" > test.ps1
            git status
            git add test.ps1
            git status
            git commit -am "Added a file and a small edit"
            git status
            git push origin main
    
    5.  Visit your project in GitLab to see the changes and view
        "History" to see the commits (versions).

3.  **Conflicts**. Add some of your fellow students to your newly
    created git project, have everyone clone the repo, make changes to
    the same file and push those changes. Study [Merge
    conflicts](https://docs.gitlab.com/ee/user/project/merge_requests/conflicts.html)
    and try to solve the conflicts that probably appears.

# Active Directory: DNS, LDAP and Kerberos

## Active Directory

Active Directory in
figure [5.1](#fig:adtech:77c0b3b0d9ad44d18792400a5de4a8f1).

  - LDAP and Kerberos (and (Dynamic) DNS)

  - Forests, (Trees), Domains and OUs: Users and Hosts  
    *Scales to really large organization\!*

![Active
Directory.<span label="fig:adtech:77c0b3b0d9ad44d18792400a5de4a8f1"></span>](/home/erikhje/office/kurs/secsrv/05-adtech/tex/../img/ad-users-and-computers.png)

Active Directory is a special database which is a core component of most
companies IT infrastructure. It is where the accounts (usernames and
passwords) are stored, and all resources (laptops, workstations,
servers, shared file systems, printers, etc.) in your network that you
want to manage with Windows-based management tools. Active Directory is
an implementation of the LDAP and Kerberos protocols, and it is also
strongly dependent upon DNS, so let’s use this chapter to become
familiar with those three technologies before we move on to actually
using Active Directory.

## DNS

DNS - What is it? in
figure [\[fig:adtech:68cbe978ffb24a21bb78f28a60a2fbd9\]](#fig:adtech:68cbe978ffb24a21bb78f28a60a2fbd9).

  - DNS is a distributed database with (key,value) pairs

  - DNS provides the following primary services
    
      - name-to-IPaddr mapping (A or AAAA)
    
      - (IPaddr-to-name mapping (PTR))
    
      - aliases (CNAME)
    
      - mail routing (MX)

  - Also be used for
    
      - Other lookups (SRV records, certificates, etc.)  
        *Service Discovery* in Windows domains, e.g. where is login
        server?
    
      - Load distribution
    
      - RBL/SPF (spam prevention)
    
      - Learn about your network…
    
      - Passive DNS
    
      - DNSSEC with SSHFP, DANE, TLSA, ...

The Domain Name System (DNS) is known as the *glue* of the Internet. We
hear about how DNS is involved in major outages and issues every year.
In 2021, Facebook (now Meta) disappeared from the Internet, and in their
own words :

> One of the jobs performed by our smaller facilities is to respond to
> DNS queries. DNS is the address book of the internet, enabling the
> simple web names we type into browsers to be translated into specific
> server IP addresses. Those translation queries are answered by our
> authoritative name servers that occupy well known IP addresses
> themselves, which in turn are advertised to the rest of the internet
> via another protocol called the border gateway protocol (BGP).
> 
> To ensure reliable operation, our DNS servers disable those BGP
> advertisements if they themselves can not speak to our data centers,
> since this is an indication of an unhealthy network connection. In the
> recent outage the entire backbone was removed from operation, making
> these locations declare themselves unhealthy and withdraw those BGP
> advertisements. The end result was that our DNS servers became
> unreachable even though they were still operational. This made it
> impossible for the rest of the internet to find our servers.

A haiku about DNS in
figure [5.2](#fig:adtech:fd4f4721516c4bb4abf0754dd3d27819).

![A haiku about
DNS.<span label="fig:adtech:fd4f4721516c4bb4abf0754dd3d27819"></span>](/home/erikhje/office/kurs/secsrv/05-adtech/tex/../img/haiku.jpg)  
<span>[@nixcraft - The Best Linux Blog In the
Unixverse](https://twitter.com/nixcraft/status/1377751311584620545)</span>

DNS is known to be the cause of many problems. Every experienced system
administrator has at some point in time been troubleshooting a problem
not believing it to be a DNS issues, and after a long time and much
headache, it would turn out to be a DNS issue after all.

DNS History in
figure [\[fig:adtech:9aa45eb5cd214f1aaa44eb50de90f66b\]](#fig:adtech:9aa45eb5cd214f1aaa44eb50de90f66b).

  - 1971  
    RFC226, HOSTS.TXT (Peggy Karp)

  - 1981  
    RFC799, DNS concepts (David Mills)

  - 1982  
    RFC819, DNS structure (Zaw-Sing Su & Jon Postel)

  - 1983  
    RFC882/883, Hostname lookup, authority and delegation (Paul
    Mockapetris)

  - 1984  
    RFC920, Outline of work to be done and  
    TLDs/TopLevelDomains (Jon Postel)

  - 1985  
    Start of DNS, first name registered (symbolics.com or think.com)

DNS History in
figure [\[fig:adtech:ee500de2c5cf4f9994dc27a95fe54fe4\]](#fig:adtech:ee500de2c5cf4f9994dc27a95fe54fe4).

  - A bunch of RFCs has followed  
    (`http://www.dns.net/dnsrd/rfc`)

  - A huge “war” of companies and academia also followed, the problem
    solved today

  - Names can be registered at one of the “ICANN Accredited Registrars”

  - In Norway: NORID responsible for `.no` (and accredits registrars)
    (<http://www.norid.no>)

### Software

DNS software in
figure [\[fig:adtech:ec3d7d41ddde4204986d1d84ec89145e\]](#fig:adtech:ec3d7d41ddde4204986d1d84ec89145e).

  - A DNS server is usually divided into *a resolver/cache* and *an
    authoritative server*. The following servers are common
    
      - Bind 9
    
      - Microsoft DNS
    
      - [Many
        others...](https://en.wikipedia.org/wiki/Comparison_of_DNS_server_software)

  - To query DNS servers we usually use the `dig` program on Linux and
    the `Resolve-DnsName` cmdlet in PowerShell (or `nslookup` on either
    platform)

<!-- end list -->

    PS> Resolve-DnsName example.com
    
    Name          Type   TTL   Section    IPAddress
    ----          ----   ---   -------    ---------
    example.com   AAAA   444   Answer     2606:2800:220:1:248:1893:25c8:1946
    example.com   A      865   Answer     93.184.216.34

Domains and Zones in
figure [5.3](#fig:adtech:6725db9689b14216bd8051aba16f9b3f).

![Domains and
Zones.<span label="fig:adtech:6725db9689b14216bd8051aba16f9b3f"></span>](/home/erikhje/office/kurs/secsrv/05-adtech/tex/../img/dns-zones.pdf)  
<span>Public domain, via Wikimedia Commons</span>

The concepts *domain* and *zone* can be a bit confusing. A domain is
everything under `.no`, but the `.no` zone excludes all its subdomains
which it has delegated. E.g. `ntnu.no` is in the `.no` domain but not in
the `.no` zone. An *authorative DNS server* is in charge of a zone, not
a domain. When we talk about the `ntnu.no`-domain we include all names
that end with `ntnu.no`, but `ntnu.no` and `iik.ntnu.no` are two
different zones. But note also that if we at IIK did not run our own
authorative name server, we could let the `ntnu.no` authorative name
servers be in charge of all names ending in `iik.ntnu.no`, and in that
case `ntnu.no` and `iik.ntnu.no` would be in the same zone. A list of
things to know about DNS:

  - e.g hostname is `www`, domainname is `ntnu.no`,  
    Fully Qualified Domain Name (FQDN) is `www.ntnu.no`

  - Names are case-insensitive and must follow the *LDH-rule* (Letters,
    Digits and Hyphen), but since 2009 it has been possible to use
    Unicode-characters in all names, allowing e.g. the use of the
    norwegian letters æ, ø and å (whether it is a good idea to use these
    letters in your domain name is a different question). The mechanism
    behind allowing this is called Punycode  and maps Unicode to the
    LDH-rule *in the application before making a DNS query*. In other
    words, it is not a part of the DNS system itself (DNS still requires
    the LDH-rule), try to enter
    [blåbærsyltetøy.no](http://blåbærsyltetøy.no) in your browser
    (probably works) and then try to do  
    `Resolve-DnsName blåbærsyltetøy.no` (probably does not work)

  - Sometimes in DNS context we have to specify the top node of the DNS
    hierarchy as well (the dot at the end): `www.ntnu.no.`

  - The top level node has a series of root servers which has
    information about all the TLDs

  - The current TLDs can be found at `  `   
    `https://www.icann.org`

  - Information about the root servers can be found at  
    `https://root-servers.org`

  - The thirteen root servers are usually listed in a configuration file
    in the DNS server (e.g `C:\Windows\ System32\dns\cache.dns` in
    Microsoft DNS and `named.root` in Bind) which can be retrieved from
    `https://www.internic.net/domain/named.root`

### DNS Query

How a Query Works in
figure [\[fig:adtech:cc09e38a6c224727ad7add36bbddd526\]](#fig:adtech:cc09e38a6c224727ad7add36bbddd526).

  - DNS works by passing around *Resource Records (RRs)* through port 53
    (TCP if larger than 512 bytes; else UDP)

  - The most important resource records are
    
      - SOA  
        Start Of Authority (accepts a delegation)
    
      - NS  
        Name Server (delegates a zone)
    
      - MX  
        Mail eXchanger (defines the mail server)
    
      - A  
        Address, define the canonical name of an IP address
    
      - CNAME  
        Canonical Name, define an alias
    
      - PTR  
        PoinTer Record, define the reverse mapping (the IP address of a
        fqdn)
    
      - SRV  
        Service record, which hosts and ports have the service

A resource record (which is what we call the DNS data packet, which is
in most cases carried in a UDP packet) always contains

  - Name  
    e.g. the name we want translated to IP address

  - Type  
    e.g. A

  - Value  
    “the answer”, e.g. the IP address

  - TTL  
    Time-To-Live, the time interval that the resource record may be
    cached before it should be retrieved again from the authoritative
    server

E.g. if we want to find out which server(s) accept email for the domain
`ntnu.no` or which port number we use for LDAP TCP-queries in our
`sec.core` domain:

    PS> Resolve-DnsName -Type MX ntnu.no
    
    Name     Type   TTL   Section    NameExchange    Preference
    ----     ----   ---   -------    ------------    ----------
    ntnu.no  MX     39    Answer     mx.ntnu.no      10
    
    Name       : mx.ntnu.no
    QueryType  : A
    TTL        : 101
    Section    : Additional
    IP4Address : 129.241.56.67
      
    Name       : mx.ntnu.no
    QueryType  : AAAA
    TTL        : 255
    Section    : Additional
    IP6Address : 2001:700:300:3::67
    
    
    PS> Resolve-DnsName -Type SRV _ldap._tcp.sec.core |
          Select-Object -Property Name,IP4Address,Port
    
    Name                IP4Address      Port
    ----                ----------      ----
    _ldap._tcp.sec.core                  389
    dc1.sec.core        192.168.111.103

A query for a RR can be either *recursive* (“do whatever you can to
resolve this”, this is what a client sends to a resolver) or *iterative*
(“please answer me this without asking anyone else”, this is what a
resolver send to an authoritative server). In the DNS data packet there
is a bit called RD which is either one or zero if it is a recursive or
an iterative query respectively.

A host can have multiple names, e.g. in a small organization you might
have a server running smtp and imap services and have the names
`smtp.example.com`,  
`imap.example.com`, `mail.example.com` and `mikke.example.com`. `mikke`
is probably the original name of the server (sometimes called the
canonical name), while `smtp`, `imap` and `mail`, are names identifying
the service the host offers. They should be implemented either as
additional `A` records or as `CNAME`s. Implementing them as `A` records
is the most efficient since `CNAME`s leads to twice as many lookups (a
`CNAME` maps to an `A` record, so you have to look up the `A` record to
find the IP address), but `CNAME`s makes it easier to understand which
name is a service name and which is the canonical name. Separating names
into canonical hostname and service names is a good idea since you might
have to move the service to a different host in the future.

Interaction of DNS servers in
figure [5.4](#fig:adtech:c1a342fb1df8456dbd03065ffdbf7474).

![Interaction of DNS
servers.<span label="fig:adtech:c1a342fb1df8456dbd03065ffdbf7474"></span>](/home/erikhje/office/kurs/secsrv/05-adtech/tex/../img/dns-resolve.pdf)  
<span>Lion Kimbro, Public domain, via Wikimedia Commons</span>

Typical sequence is

  - Look in hosts-file (`%SystemRoot%\system32\drivers\etc\hosts` on
    Windows)

  - Look in local config for which resolver to use  
    (`Get-DnsClientServerAddress`)

  - Ask the name resolver a recursive query for `www.wikipedia.org`

  - (1) The name resolver checks to see if it has the `A` record for  
    `www.wikipedia.org`, or `wikipedia.org` or `.org`, it does not, so
    it has to contact (send an iterative query to) one of the root
    servers

  - The root server replies with the NS records of the `.org` zone and
    their corresponding A records (glue records\!)

  - (2) It sends one of the `.org` servers an iterative query, e.g. one
    of the `.org` servers

  - The `.org` authoritative server replies with the `NS` records of  
    `wikipedia.org` along with their `A` records (glue records)

  - (3) It then sends an iterative query to one of the `wikipedia.org`
    authoritative servers which answers with the `A` record of
    `www.wikipedia.org`

Several Caches Involved in
figure [5.5](#fig:adtech:27ee00070b764d3faeae57eda9cc3643).

![Several Caches
Involved.<span label="fig:adtech:27ee00070b764d3faeae57eda9cc3643"></span>](/home/erikhje/office/kurs/secsrv/05-adtech/tex/../img/dns-caches.pdf)  
<span>Aaron Filbert, [CC
BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0), via
Wikimedia Commons</span>

Replies are cached and the TTL (Time To Live) field is important. TTL is
defined by the authoritative name server to be e.g. one hour (3600
seconds), meaning that the reply you get might not be correct if the
site have changed its authoritative DNS within the last hour. You can
see which resource records are cached on you computer with
`Get-DnsClientCache` (repeat and check TTL)

### Dynamic DNS

Dynamic DNS in
figure [\[fig:adtech:e2a8cef4a09f4c1ba4d094d6a926583f\]](#fig:adtech:e2a8cef4a09f4c1ba4d094d6a926583f).

  - *What to do with DHCP clients needing a name?*
    
      - Get a new name with every new IP address?
    
      - Always have the same name? Dynamic DNS\!

  - Practical and widely used (Active Directory, dyndns.com, ...)

  - A security problem?
    
      - *clients changing server configuration...*

Dynamic DNS means that a client can notify an authoritative DNS server
that it has a new IP address, and the server will update the clients DNS
entry to map the same name to the new IP address. This can also apply to
other kinds of DNS records (e.g. service records (SRV) in active
directory). The fundamental security problem here is the concept of a
client being able to force a configuration change on the server. This
might not be a problem, but this should ring a bell in our security
conscious minds.

### DNS security

Security and Privacy in
figure [\[fig:adtech:8b636b050f19471badf5753e1599be9e\]](#fig:adtech:8b636b050f19471badf5753e1599be9e).

  - Security, see table of contents and browse chapter two of [DNS
    Security Introduction and
    Requirements](https://tools.ietf.org/html/rfc4033)

  - Privacy, see [DNS Privacy - The
    Problem](https://dnsprivacy.org/wiki/display/DP/DNS+Privacy+-+The+Problem)

A key problem with DNS is that it is vulnerable to cache poisoning, with
the most famous vulnerability being probably [Multiple DNS
implementations vulnerable to cache
poisoning](https://www.kb.cert.org/vuls/id/800113/) (where all DNS
servers that did not randomize UDP source port were vulnerable).

DNS works by sending messages called Resource Records in UDP packets. It
uses UDP since it needs to be lightweight and simple. If a packet is
lost, we can just try again. However, there are security and privacy
concerns  over DNS and implementations of DNS over HTTPS (DoH) are being
deployed and seems to work without too big performance hit . However,
another privacy concern is introduced with DoH: if everyone starts using
[the few public providers that offers
DoH](https://github.com/curl/curl/wiki/DNS-over-HTTPS#publicly-available-servers),
they will be able to see everyones DNS lookups (instead of having them
spread across many local ISPs).

Security and privacy in DNS is major topic which you need to be aware
of, but we will not study it in detail in this course, we keep our focus
on learning how DNS works in general and also specifically how it works
in a Windows domain.

### Our Setup

Our Setup Before AD Install in
figure [5.6](#fig:adtech:fb8ada1f05fb480b87a3c21c96aa5da3).

![Our Setup Before AD
Install.<span label="fig:adtech:fb8ada1f05fb480b87a3c21c96aa5da3"></span>](/home/erikhje/office/kurs/secsrv/05-adtech/tex/../img/our-dns-setup-before.pdf)

When we install a DNS server on DC1 (which we do as part of the
installation of Active Directory), we will get this warning:

> WARNING: The following recommended condition is not met for DNS: No
> static IP addresses were found on this computer. If the IP address
> changes, clients might not be able to contact this server. Please
> configure a static IP address on this computer before installing DNS
> Server.

We can ignore this warning because of the way IP address assignment
works in cloud infrastructures: IP addresses are always assigned to
hosts through DHCP (Dynamic Host Configuration Protocol) which normally
means that they are likely to change during a host’s lifetime. However,
the standard setup in public and private clouds is that the cloud
software (OpenStack) guarantees that each host will always receive the
same IP address from the DHCP server during their entire lifetime. This
means that even though the warning message "the host does not have a
static IP address" is correct, it is not a problem because we know that
the dynamic IP address will in practice behave like a static IP address:
it will not change. E.g. in Azure :

> A virtual machine (VM) is automatically assigned a private IP address
> from a range that you specify. This range is based on the subnet in
> which the VM is deployed. The VM keeps the address until the VM is
> deleted.

Our Setup After AD Install in
figure [5.7](#fig:adtech:de43274dc0234622a43d862508f07c18).

![Our Setup After AD
Install.<span label="fig:adtech:de43274dc0234622a43d862508f07c18"></span>](/home/erikhje/office/kurs/secsrv/05-adtech/tex/../img/our-dns-setup-after.pdf)

## LDAP

What is a Directory Service? in
figure [\[fig:adtech:e01a05d53f4a40f2adeb80f04328e133\]](#fig:adtech:e01a05d53f4a40f2adeb80f04328e133).

Examples are DNS, NIS (Network Information Service), *LDAP (Lightweight
Directory Access Protocol)*

  - A simple database *optimized for reads and searches (lookups)*

  - Centralized storage of users, groups, computers, services, printers,
    mailinglists, ...

A directory is a special database which you use for finding values
stored in attributes (properties). The ratio between reads:writes in a
directory service is typically 1000:1, 10000:1 or even 100000:1. There
are very frequent searches and reads, and very rarely there are new
writes (new registrations or updates).

LDAP is a directory service similar to DNS, but it serves a different
purpose. While DNS is the glue of the Internet, LDAP (implemented in
Active Directory) is the glue for resource management inside a company
IT infrastructure. Both are directory services that we use primarily for
reads ("lookups"), but their internal structure differs.

### Structure

Lightweight Directory Access Protocol in
figure [\[fig:adtech:94405633b6274dda87c6825f9d558165\]](#fig:adtech:94405633b6274dda87c6825f9d558165).

  - LDAP is a client-server protocol for communication with a directory
    service

  - Defined in RFC4510 - RFC4533 *\!* (most important are
    RFC4511\&RFC4512)

  - LDAP directories follow the X.500 model:
    
      - a tree (a hierarchy) of directory entries (records/objects)
    
      - an entry (record/object) consists of a set of attributes
    
      - an attribute has a name and one or more values

  - *LDAP uses TCP on port 389* (or SSL-connection to 636 in addition in
    some setups)

Wait, ldaps (where ’s’ is the secure connection we like to see just like
’https’) is port 636, and we should always use the port with the ’s’ to
have a secure connection? No, not necessarily, see [Frequently asked
questions about changes to Lightweight Directory Access
Protocol](https://support.microsoft.com/en-us/help/4546509/frequently-asked-questions-about-changes-to-ldap):
*Does this mean we have to move all LDAP applications to port 636 and
switch to SSL/TLS? - No. When SASL with signing is used, LDAP is more
secure over port 389.*

Note: the actual data in the directory service can be stored in any kind
of backend, e.g. flat files or a relational database, the requirement
however is that the data have to be accessed (read, searched, updated,
added, modified, etc.) according to the LDAP protocol.

LDAP Structure in
figure [5.8](#fig:adtech:cc8ed6794fa8462aa62629adba967f41).

![LDAP
Structure.<span label="fig:adtech:cc8ed6794fa8462aa62629adba967f41"></span>](/home/erikhje/office/kurs/secsrv/05-adtech/tex/../img/rdn.pdf)

The directory tree structure (hierachical structure) is similar to a
file system. In the same way as we address file paths in a file system
with absolute or relative path (remember: absolute paths starts with a
’/’), we address entries in a directory tree with the *distinguished
name* or the *relative distinguished name*:

  - *DN = RDN + Parent’s DN*

  - DN is the unique identifier (primary key)

  - RDN is unique identifier only at its own level

  - A DN is typically composed of
    
      - DC  
        Domain Component
    
      - OU  
        Organizational Unit
    
      - CN  
        Common Name

E.g. in NTNU’s LDAP directory we can have an entry with RDN:
`uid=erikhje` and DN: `uid=erikhje,ou=iik,ou=ie,dc=ntnu,dc=no`). Maybe
NTNU’s LDAP has a structure like this:

``` 
  dc=no
   |
   +--dc=ntnu (these could be one entry dc=no,dc=ntnu)
       |
       +--ou=ie
           |
           +-ou=iik
              |
              +-uid=erikhje
              |     roomNumber=A117
              |     mobile=93034446
              |     .
              |     .
              +-uid=eigilo
              +-uid=erjonz
                .
                .
```

The Windows tools `Get-ADObject` and `dsquery` will only query LDAP on
port 389 while if we want to use NTNUs LDAP without authenticating
first, we have to use LDAP over SSL on port 636, so to demo access to
NTNU’s LDAP we use `ldapsearch` on Linux (from the package `ldap-utils`,
try this on `login.stud.ntnu.no` if you want to):

    ldapsearch -x -b "ou=people,dc=ntnu,dc=no" -h at.ntnu.no \
    "(&(mail=*frode*)(mobile=*95*))" sn givenName mobile
    ldapsearch -x -b "ou=people,dc=ntnu,dc=no" -h at.ntnu.no \
    "(mail=erik.hjelmas@ntnu.no)" sn givenName mobile
    echo SGplbG3DpXM= | base64 -d && echo

LDAP lookups can be used to [connect to NTNU LDAP from your email
client](https://innsida.ntnu.no/wiki/-/wiki/English/Configuring+LDAP) to
have automatic address lookups.

### Schema

The schema defines the structure of an LDAP directory:

  - Entries (objects) are instance of an objectClass

  - *ObjectClass* is the link to Schema which defines the object

  - From RFC4512 4. Directory Schema: The schema enables the Directory
    system to, for example:
    
      - prevent the creation of subordinate entries of the wrong
        object-class (e.g., a country as a subordinate of a person)
    
      - prevent the addition of attribute-types to an entry
        inappropriate to the object-class (e.g., a serial number to a
        person’s entry)
    
      - prevent the addition of an attribute value of a syntax not
        matching that defined for the attribute-type (e.g., a printable
        string to a bit string).

The Active Directory LDAP schema allows for the entries necessary for
Unix/Linux user accounts (POSIX attributes), in order for Active
Directory to be used in heterogenious environments (where users have a
mix of Windows, Mac and Linux).

### Search Syntax

Search Syntax in
figure [\[fig:adtech:aeb0c7baca8740bd8624cb745b99950a\]](#fig:adtech:aeb0c7baca8740bd8624cb745b99950a).

Boolean expressions (true/false) in *Prefix* notation (aka Polish
notation), not infix or postfix.

  - () group with parenthesis

  - & AND operator

  - OR operator

  - \! NOT operator

  - \* Wildcard

  - \=, \>=, \<= comparison operators

e.g. `(&(!Name=A*)(logonCount>=10))`

Prefix notation (aka Polish notation) means that the operator comes
first. In a mathematical expression this would mean we say `+ 2 2`
instead of `2 + 2`. To do LDAP queries in a Windows environment we need
the Active Directory tools installed. They are installed together with
Active Directory on DC1, but since we want to work on a Windows 10 (e.g.
the host MGR) we can install then with the following:

    Add-WindowsCapability -Online `
      -Name 'Rsat.ActiveDirectory.DS-LDS.Tools~~~~0.0.1.0'
    # test with e.g.
    Get-ADObject -LDAPFilter '(sAMAccountName=*)'

### Operations

LDAP Operations ("Commands") in
figure [\[fig:adtech:165b6e6736d84d0ea0f4623537553819\]](#fig:adtech:165b6e6736d84d0ea0f4623537553819).

  - StartTLS

  - Bind

  - Search

  - Compare

  - Add (*atomic*)

  - Delete (*atomic*)

  - Modify (*atomic*)

  - Modify DN (move entry) (*atomic*)

  - Abandon

  - Extended operation

  - Unbind

A connection to a LDAP server is initiated with StartTLS and Bind to
port 389 (TLS is Transport Layer Security, ensures encrypted
communication). A not-encrypted connection is initiated to port 389, but
then the StartTLS operation is given and from then on the connection is
encrypted. This is what happens in Active Directory LDAP-connections.
The alternative that some LDAP-servers use is to first establish an
encrypted connection with SSL, and then initiate the LDAP-dialogue, in
this case the port used is 636 and not 389. All write-operations to an
LDAP server is *atomic*, meaning they are either completed or not: they
cannot be interrupted or half-way completed under any circumstances
(think of atomic like an atom: something than cannot be split into
smaller pieces).

### Case: FEIDE

FEIDE (Felles Elektronisk IDEntitet) in
figure [5.9](#fig:adtech:944347ffbf6e480cb19bb0b1b20fd3b0).

![FEIDE (Felles Elektronisk
IDEntitet).<span label="fig:adtech:944347ffbf6e480cb19bb0b1b20fd3b0"></span>](/home/erikhje/office/kurs/secsrv/05-adtech/tex/../img/vertsfeide.png)  
<span>from [Feides arkitektur](https://www.feide.no/teknisk)</span>

This is the basic architecture of FEIDE, built around LDAP. In NTNU, the
box in the diagram "LDAP Brukerkatalog" is the server `at.ntnu.no` which
is not Active Directory, but OpenLDAP. As of Jan 2022 FEIDE is the
authentication backend for [1096
services](https://www.feide.no/tjenester-med-feide-innlogging), and you
probably authenticate with FEIDE every day without paying much attention
to it. The takeaway is that LDAP is widely used currently, has been
widely used for the last 20 years and will be for the next 20 years as
well. Learning LDAP is lasting knowledge. LDAP also plays a role in the
famous log4j vulnerability :

> An attacker who can control log messages or log message parameters can
> execute arbitrary code loaded from LDAP servers when message lookup
> substitution is enabled.

## Kerberos

In Greek mythology, Kerberos is a many headed dog, the guardian of the
entrance of Hades . Some key points about the Kerberos protocol:

  - Developed at MIT in the 80s

  - Provides a centralized authentication server to authenticate users
    to servers and servers to users

  - Relies on conventional encryption, making no use of public-key
    encryption

  - Provides Single-Sign On

  - Kerberized applications (apps need to support kerberos)

  - Two versions: version 4 and 5

  - Version 4 makes use of the old Data Encryptopn Standard which is
    considered obsolete

  - Used as the standard authentication and authorization mechanism in
    Active Directory

Kerberos in figure [5.10](#fig:adtech:046d3017bcfa430297d37a0e6848de66).

![Kerberos.<span label="fig:adtech:046d3017bcfa430297d37a0e6848de66"></span>](/home/erikhje/office/kurs/secsrv/05-adtech/tex/../img/kerberos.pdf)  
<span>Jeran Renz, [CC
BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0), via
Wikimedia Commons</span>

The following is a long quote from the Kerberos Wikipedia page . You do
not have to learn all these details by heart of course, but focus on
understanding how Kerberos is different from "User Client-based Login
without Kerberos":

(START OF WIKIPEDIA QUOTE) **User Client-based Login without Kerberos**

1.  A user enters a username and password on the client machine(s).
    Other credential mechanisms like pkinit (RFC 4556) allow for the use
    of public keys in place of a password. The client transforms the
    password into the key of a symmetric cipher. This either uses the
    built-in key scheduling, or a one-way hash, depending on the
    cipher-suite used.

2.  The server receives the username and symmetric cipher and compares
    it with the data from database. Login was a success if the cipher
    matches the cipher that is stored for the user.

**Client Authentication (now with Kerberos)**

1.  The client sends a cleartext message of the user ID to the AS
    (Authentication Server) requesting services on behalf of the user.
    (Note: Neither the secret key nor the password is sent to the AS.)

2.  The AS checks to see if the client is in its database. If it is, the
    AS generates the secret key by hashing the password of the user
    found at the database (e.g., Active Directory in Windows Server) and
    sends back the following two messages to the client:
    
      - Message A: Client/TGS Session Key encrypted using the secret key
        of the client/user.
    
      - Message B: Ticket-Granting-Ticket (TGT, which includes the
        client ID, client network address, ticket validity period, and
        the Client/TGS Session Key) encrypted using the secret key of
        the TGS.

3.  Once the client receives messages A and B, it attempts to decrypt
    message A with the secret key generated from the password entered by
    the user. If the user entered password does not match the password
    in the AS database, the client’s secret key will be different and
    thus unable to decrypt message A. With a valid password and secret
    key the client decrypts message A to obtain the Client/TGS Session
    Key. This session key is used for further communications with the
    TGS. (Note: The client cannot decrypt Message B, as it is encrypted
    using TGS’s secret key.) At this point, the client has enough
    information to authenticate itself to the TGS.

**Client Service Authorization**

1.  When requesting services, the client sends the following messages to
    the TGS:
    
      - Message C: Composed of the message B (the encrypted TGT using
        the TGS secret key) and the ID of the requested service.
    
      - Message D: Authenticator (which is composed of the client ID and
        the timestamp), encrypted using the Client/TGS Session Key.

2.  Upon receiving messages C and D, the TGS retrieves message B out of
    message C. It decrypts message B using the TGS secret key. This
    gives it the Client/TGS Session Key and the client ID (both are in
    the TGT). Using this Client/TGS Session Key, the TGS decrypts
    message D (Authenticator) and compares the client IDs from messages
    B and D; if they match, the server sends the following two messages
    to the client:
    
      - Message E: Client-to-server ticket (which includes the client
        ID, client network address, validity period, and Client/Server
        Session Key) encrypted using the service’s secret key.
    
      - Message F: Client/Server Session Key encrypted with the
        Client/TGS Session Key.

**Client Service Request**

1.  Upon receiving messages E and F from TGS, the client has enough
    information to authenticate itself to the Service Server (SS). The
    client connects to the SS and sends the following two messages:
    
      - Message E: From the previous step (the Client-to-server ticket,
        encrypted using service’s secret key).
    
      - Message G: A new Authenticator, which includes the client ID,
        timestamp and is encrypted using Client/Server Session Key.

2.  The SS decrypts the ticket (message E) using its own secret key to
    retrieve the Client/Server Session Key. Using the sessions key, SS
    decrypts the Authenticator and compares client ID from messages E
    and G, if they match server sends the following message to the
    client to confirm its true identity and willingness to serve the
    client:
    
      - Message H: The timestamp found in client’s Authenticator (plus 1
        in version 4, but not necessary in version 5\[9\]\[10\]),
        encrypted using the Client/Server Session Key.

3.  The client decrypts the confirmation (message H) using the
    Client/Server Session Key and checks whether the timestamp is
    correct. If so, then the client can trust the server and can start
    issuing service requests to the server.

4.  The server provides the requested services to the client. (END OF
    WIKIPEDIA QUOTE)

There are several attack techniques  against Kerberos which we will
study a bit in the last part of this course.

## Review questions and problems

1.  Use `Resolve-DnsName` to figure out who (which servers from which
    company) accepts email you send to the domain `vg.no`

2.  (**KEY PROBLEM**) Consider the following session in PowerShell which
    shows two identical command lines executed with just a few seconds
    in between them (`1.1.1.1` is Cloudflare’s public DNS server):
    
        PS> Resolve-DnsName -Server 1.1.1.1 -Type A facebook.com
        
        Name                   Type   TTL   Section    IPAddress
        ----                   ----   ---   -------    ---------
        facebook.com           A      244   Answer     31.13.72.36
          
        PS> Resolve-DnsName -Server 1.1.1.1 -Type A facebook.com
          
        Name                   Type   TTL   Section    IPAddress
        ----                   ----   ---   -------    ---------
        facebook.com           A      0     Answer     157.240.194.35
    
    Why have the values for the properties TTL and IPAddress changed?
    Explain and comment as detailed as you can.

3.  Log in to `login.stud.ntnu.no` with ssh from your laptop. In case
    you have not used ssh in a while, the syntax is:  
    `ssh YOUR_NTNU_USERNAME@login.stud.ntnu.no`  
    Use `ldapsearch` to find yourself (the query should list only you)
    with the following LDAP filters (hint: see examples in the chapter
    text with `frode` and `erik`):
    
    1.  provide your username (the attribute `uid`)
    
    2.  provide parts of your email address (the attribute `mail`)
    
    3.  provide parts of your first name (the attribute `givenName`) and
        parts of your mobile phone number (the attribute `mobile`)
    
    You should list the attributes `dn` (DistinguishedName), `givenName`
    (first name) and `sn` (last name). Remember that if an attribute
    contains norwegian characters (like my surname "Hjelmås"), you can
    decode the result you get from ldap with (example with encoded
    "Hjelmås"):  
    `echo SGplbG3DpXM= | base64 -d && echo`

## Lab tutorials

1.  Install the `sec.core` domain according to [PowerShell 2:
    Domain-joined hosts and
    Remoting](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#powershell-2-domain-joined-hosts-and-remoting),
    but add all hosts to the domain including `srv1`.

2.  After installing Active Directory and the Microsoft DNS server
    (which is installed as part of the process), list all *Resource
    Records (RRs)* in the DNS server (as Domain Administrator):  
    `Get-DnsServerResourceRecord -ZoneName sec.core`  
    Study carefully the SRV-records so you understand how DNS is used to
    locate services in a Windows domain (an Active Directory
    infrastructure).

3.  **DNS (Domain Name System)**.
    
    1.  Log in to CL1 and DC1. Which DNS server(s) do they use? Run this
        cmdlet on both hosts
        
            Get-DnsClientServerAddress
    
    2.  If you try to lookup the hostnames `srv1` and `cl1`, which
        domain suffixes are attempted added? Run this cmdlets on both
        hosts
        
            Get-DnsClient | Format-Table `
             -Property InterfaceAlias,InterfaceIndex,ConnectionSpecificSuffix
            Get-DnsClientGlobalSetting
            @('srv1','cl1') | Resolve-DnsName
    
    3.  What is in their local cache? Run these cmdlets on both hosts
        
            Get-DnsClientCache
            Clear-DnsClientCache
            Get-DnsClientCache
    
    4.  Do a DNS lookup to find the IP address of `rtfm.mit.edu`
        
            Resolve-DnsName rtfm.mit.edu
        
        The CNAME resource record represents an alias. It means it is a
        host name that maps to another host name. The A resource record
        is what we are really looking for. It contains the host name to
        IP address mapping.
    
    5.  Do a reverse DNS lookup to find the host name of ip address we
        found for `rtfm.mit.edu`
        
            $ip = (Resolve-DnsName rtfm.mit.edu).IP4Address
            $ip
            Resolve-DnsName $ip -Type PTR
        
        The PTR resource record contains the reverse mapping. Note that
        some hosts might have A records but not PTR records, meaning a
        host name to IP address lookup will work, but not the other way
        around.
    
    6.  Skip the internal DNS server and ask two other servers for the
        lookup instead: first one of NTNUs servers, then one of Google’s
        public servers.
        
            Resolve-DnsName -Server 129.241.0.200 rtfm.mit.edu
            Resolve-DnsName -Server 8.8.4.4 rtfm.mit.edu
        
        In the output from the Resolve-DnsName command shown below, make
        sure you understand the TTL (Time To Live) number. Run one of
        the commands above repeatedly and see how the number changes.
        
            Name             Type   TTL   Section    NameHost
            ----             ----   ---   -------    --------
            rtfm.mit.edu     CNAME  1799  Answer     xvm-75.mit.edu
    
    7.  Log in to login.stud.ntnu.no and observe the entire hierarchy of
        servers involved in a fresh DNS lookup (here you will also see
        other resource records: the NS (Name Server) record used to
        delegate responsibility for a domain to another DNS server, and
        the RRSIG which belongs to DNSSEC)
        
            dig @129.241.0.200 +trace rtfm.mit.edu
        
        Notice that some servers (root name servers) are responsible for
        the top level called just `.`, some servers are responsible for
        `edu.` and some servers are responsible for `mit.edu.` and one
        of them (the ones responsible for `mit.edu.`) will return the IP
        address of `rtfm.mit.edu`. *Notice that it is not very often
        that we ask the root name servers since there is caching used at
        all levels (temporary storage for a time indicated by the TTL
        (Time To Live) value).* Do you know what caching is? Ask teacher
        if unsure.

4.  **Testing our domain**.
    
    1.  Let’s see which lookups works. Do this on MGR.
        
            $hostnames = @(
              'dc1',
              'srv1',
              'cl1',
              'mgr'
            )
            $hostnames | Resolve-DnsName
            $hostnames | ForEach-Object {Resolve-DnsName "$_.sec.core"}
    
    2.  Which DNS server is authorative for the domain `sec.core`?
        
            $hostnames | 
              ForEach-Object {Resolve-DnsName "$_.sec.core" -Server dc1}
        
        Do you understand the output and have the answer? If not, ask
        teacher (or one of your fellow students) to explain.
    
    3.  Go back to CL1 and let’s see how a Windows Domain uses DNS for
        service discovery (SRV records in DNS)
        
            # Where is the "primary domain controller"?
            # (the one holding FSMO roles)
            Resolve-DNSName _ldap._tcp.pdc._msdcs.sec.core -Type SRV
            
            # Where are all the domain controllers?
            Resolve-DNSName _ldap._tcp.dc._msdcs.sec.core -Type SRV
            
            # Where is the global catalog?
            Resolve-DNSName _ldap._tcp.gc._msdcs.sec.core -Type SRV
            
            # where is the Kerberos server(s)?
            Resolve-DNSName _kerberos._tcp.dc._msdcs.sec.core -Type SRV

# Active Directory: Design and Implementation

In 2017, the company Maersk (shipping) was hit with a cyberattack which
caused them a financial loss of estmated close to NOK 2.500.000.000.
From Alsinawi :

> Fortunately, Maersk had one small bit of luck. An unplanned power
> outage kept a single server from getting infected, helping preserve a
> lone domain controller in an office in Ghana. When the office in Ghana
> didn’t have sufficient bandwidth to synch up the data center over the
> internet, a relay race was set in motion in which personnel from Ghana
> frantically met personnel from London in Nigeria\! A perfect Ethan
> Hunt mission.

## Windows Domain

We know about domains in the context of the domain name system (DNS). A
*Windows domain* is something else, it is a LDAP-based domain. It is
common to use the same name on a Windows domain as the corresponing DNS
domain. We are students and employees at NTNU where NTNU has authority
over the DNS domain `ntnu.no` and NTNU has the corresponding Windows
Domain `dc=ntnu,dc=no`. We establish a Windows domain by installing a
*domain controller (DC)* which is a Windows server with Active Directory
running as a service (it can also be Linux with Samba in some rare
cases). A company will always have more than one domain controller,
maybe tens or hundreds, and they all synchronize parts of (or all of)
their data periodically.

### Forest, Tree, Domain

Forest, Tree, Domain in
figure [6.1](#fig:adimpl:2c953049ebb94189b55a147e64a6468e).

![Forest, Tree,
Domain.<span label="fig:adimpl:2c953049ebb94189b55a147e64a6468e"></span>](/home/erikhje/office/kurs/secsrv/06-adimpl/tex/../img/forest.pdf)

A domain exists in a *forest*. In the simplest case (such as the one we
will use in this course) we have a single domain in a forest by itself.
In larger infrastructures we might have several domains where the
domains belong to different *trees* in a forest. E.g. when HiST, HiG and
HiALS where overtaken by NTNU in 2016, the IT infrastructures needed to
be merged. The author do not know the details of this process, but it
might be that for practical reason it would be useful to merge Høgskolen
i Sør-Trøndelag (HiST) and Høgskolen i Gjøvik (HiG) as domains *in the
same namespace (ntno.no) as NTNU*, and thereby making them into a domain
*tree*, while Høgskolen i Ålesund maybe had an infrastructure which was
more difficult to merge, and it would be better if they were merged as a
separate tree in the forest. What we need to know about the differences
between domains, trees and forests is the following:

  - Forest  
    is the *security boundary*. All domains in a forest share
    configation, schema, global catalog and trust each other. The
    *global catalog* holds a replica of every object in the forest, but
    with only a few selected attributes, this allows for fast search of
    all objects (e.g. users) in the entire forest, no matter which
    domain controller you contact.

  - Tree  
    is not a very important concept. It just means a collection of
    domains that shares the same namespace (have the same parent domain
    name).

  - Domain  
    is where all the objects (users, computers, groups, etc.) are
    stored. All domain controllers replicate their data to each other
    within the domain (all domain controllers have the same information
    within a domain). Note that there are a few attributes that are not
    replicated , e.g. a user’s `lastLogon`.

A company can even have multiple forests, and establish some kind of
relationship between them, but we will not study multiple forest setups.
We can mostly think of all domain controllers as "equals", but there are
some who have special roles when it comes to exact behavior during
replication of the most critical data. We will not study this here, but
if interested you can read about it in Microsoft’s documentation "Active
Directory FSMO roles in Windows" .

### Thinking Security

Perimeter Security vs Zero Trust in
figure [6.2](#fig:adimpl:4df03197e78745518eafbdb5d00da613).

![Perimeter Security vs Zero
Trust.<span label="fig:adimpl:4df03197e78745518eafbdb5d00da613"></span>](/home/erikhje/office/kurs/secsrv/06-adimpl/tex/../img/zero.pdf)

You just learned that the Active Directory forest is the security
boundary. This is true, but whenever we think of a security boundary we
should think that this is just a part of our security model. Protecting
the boundary (or perimeter) is something we should do, but with the
current and future threat landscape we should also think in terms of a
*Zero Trust* model which in principle means that you do not trust any
device inside our network either. Zero trust thinking can be best
achieved if we try mentally to remove the perimeter and have
perimeterless security . Setting up true zero trust is of course
impossible in practice, but zero trust is the mind set we should have
and try to achieve. A simple example is a user clicking on an email
attachment which initiates downloading and executing malware. An email
is allowed to pass the perimeter to the inside of our network, and with
thousands of users in our company someone’s computer will be infected by
malware. The question then is: how can malware propagate itself inside
our network? Zero trust is the generalization of that question: not
trusting any device inside our network by default (or actually removing
the concept of "our network" making our infrastructure perimeterless).

### Purpose

Why Active Directory in
figure [\[fig:adimpl:c7970c86e4394d90afb3d1c466020f8a\]](#fig:adimpl:c7970c86e4394d90afb3d1c466020f8a).

  - Identity and access management

  - Asset management

  - Policy-based configuration management

*Scales to very large organizations*

Active Directory can serve as one shared large database for a
multinational company with thousands of employees and hundreds of
locations spread around the world. It stores all IT resources as
identifiable objects (users, groups, computers, network devices,
printers, etc.) and provides access control and configuration management
for these objects.

## Implementing Active Directory

### A Case

Organizational Chart in
figure [6.3](#fig:adimpl:c96bb338af994a698458330a02cea50b).

![Organizational
Chart.<span label="fig:adimpl:c96bb338af994a698458330a02cea50b"></span>](/home/erikhje/office/kurs/secsrv/06-adimpl/tex/../img/organogram.pdf)

SEC.CORE is a security consultancy company offering security testing
services in terms of blue team and red team, and investigation services
in terms of digital forensics and incident response (DFIR). In addition,
there is a human resources (HR) department and the general
administration including the CEO and two system and network
administrators (the IT staff). SEC.CORE has all its general IT services
(email, www, accounting, etc.) in a public cloud, so in addition to the
domain controller DC1, there is only one additional server inside the
company called SRV1. They manage their IT resources through the
workstation MGR (which should be configured in the same way as a laptop
used by the administration), and CL1 is a laptop used by the
consultants.

### AD ObjectClasses

Important ObjectClasses in
figure [\[fig:adimpl:6596b0ea03244031a015deaf0d720969\]](#fig:adimpl:6596b0ea03244031a015deaf0d720969).

  - OU  
    organizational units

  - Container  
    like an OU, but cannot have Group Policy Objects

  - Group  
    have users, used for access control (a group have a SID, OUs don’t)

In addition, note the root directory server agent service entry (“root
of the directory information tree”) *RootDSE* which is not an
ObjectClass.

In the Active Directory *schema* all the ObjectClasses are described ,
and we create instances (objects) based on some of these definitions.
For grouping objects, we mostly create *organizational units (OUs)*. If
we join users and computers to a domain without specifying in which OU
they should be created, they end up in the default containers `users`
and `computers`, and we should move them into OUs as soon as we have
established an OU-infrastructure.

Default Setup of Users and OUs in
figure [6.4](#fig:adimpl:e289a2d0e3f24bf38ba5d2075a3e7c18).

![Default Setup of Users and
OUs.<span label="fig:adimpl:e289a2d0e3f24bf38ba5d2075a3e7c18"></span>](/home/erikhje/office/kurs/secsrv/06-adimpl/tex/../img/DefaultAD.png)

After installation of Active Directory the setup is by default:

  - Domain container  
    the root container to the hierarchy

  - Built-in container  
    the default service administrator group accounts

  - Users container  
    the default location for new user accounts and groups

  - Computers container  
    the default location for new computer accounts

  - Domain Controllers OU  
    the default location for the domain controllers computer accounts
    (computers that are domain controllers)

  - ForeignSecurityPrincipals  
    this is empty by default

  - Managed Service Accounts  
    this is empty by default

Default Setup of Users and OUs in
figure [6.5](#fig:adimpl:649025ee3b844375aab792478780bf42).

![Default Setup of Users and
OUs.<span label="fig:adimpl:649025ee3b844375aab792478780bf42"></span>](/home/erikhje/office/kurs/secsrv/06-adimpl/tex/../img/DefaultUsersAndOU.pdf)

When we install Active Directory on DC1, DC1 is registered in a computer
account in the only OU that exists by default: the Domain Controllers
OU. In the figure, we see that we have joined MGR to the domain, and it
ends up in the default container Computers (and should be moved from
there as we will see). We have five user accounts by default:

  - Administrator  
    This is the local Administrator account that has been turned into a
    Domain Administrator account.

  - Guest  
    This is the Guest account which is still (hopefully) disabled.

  - cloudbase-init  
    This is an account that exists in Windows cloud instances, it is
    used in the provisioning ("making ready for use") of Windows.

  - Admin  
    This is the default account created in Windows cloud instances
    (created by Cloudbase).

  - krbtgt  
    This acts as a service account for the Kerberos Key Distribution
    Center (KDC) service (it is created during the installation of
    Active Directory).

How does this relate to the local user accounts we learned about earlier
in the course? On the domain controller DC1, they are moved into Active
Directory when Active Directory is installed (each host that joins the
domain will still have local accounts). Before install of Active
Directory it looks like this:

    PS> Get-LocalUser | Select-Object -Property Name,PrincipalSource
    
    Name               PrincipalSource
    ----               ---------------
    Admin                        Local
    Administrator                Local
    cloudbase-init               Local
    DefaultAccount               Local
    Guest                        Local
    WDAGUtilityAccount           Local

After Active Directory has been installed it looks like this:

    PS> Get-LocalUser | Select-Object -Property Name,PrincipalSource
    
    Name           PrincipalSource
    ----           ---------------
    Administrator  ActiveDirectory
    Guest          ActiveDirectory
    krbtgt         ActiveDirectory
    cloudbase-init ActiveDirectory
    Admin          ActiveDirectory
    DC1$           ActiveDirectory

In practice this means that we can log into any domain-joined host using
an account that exists in Active Directory, but we can also log in on
the domain-joined hosts as local users, so in other words we have to
choose what kind of login we do when we log into a host. Maybe you have
experienced this: you have to choose which domain you log into (and the
local hostname can be one of those choices). All the system user and
group accounts (`Win32_SystemAccount`) also move to Active Directory.
The `$` in the account name `DC1$` means that it is a computer account
and not a user account. All computer accounts (aka machine accounts)
have names that end with a `$`.

### OU Design

OU Design in figure [6.6](#fig:adimpl:f42a4c8b399f4c61a43de274c20de4df).

![OU
Design.<span label="fig:adimpl:f42a4c8b399f4c61a43de274c20de4df"></span>](/home/erikhje/office/kurs/secsrv/06-adimpl/tex/../img/ou.pdf)

In the figure, SEC.CORE’s organizational chart has been converted into a
possible logical structure in Active Directory with organizational units
(OUs). This design follows the best practice  of not putting users and
computers in the same OUs and creating OUs for all objects to avoid them
staying in the default containers `Users` and `Computers`. Since
SEC.CORE is a small organization we base our design mostly on the
company’s organizational chart. We deviate on two grounds: we separate
IT since those user accounts will need a different set of permissions,
and in a small company HR does not have to be a separate OU, they can be
part of the general administration (note: this might be a bad decision
if the company grows and/or merges with other companies). The
alternative (which is often used for larger companies) is to base the
design on geographical location (in Active Directory this is called a
*site*) or a mix of geographical location and organizational structure.

The most important part about active directory is how to design the set
and structure of Organizational Units (OUs). The best practice for
designing OUs state that OUs should be structured primarely to
facilitate administrative delegation, secondary to facilite Group Policy
Objects. In practice this means that if you have multiple teams of
system administrators (a much larger organization than our SEC.CORE),
you should delegate responsibility to those teams by letting them be
administrators for a set of OUs. This is closely related to the
secondary purpose: facilitate GPOs. This means that you should group
users and computers in OUs based on who should have the same policies
applied to them. E.g. in a University you would have a natural
separation between students, faculty and administrative staff. And the
same for computers: students workstations should be in a separate OU
from faculty laptops.

Sites in active directory are meant to represent geographical locations
which how their own physical network. The concept of site is relevant
when your infrastructure consists of multiple physical sites where you
need to consider that bandwidth between sites might be an issue. If you
have sites that are far apart (e.g. a branch office in a different part
of the world) you will probably want users at those sites to have some
local services instead of communicating with the main physical site for
all computing services. AD sites are just for this purpose, so that you
e.g. can create a Read-Only Domain Controller (RODC) at a low-bandwidth
site to better serve your users at that site.

### Creating the OUs

Let’s create the OUs. If we don’t specify the `Path`, the OU will be
created on the top level `DC=sec,DC=core`.

``` 
# User OUs
New-ADOrganizationalUnit 'AllUsers' -Description 'Containing OUs and users'
New-ADOrganizationalUnit 'IT' -Description 'IT staff' `
  -Path 'OU=AllUsers,DC=sec,DC=core'
New-ADOrganizationalUnit 'Cons' -Description 'Consultants' `
  -Path 'OU=AllUsers,DC=sec,DC=core'
New-ADOrganizationalUnit 'Adm' -Description 'Administration' `
  -Path 'OU=AllUsers,DC=sec,DC=core'
New-ADOrganizationalUnit 'Blue' -Description 'Blue Team' `
  -Path 'OU=Cons,OU=AllUsers,DC=sec,DC=core'
New-ADOrganizationalUnit 'Red' -Description 'Red Team' `
  -Path 'OU=Cons,OU=AllUsers,DC=sec,DC=core'
New-ADOrganizationalUnit 'DFIR' -Description 'Dig For and Inc Resp' `
  -Path 'OU=Cons,OU=AllUsers,DC=sec,DC=core'
# Computer OUs
New-ADOrganizationalUnit 'Clients' -Description 'Containing OUs and users laptops'
New-ADOrganizationalUnit 'Servers' -Description 'Containing OUs and servers'
New-ADOrganizationalUnit 'Adm' -Description 'Adm laptops' `
  -Path 'OU=Clients,DC=sec,DC=core'
New-ADOrganizationalUnit 'Cons' -Description 'Consultants laptops' `
  -Path 'OU=Clients,DC=sec,DC=core'  
```

### Joining Computers

When joining computers to the domain we can either join without
specifying a path, and move the computer to the right OU from the
`Computers` container, or we can specify the path the computer should be
in Active Directory when we join.

    # In our case, all hosts were joined without specifying path 
    # so we need to move them:
    Get-ADComputer "MGR" | 
      Move-ADObject -TargetPath "OU=Adm,OU=Clients,DC=sec,DC=core"
    Get-ADComputer "CL1" | 
      Move-ADObject -TargetPath "OU=Cons,OU=Clients,DC=sec,DC=core"
    Get-ADComputer "SRV1" | 
      Move-ADObject -TargetPath "OU=Servers,DC=sec,DC=core"
    
    # If e.g. we had not joined cl1 yet, we could have joined it 
    # directly to its correct OU with
    # Add-Computer -Credential $cred -DomainName sec.core `
    #   -OUPath 'OU=Cons,OU=Clients,DC=sec,DC=core' -PassThru -Verbose
    #
    # Manual for Add-Computer states it is smart to use -PassThru:
    # "Returns an object representing the item with which you are working. 
    # By default, this cmdlet does not generate any output."
      
    # Let's check status, query via dedicated Cmdlet
    Get-ADComputer -Filter * |
      Select-Object -Property DNSHostName,DistinguishedName
    
    # Same query via generic LDAP Cmdlet
    Get-ADObject -LDAPfilter "(ObjectClass=Computer)" |
      Select-Object -Property DNSHostName,DistinguishedName
    # notice missing DNSHostName because cmdlets return different objects

### Adding Users

SEC.CORE with OUs and Users in
figure [6.7](#fig:adimpl:32e79ecea6744b1681add7900b2b37c3).

![SEC.CORE with OUs and
Users.<span label="fig:adimpl:32e79ecea6744b1681add7900b2b37c3"></span>](/home/erikhje/office/kurs/secsrv/06-adimpl/tex/../img/seccoreAD.png)

We want to create some random users without any default passwords, and
place them in the OUs. See more information in the start of the
`CreateUserCSV.ps1` file.

    # Lets create the users
    curl -O https://gitlab.com/erikhje/heat-mono/-/raw/master/scripts/CreateUserCSV.ps1
    .\CreateUserCSV.ps1
    $ADUsers = Import-Csv seccoreusers.csv -Delimiter ";"
    foreach ($User in $ADUsers) {
      New-ADUser `
        -SamAccountName        $User.Username `
        -UserPrincipalName     $User.UserPrincipalName `
        -Name                  $User.DisplayName `
        -GivenName             $User.GivenName `
        -Surname               $User.SurName `
        -Enabled               $True `
        -ChangePasswordAtLogon $False `
        -DisplayName           $user.Displayname `
        -Department            $user.Department `
        -Path                  $user.path `
        -AccountPassword (ConvertTo-SecureString $user.Password -AsPlainText -Force)
    }
    # How many users do we have now?
    (Get-ADUser -Filter * | Measure-Object).Count    
    # View all attributes of a user:
    Get-ADObject -Filter { SamAccountName -eq 'Jenny' } -Properties *

### Groups

The AGDLP Pattern in
figure [6.8](#fig:adimpl:88b2cce4bfcc4d3aa5963b39e749dae7).

![The AGDLP
Pattern.<span label="fig:adimpl:88b2cce4bfcc4d3aa5963b39e749dae7"></span>](/home/erikhje/office/kurs/secsrv/06-adimpl/tex/../img/agdlp.pdf)

Access control is hard. In the Unix/Linux world the standard model is
simple with owner, group and others, with read, write and execute
permissions. In the Windows world the set of possible permissions that
can be assigned is much bigger, and everything is not just a file like
in Unix/Linux. With more permissions and all kinds of objects, comes
more complexity and complexity is always our enemy. In Active Directory
it is considered bad practice to assign permissions directly to user
accounts, this can quickly get complicated, and it is hard to manage and
keep overview. To get access control right, we need to think in terms of
groups and roles. This gives us a more high level view of access control
that we are able to manage.

> *Make a note that this is the way we "always" solve complex problems
> in computer science, we manage complexity with hierarchy, layers and
> abstractions (aka "decompose a big complex problem into smaller
> simpler subproblems")*

Active Directory has the following types of groups :

  - Distribution groups  
    only used for mailing lists (not directly relevant for us).

  - Security groups  
    used for assigning permissions and roles ("grouped permissions")
    with different *scope*:
    
      - Universal  
        Forest scope.
    
      - Global  
        Domain scope.
    
      - Domain local  
        Domain scope, but cannot be a member of "built-in groups that
        have well-known SIDs" .

The standard pattern for setting up access control in Actice Directory
is called AGDLP which is an acronym for Account, *Global group*, *Domain
Local group* and Permissions. We typically make global groups
representing each OU we have with user accounts (the user accounts from
the OU are added to the global group). We cannot do access control based
on OUs directly because OUs do not have SIDs (Security IDentifiers),
they are not *security principals*. Computer accounts, user accounts and
groups are security principals (they have SIDs) and can be used for
access control. As mentioned we make groups based on each OU, typically
inside each OU, and assign the users in the OU to this group. For each
resource we want to control access to, we make domain local groups which
are assigned permissions to those resources. In this setup we can then
assign the global groups to the domain local groups. This makes a
managable setup of roles and permissions, but it is not dynamic: if we
add another user account to the OU it is not automatically added to the
global group. Whenever we add ned users to the domain, we have to have
routines for updating the global groups as well.

Default Domain Local Groups in Builtin in
figure [6.9](#fig:adimpl:3ae78cd52b6b4bcc8212248ce2fce3a7).

![Default Domain Local Groups in Builtin
.<span label="fig:adimpl:3ae78cd52b6b4bcc8212248ce2fce3a7"></span>](/home/erikhje/office/kurs/secsrv/06-adimpl/tex/../img/DefaultBuiltin.png)

Active Directory comes with a set of predefined domain local groups in
the container Builtin. These groups represent common administrative
roles in a domain. We can add users to these groups to assign the user
the role the group represents (e.g. IT staff working with
troubleshooting or incident management should be added to "Event Log
Readers").

Default Groups and Users in Users in
figure [6.10](#fig:adimpl:43635766b42944fab6e59648463270fd).

![Default Groups and Users in
Users.<span label="fig:adimpl:43635766b42944fab6e59648463270fd"></span>](/home/erikhje/office/kurs/secsrv/06-adimpl/tex/../img/DefaultUsers.png)

Active Directory comes with a set of predefined universal, global,
domain local groups and users in the container Users. These groups
represent common roles in a domain just like the groups in Builtin, but
there are universal groups, global groups and users in addition to
domain local groups. For the groups and users in Builtin and Users,
Microsoft states :

> You can move groups that are located in these containers to other
> groups or organizational units (OU) within the domain, but you cannot
> move them to other domains.

RDP group in
figure [6.11](#fig:adimpl:edfe63a68ccc482abfe891a5a8057e48).

![RDP
group.<span label="fig:adimpl:edfe63a68ccc482abfe891a5a8057e48"></span>](/home/erikhje/office/kurs/secsrv/06-adimpl/tex/../img/RDPgroup.png)

By default only domain administrators are allowed to RDP (connect to a
host with remote desktop) to hosts in a domain. If other users should be
allowed this they have to be added to the local group "Remote Desktop
Users" on the host they want to RDP to. In our case we want to allow a
specific user, e.g. `julie`, to be allowed to RDP to the host CL1. Since
the local group (not a "domain local group", it is a "local group" that
exist only on the host CL1) "Remote Desktop Users" on CL1 is one of the
"built-in groups that have well-known SIDs" , we cannot simply add
`julie` to the builtin "Remote Desktop Users" in Active Directory
because that group is a domain local group. To follow the AGDLP pattern
(actually this will be a slightly modified "AGLP" pattern) we need to
create a new global group, add `julie` to that group, and then add that
group to the local "Remote Desktop Users" on CL1:

    # on DC1
    $GROUP = @{
      Name          = "g_My RDP Users"
      GroupCategory = "Security"
      GroupScope    = "Global"
      DisplayName   = "Local RDP users"
      Path          = "OU=AllUsers,DC=sec,DC=core"
      Description   = "Users/Groups that will be allowed to RDP to CL1"
    }
    New-ADGroup @GROUP
    Add-ADGroupMember -Identity 'g_My RDP Users' -Members 'Julie'
    Get-ADGroupMember -Identity 'g_My RDP Users'
      
    # on CL1
    Add-LocalGroupMember -Group "Remote Desktop Users" -Member 'SEC.CORE\g_My RDP Users'
    Get-LocalGroupMember -Group "Remote Desktop Users"

We prefix all global groups with `g_` to make it easy to identify the
global groups just by their name.

## Review questions and problems

1.  Do the lab tutorial before you try to solve this problem.
    
    1.  Use `Get-ADUser -Filter` to find all user accounts that contain
        the letter ’M’ in the `SamAccountName`. Pipe to `Format-Table`
        and print only `SamAccountName` and `Name`.
    
    2.  Use `Get-ADUser -LDAPFilter` to find all user accounts that
        contain the letter ’M’ in the `SamAccountName`. Pipe to
        `Format-Table` and print only `SamAccountName` and `Name`.
    
    3.  Without piping to `Format-Table`, measure how much time each of
        the two different commands use with `Measure-Command`. Is one of
        them faster than the other?

2.  (**KEY PROBLEM**) Do the lab tutorial before you try to solve this
    problem.
    
    1.  Delete all the users in the OU `DFIR`.
    
    2.  Remove the OU `DFIR` with
        
            Remove-ADOrganizationalUnit -Identity `
              'OU=DFIR,OU=Cons,OU=AllUsers,DC=sec,DC=core'
        
        (hint: you will not be allowed, search the internet to find our
        how what you need to do to remove an OU)
    
    3.  Create the OU DFIR again.
    
    4.  Create the users again by modifying the `foreach` loop you used
        in the lab tutorial to create them.

## Lab tutorials

1.  Add OUs, Users, Groups to your sec.core domain (assuming you
    installed the domain in last weeks exercise) and use the AGDLP
    pattern to allow selected users to RDP to CL1 (like `Julie`), all
    according to the description in the chapter text. In other words,
    implement what you have read in this chapter.

2.  Mount Active Directory as a PSDrive and navigate around in it:
    
        Import-Module ActiveDirectory
        Get-PSDrive
        cd AD:\
        cd 'DC=sec,DC=core'
        cd OU=AllUsers
        Get-ChildItem
        cd OU=Adm
        Get-ChildItem
        cd ../..
        Get-ChildItem

# Remoting, Config Management and Group Policy

Configuration Management is about *trying to control the configuration
("settings") on many hosts from a central location*. There are many
approaches and technologies that can be used for this, e.g. Cfengine,
Puppet and Ansible. In our context we focus on the most widely used and
long-lasting technology for Windows infrastructures, namely Group
Policy. We will also cover briefly some alternative approaches and
complementary technologies (specifically PowerShell remoting).

## Push vs Pull

Push vs Pull in
figure [7.1](#fig:policy:945a2e076de441ff91cfbaaa31eb2956).

![Push vs
Pull.<span label="fig:policy:945a2e076de441ff91cfbaaa31eb2956"></span>](/home/erikhje/office/kurs/secsrv/07-policy/tex/../img/pushpull.pdf)

There are two major approaches to controlling the configuration on
numerous hosts from a central server :

  - Push  
    is when the central server initiates the connection to the hosts
    that are under its "control". The central server "pushes" the
    configuration on the hosts. Examples of push are PowerShell remoting
    and SSH.

  - Pull  
    is when the hosts themselves at "their own will" contact the server
    and download the configuration and applies it at their own time.
    Examples of pull are Group Policy and PowerShell Desired State
    Configuration (DSC).

Different technologies use different ports for communication. Sometimes
the configuration management systems communicate over separate "trusted
management LANs", but if we follow a strict Zero Trust model (as
mentioned in previous chapters) we still should not trust each host
unless they mutually authenticate.

## PowerShell Remoting

In PowerShell, we can connect to other hosts using either WSMan or SSH
as the underlaying protocol (some Cmdlets also can use DCOM/WMI/RPC
(port 135) for remoting, but we will not cover that). SSH has to be used
if we want to do PowerShell remoting to non-Windows hosts. In our
environment we will use PowerShell remoting over the WSMan protocol (aka
Windows Remote Management, WinRM).

Using PowerShell remoting is simple as long as all hosts are in the same
domain. Connecting to a host outside the domain, or between two hosts
not in a domain, is also possible but some intitial configuration is
required. See [the three scenarios in the
PowerShell-document](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#scenario-1-both-hosts-in-domain-logged-in-as-domain-administrator).

There are three standard ways of using PowerShell remoting (the examples
below are from MGR connecting to DC1 where both are in the same domain):

1.  An interactive session:
    
        Enter-PSSession DC1

2.  Executing a set of commands on a set of remote host:
    
        Invoke-Command -ComputerName DC1,SRV1 `
          {Get-LocalUser -Name Guest | Select-Object -Property Enabled}

3.  Establish a persistant session:
    
        $s = New-PSSession -ComputerName DC1,SRV1
        Invoke-Command -Session $s {Get-HotFix}
        Get-PSSession
        Remove-PSSession $s

Establishing a persistant session can sometimes be faster than using
`Invoke-Command`.

## SMB and File Shares

Windows can share parts of its file system using the Server Message
Block (SMB) protocol (aka Common Internet File System (CIFS)). Normally
the SMB service listens on port 445. SMB can also be used for other
purposes, including Remote Command Execution (RCE), when used in
combination with Remote Procedure Calls (RPC) on port 135 (and possible
137 and 139). For now, we will only focus on the file share
functionality.

Managing file share in PowerShell can be done with cmdlets from the
`SmbShare` module:  
`Get-Command -Module SmbShare`

SMB File Shares in
figure [7.3](#fig:policy:e66693c29fb4483c909d94b8568881f8).

Normal Windows host:

![SMB File
Shares.<span label="fig:policy:e66693c29fb4483c909d94b8568881f8"></span>](/home/erikhje/office/kurs/secsrv/07-policy/tex/../img/smbclient.png)

Domain Controller:

![SMB File
Shares.<span label="fig:policy:e66693c29fb4483c909d94b8568881f8"></span>](/home/erikhje/office/kurs/secsrv/07-policy/tex/../img/smb.png)

File shares can be *hidden* or visible. Hidden means that the shares are
not visible in Windows Explorer or My Computer, we can still easily
"see" them with other tools. The hidden file share have a `'$'` at the
end of the share name. The Admin, C and IPC shares are administrative
shares that exists on all Windows hosts. NETLOGON and SYSVOL exists on
domain controller. To access a file share we do not use an ordinary file
path, but we use a *Universal Naming Convention (UNC)* path . A UNC-path
looks like this:

    \\server\share\filepath
    e.g.
    Get-ChildItem '\\DC1\ADMIN$\System32\calc.exe'

Local file shares can be listed with the cmdlet `Get-SmbShare`. To view
shares on a remote computer from the command line we can use the old
cmd-program `net view`, e.g.  
`net view \\dc1`

SMB File Share Access in
figure [7.4](#fig:policy:76f182e14b5a4145972baccce59a8111).

![SMB File Share
Access.<span label="fig:policy:76f182e14b5a4145972baccce59a8111"></span>](/home/erikhje/office/kurs/secsrv/07-policy/tex/../img/smbshareaccess.png)

The default shares Admin, C and IPC are only accessible by local users
with administrator privileges by default. In a domain this means that
administrator accounts in the domain also have access. The NETLOGON and
SYSVOL shares are the most interesting ones for us:

  - SYSVOL  
    This is where the group policy objects are.

  - NETLOGON  
    This is where user logon scripts are (but the share is really just a
    link to a subfolder of SYSVOL).
    
        PS> (Get-SmbShare -Name NETLOGON).Path
        C:\windows\SYSVOL\sysvol\sec.core\SCRIPTS
        PS> (Get-SmbShare -Name SYSVOL).Path
        C:\windows\SYSVOL\sysvol

## Group Policy

### Architecture

Group Policy Architecture in
figure [7.5](#fig:policy:c82c24fc12af46a888279bddc47fb7b3).

![Group Policy
Architecture.<span label="fig:policy:c82c24fc12af46a888279bddc47fb7b3"></span>](/home/erikhje/office/kurs/secsrv/07-policy/tex/../img/gparch.pdf)

The group policy client is the `gpsvc`-service running inside a
service-hosting process (`svchost`). Its configuration is stored in the
registry location  
`HKLM:\\SYSTEM\CurrentControlSet\services\gpsvc`  
The group policy client contacts Active Directory to get information
about where the host is registered in LDAP (in which OU). Based on this
information it can procede to download the correct Group Policy Objects
(GPOs) from the SYSVOL file share, and then apply the policies and
preferences in those GPOs.

On domain controllers, the group policy client runs by default every
five minutes, while on all other hosts it run every 90 minutes (with a
30 minutes random offset to ensure load distribution). These values can
be changed in registry. A Group Policy update can also be forced by
running `Invoke-GPUpdate` (if you want it to happen immediately you do
`Invoke-GPUpdate -RandomDelayInMinutes 0`). If `Invoke-GPUpdate` is not
available as a command we can use the old cmd-command `gpupdate`.
Remember that we can find all processes and services directly involved
in network communication with (from chp one):

    Get-NetTCPConnection |
    Select-Object -Property LocalAddress,LocalPort,State,OwningProcess,`
    @{Name='ProcessName';Expression={(Get-Process `
      -Id $_.OwningProcess).ProcessName}},`
    @{Name='ServiceName';Expression={(Get-CimInstance -ClassName `
      Win32_Service -Filter "ProcessID=$($_.OwningProcess)").Name}} | 
      Format-Table -AutoSize

### Processing Order

GPO Processing Order in
figure [\[fig:policy:7118aa113eca4c15baf670bf3f8f0cf6\]](#fig:policy:7118aa113eca4c15baf670bf3f8f0cf6).

When a host is joined to a domain:

1.  Local GPO

2.  GPO linked to site (do not do this)

3.  GPO linked to domain

4.  GPO linked to OU (do this)

GPO’s are created, tested, then linked to site, domain or OU.

*Last writer wins\! In other words, OU-GPOs overwrites conflicting GPOs
from previous steps (unless `Enforced` is set).*

A site is a defined IP subnet, typically a geographical location.
Sometimes managing hosts based on their physical location is relevant,
but mostly we do not apply group policy based on geographical location,
so we will not address this here.

### Policy Settings

Group Policy in
figure [\[fig:policy:bad0d25d66114eeeb0af1ea7a34db308\]](#fig:policy:bad0d25d66114eeeb0af1ea7a34db308).

  - Policy settings are grouped into GPOs
    
      - Computer  
        applies to all users
    
      - Users  
        only for specific users/user groups

  - Subcategories are
    
      - Software settings (installations)
    
      - Windows settings (login scripts, folder redirection, printers,
        ...)
    
      - Administrative templates

  - Options for each setting
    
      - Not configured
    
      - Enabled
    
      - Disabled, can mean
        
          - Reverse the setting from a previous level
        
          - Force disabling of an OS default

*Almost everything is changes to the Windows Registry (HKLM and HKCU)*

Settings are grouped into Software settings, Windows settings and
Administrative templates for Computer and for Users. Some settings do
not make sense to set for specific users (or user groups) so they are
not present in the Users category, only in the Computer category,
likewise the other way around. When you set something in the Computer
category it will apply to all users.

Administrative templates for Computer are changes in the HKLM part of
the registry, while Administrative templates for User are changes in
HKCU.

If a computer is not joined to a domain, you can use *Local Group
Policy* on that standalone computer if you want to manage it with the
mechanisms of group policy.

DEMO LOCAL GROUP POLICY: disable task manager after CTRL-ALT-DEL:

    User configuration
     -> Administrative templates
     -> System
     -> CTRL-ALT-DEL options

(Note how this is only available to Users, and not under Computer
configuration.)

DEMO LOCAL GROUP POLICY: allow bginfo.exe from sysinternals:

    User configuration
     -> Administrative templates
     -> Desktop
     -> Desktop
     -> Desktop wallpaper

If we were to make this change manually by changing the registry using
PowerShell, we would do something like this:

``` 
  Set-ItemProperty -Path 'HKCU:\Control Panel\Desktop\' `
    -Name WallPaper `
    -Value 'C:\Users\ADMINI~1\AppData\Local\Temp\BGInfo.bmp'
```

Inheritance in
figure [7.6](#fig:policy:804cc564d8e6436595ccf998301e7752).

<http://technet.microsoft.com/en-us/library/cc754948%28v=ws.10%29.aspx>:

![Inheritance.<span label="fig:policy:804cc564d8e6436595ccf998301e7752"></span>](/home/erikhje/office/kurs/secsrv/07-policy/tex/../img/inheritance.png)

Since GPOs are applied in a specific order, we can apply general
policies early in the processing (e.g. at domain level, or a
higher-level OU), and then override the same settings for specific
groups of users in a lower level OU.

DEMO GROUP POLICY IN A DOMAIN: “Force disabling of an OS default”
(windows has a firewall on by default, this is not something turned on
by Group Policy, but we are going to use Group Policy to turn it off):

1.  Creating a Group Policy Object is probably best to do with a GUI. We
    can do it with PowerShell if we know the exact registry settings we
    want to apply, but probably we should take advantage of a GUI in the
    design stage of a GPO:
    
        # On DC1
        gpme.msc
        
        # Create a new GPO called 'MyFWSettings'
        Computer configuration
         -> Administrative templates
         -> Network
         -> Network connections
         -> Windows Defender firewall
         -> Domain profile
         -> Protect all network connections (disable)
        # exit gpme

2.  A good strategy would be to have a repository of all GPOs and
    separate those from the ones we actually apply, just import them
    when needed. In other words, create a GPOs, label them starting with
    ’My’ and let those be our "code" which we could put in a git-repo,
    or at least have version controlled and backed-up somewhere.
    
        # On DC1
        
        # copy the one we created to one that we are going to use
        Copy-GPO -SourceName "MyFWSettings" -TargetName "FWSettings"
        
        # link it to the Clients OU so it will be applied to CL1
        Get-GPO -Name "FWSettings" | 
         New-GPLink -Target "OU=Clients,DC=sec,DC=core"
        
        # btw we can view all GPOs with
        Get-GPO -All -Domain $env:USERDNSDOMAIN
        # or
        Get-GPO -All | Format-Table -Property displayname
        
        # Remember also that GPOs are just objects in an AD LDAP tree:
        Get-ADObject -LDAPFilter "(ObjectClass=groupPolicyContainer)" | 
         ForEach-Object {Get-GPO -Id $_.Name}
        
        # And they are just a file structure made available
        # to hosts through a share
        Get-ChildItem C:\Windows\SYSVOL\domain\Policies
        Get-SmbShare

3.  Let’s see how this GPO affects CL1. Keep the Firewall control
    panel  
    (`firewall.cpl`) visible alongside PowerShell.
    
        # On CL1
        
        # Install if not present:
        Get-WindowsCapability -Online `
         -Name Rsat.GroupPolicy.Management.Tools~~~~0.0.1.0 |
         Add-WindowsCapability -Online
        
        Invoke-GPUpdate -RandomDelayInMinutes 0
        
        # or just
        gpupdate /force
        
        # We can view report with
        Get-GPOReport -All -Domain $env:USERDNSDOMAIN -ReportType HTML `
         -Path ".\GPOReport1.html"
        .\GPOReport1.html
        
        # or
        gpresult.exe /h GPOReport.html

A Group Policy Object (GPO) in a domain consists of two parts: 1. The
*container* which is stored in Active Directory and contains metadata
(version information, etc.) and 2. The *template* which is stored in the
file system, in the SYSVOL folder, and contains the actual data (the
policy settings).

> *Notice that we use a GUI to create GPOs. We can use PowerShell as
> well, but creating GPOs is mostly a one time job and best done with a
> GUI. Instead of trying to create GPOs with PowerShell, we use the GUI,
> and then we link, backup, apply, monitor, etc. with command line
> PowerShell afterwards. Use the right tool for the right job\!*

### Settings vs Preferences

GP Settings vs GP Preferences in
figure [\[fig:policy:72a17fa3bf6f406e84ac167800305f0f\]](#fig:policy:72a17fa3bf6f406e84ac167800305f0f).

  - Settings are enforced

  - Preferences can be changed by the user after they have been applied

In a domain policy (meaning: not in Local Group Policy), we have both
settings and *preferences* . Preferences look similar to settings, but
the difference is that preferences can be changed by the user.
Preferences extend the functionality of group policy, they offer
additional "settings" to group policy settings, but we cannot
control/enforce these "settings" in the same way as standard group
policy settings. Preferences are used for less important configurations
(compared to settings) like creating a shortcut on a users desktop.

### Professional Practice

Are we all doing the same? in
figure [\[fig:policy:e62f643937a2441182e638e18d9e5f5d\]](#fig:policy:e62f643937a2441182e638e18d9e5f5d).

  - Everyone is managing Windows servers and laptops, isn’t there some
    common best practice we can all adopt and adapt?

  - [Microsoft Security
    Baselines](https://techcommunity.microsoft.com/t5/microsoft-security-baselines/bg-p/Microsoft-Security-Baselines)

Microsoft Security Baseline is a collection of GPOs you can download and
import from Microsoft and use as a starting point for a good baseline
security setup for your hosts. There are separate GPOs for each Windows
edition and also sometimes for specific functionality or application
(especially the browser Microsoft Edge, since the browser really is a
critical component of all client hosts). When you apply the security
baseline(s) to a Windows host the most immediate effect you will
probably notice is that the screen will be locked if you are away from
the keyboard for a while.

    # We have to use 7zip instead of Expand-Archive because 7zip
    # handles wierd long file and directory names better
    # (trust me :)
    choco install -y 7zip
    # the following should be on one line:
    curl -O https://download.microsoft.com/download/8/5/C/
      85C25433-A1B0-4FFA-9429-7E023E7DA8D8/
      Windows%2010%20version%2021H2%20Security%20Baseline.zip
    7z x .\Windows%2010%20version%2021H2%20Security%20Baseline.zip
    cd .\Windows-10-v21H2-Security-Baseline\Scripts
    Get-GPO -All | Format-Table -Property displayname
    .\Baseline-ADImport.ps1
    Get-GPO -All | Format-Table -Property displayname
    
    # We need the OU distinguished name several times
    $OU = "OU=Clients,DC=sec,DC=core"
    
    # Get all currently linked to OU Clients
    # (Repeat this after you have linked to GPOs below)
    Get-ADOrganizationalUnit $OU | 
     Select-Object -ExpandProperty LinkedGroupPolicyObjects
    # if you want to see the names of the GPOs
    # from https://community.spiceworks.com/topic/
           2197327-powershell-script-to-get-gpo-linked-
           to-ou-and-its-child-ou
    $LinkedGPOs = Get-ADOrganizationalUnit $OU | 
     Select-object -ExpandProperty LinkedGroupPolicyObjects
    $LinkedGPOGUIDs = $LinkedGPOs | ForEach-object{$_.Substring(4,36)}
    $LinkedGPOGUIDs | 
     ForEach-object {Get-GPO -Guid $_ | Select-object Displayname }
    
    # link two new ones to OU Clients
    Get-GPO -Name "MSFT Windows 10 21H2 - Computer" | 
     New-GPLink -Target $OU
    Get-GPO -Name "MSFT Windows 10 21H2 - User" | 
     New-GPLink -Target $OU
    
    # On CL1 and/or MGR apply
    Invoke-GPUpdate -RandomDelayInMinutes 0
    # or 
    gpupdate /force
    
    # See changes in report, Policies, Settings, Windows Settings,
    # Security Settings, Who is the "Winning GPO"?
    gpresult.exe /h GPOReport.html
    ./GPOReport.html

See also examples of how you can import Group Policy templates for other
applications at e.g. [Group Policy Administrative Templates
Catalog](https://admx.help/) (but please inspect what you are
downloading and applying on your domain controllers. Always look for
direct sources and consider if the code is trustworthy).

### Tools

Group Policy Tools Overview in
figure [\[fig:policy:da95792c87994f83b90acd8f22a58618\]](#fig:policy:da95792c87994f83b90acd8f22a58618).

  - Local GPO: `gpedit.msc`

  - AD: `gpmc.msc`, `gpme.msc`  

  - PowerShell:  
    `Import-Module GroupPolicy`  
    `Get-Command -Module GroupPolicy`

  - Group Policy Results  
    `Get-GPResultantSetOfPolicy`  
    *Resultant Set of Policy - RSoP*

Remember manual client pull with `Invoke-GPUpdate`

In a domain, `gpme.msc` is our most important GUI tool. It is where we
design and configure our GPOs, and then we should use PowerShell for the
rest of the work involved.

Some Important Notes in
figure [\[fig:policy:22a459d7669149648274b5c32269583f\]](#fig:policy:22a459d7669149648274b5c32269583f).

  - Settings for a set of users or computers, managed by the same
    administrators: *should be in same GPO*

  - Filtering by WMI can be slow

  - Enforce settings with a new GPO at the domain level to avoid changes
    by OU-administrators

  - Link order can be manipulated

  - *Max 999 GPOs*

These are some useful notes collected from the excellent book by Jeremy
Moskowitz :

  - GPOs cannot be applied to the default categories Users and Computers
    since these are containers not OUs.

  - Higher level policies (domain) can be set to "enforced", meaning
    they can not be overridden by lower level (OU) (Enforced takes
    precedence over Block policy inheritance).

  - Settings that apply to the same set of users or computers and are
    managed by the same administrators, should be in the same GPO.

  - A GPO (not individual policy settings) can be filtered by security
    (e.g. instead of all authenticated users, choose a specific user
    group) or WMI to only apply to a subset of users or computers.

  - Use WMI filters only when necessary (exceptions), can be
    time-consuming or do not time out at all (long logon times...) (WMI
    filters can be though of as conditional statements).

  - Max 999 GPOs can be applied, if you have more, NONE WILL BE
    APPLIED\!

  - Do not change default domain or domain controller policy, instead
    create a new GPO and set it to Enforce.

  - You create Enforce settings at the Domain level to avoid changes by
    OU-admini-strators.

  - You can manipulate the link order of GPOs (e.g. the GPOs linked to
    an OU).

Great Book\! in
figure [7.7](#fig:policy:8a9ba90bf2994666a6d8bd1e610e7db7).

![Great
Book\!.<span label="fig:policy:8a9ba90bf2994666a6d8bd1e610e7db7"></span>](/home/erikhje/office/kurs/secsrv/07-policy/tex/../img/GPbook.png)

## Other Techniques

### PsExec

PsExec in figure [7.8](#fig:policy:4dfebddd66df4fb2a9ffb4fe8b0e0798).

From [PSExec
Demystified](https://www.rapid7.com/blog/post/2013/03/09/psexec-demystified/):

![PsExec.<span label="fig:policy:4dfebddd66df4fb2a9ffb4fe8b0e0798"></span>](/home/erikhje/office/kurs/secsrv/07-policy/tex/../img/psexec.png)

`PsExec` from Sysinternals is one tool that can be used to run remote
commands:

    # On MGR, as Domain Administrator
    #
    PS C:\Users\Administrator> PsExec.exe \\dc1 ipconfig
    
    PsExec v2.34 - Execute processes remotely
    Copyright (C) 2001-2021 Mark Russinovich
    Sysinternals - www.sysinternals.com
    
    Windows IP Configuration
    
    Ethernet adapter tapd1f5f055-ad:
    
       Connection-specific DNS Suffix  . : openstacklocal
       Link-local IPv6 Address . . . . . : fe80::f816:3eff:fe3a:fbc1%15
       IPv4 Address. . . . . . . . . . . : 192.168.111.134
       Subnet Mask . . . . . . . . . . . : 255.255.255.0
       Default Gateway . . . . . . . . . : 192.168.111.1
    ipconfig exited on dc1 with error code 0.

### PowerShell DSC

Desired State Configuration in
figure [\[fig:policy:11eb91af1e554ceba325a8132f5f2d8e\]](#fig:policy:11eb91af1e554ceba325a8132f5f2d8e).

Example of PowerShell DSC code:

    Node SRV1
    {
      User MyUsers
      {
          Ensure   = "Absent"
          UserName = "Mysil"
      }
    }

The code makes sure that the user `Mysil` does not exist on host SRV1.
PowerShell Desired State Configuration (DSC) is a way of writing
*declarative* code in PowerShell, and it has its own framework for
applying this code. Declarative code means that you just state the end
state that you want to achieve, and then the framework will make it
happen. This is different from the kind of programming you are used to,
which is called *imperative* code because it is focused on how to reach
the end state instead of just describing it.

### SSH

SSH you know from earlier courses. We mention it here since it can also
be used between Windows hosts, and PowerShell can use it as its
underlying protocol when doing PowerShell remoting (which means we then
can use PowerShell remoting with Linux and Mac hosts as well).

### Intune and Endpoint Configuration Manager

Microsoft has other tools for configuration management that we will not
cover in this course, but they are worth being aware of. Microsoft
Intune is a cloud-based service for managing mobile devices. Microsoft
Endpoint Configuration Manager (previously known as System Center
Configuration Manager (SCCM)) is a GUI-based tool for configuration
management that mostly used HTTPS for communication between nodes .

## Review questions and problems

1.  How is Local Group Policy different from Group Policy in a domain?

2.  What is the difference between choosing "Not configured" and
    "Disabled" for a policy setting?

3.  Why will the following command fail?
    
        Get-GPO -Name "MSFT Windows 10 21H2 - User" |
          New-GPLink -Target "CN=Users,DC=sec,DC=core"

4.  (**KEY PROBLEM**) In the chapter text, you can read how to apply the
    security baseline to Windows 10 hosts. Do the same with the Server
    2022 Baseline for the hosts DC1 and SRV1:
    
      - Link the GPO "MSFT Windows Server 2022 - Member Server" to
        SRV1’s OU
    
      - Link the GPO "MSFT Windows Server 2022 - Domain Controller" to
        DC1’s OU
    
    You can download the security baseline from [Windows Server 2022
    Security
    Baseline](https://techcommunity.microsoft.com/t5/microsoft-security-baselines/windows-server-2022-security-baseline/ba-p/2724685).
    Remember: after you have downloaded something with your browser you
    can probably find the URL of what you downloaded in "Downloads"
    (somewhere in a menu), use this to then download on DC1 with `curl
    -O` like in the chapter text. You should not use a web browser
    directly from a critical server. Web browsers are security
    nightmares and should only be used from client hosts, not servers.

5.  On MGR, write a PowerShell command line that uses `Invoke-Command`
    to execute `Get-HotFix` on the hosts DC1 and SRV1.

## Lab tutorials

1.  No lab tutorials this week.

# Software Package Management

## What is Software?

What Is a Software Application? in
figure [\[fig:software:254718d40b314909a39090681cbb1087\]](#fig:software:254718d40b314909a39090681cbb1087).

  - *Executables*

  - Shared and non-shared libraries, modules

  - Images (icons), sound, help files, directories

  - Config files and registry/database entries

  - Menu entries, shortcuts, environment variables

  - License
    
      - Open Source/Free software
    
      - EULA’s
    
      - Dual licensing
    
      - Product keys and (time-restricted) activation
    
      - Per user/Per host licenses
    
      - License servers

A software application is much more than just the executable file you
run to start the application. Sometimes we install small simple programs
that maybe consist of just a single executable file, or we install
modules with code to expand the functionality of the tools we are using
(e.g. Visual Studio Code extensions or Python modules), or we install
huge software applications with many dependencies installed together in
the process.

When living in the open source world of Linux, we are mostly spoiled
with not having to think about licensing issues. The software we use is
"free", where "free" typically means freedom but can also mean just
"free of charge". In Windows infrastructures we also use a lot of free
software, but it is more common to use commercial software where we have
to address licensing issues. Dealing with licenses can be quite
complicated and one can quickly run into legal issues if not addressed
properly. For examples of typical licenses Norwegian companies have, see
e.g. [Microsoft 365 License
plans](https://www.microsoft.com/nb-no/microsoft-365/compare-microsoft-365-enterprise-plans?market=no)
and [Azure Pricing
Calculator](https://azure.microsoft.com/en-us/pricing/calculator/)

## Where is Software?

NSM’s "Grunnprinsipper for IKT-sikkerhet 2.0"  state in chaper 1.2
"Kartlegg enheter og programvare":

> Kartlegging av enheter og programvare er viktig for å få oversikt over
> hva som befinner seg i virksomheten. Kartleggingen av enheter bør
> avdekke både virksomhetsstyrte enheter, legitime enheter med
> begrensede rettigheter (for eksempel IoT-enheter) og ukjente enheter
> (kan være f.eks. ansattes privat utstyr eller ondsinnede enheter).
> Tilsvarende bør kartlegging av programvare dekke all programvare som
> benyttes i virksomheten, både installert av IT-avdelingen og
> uautorisert programvare. Det er viktig at virksomheten selv får
> oversikt over enheter, programvare og deres sårbarheter før angripere
> gjør det.

Just this? in
figure [8.1](#fig:software:d156e9d9e4144e648a22123df0f3cc67).

![Just
this?.<span label="fig:software:d156e9d9e4144e648a22123df0f3cc67"></span>](/home/erikhje/office/kurs/secsrv/08-software/tex/../img/apps.png)

*NO\!* Operating system, firmware, aaaaand see next slide …

In the perfect world software would be packaged and managed in a
standardized way. For instance on Windows it would be perfect if
everything was packaged as
[MSIX](https://docs.microsoft.com/en-us/windows/msix/). Unfortunately
this is not the case, there are several competing standards and
packaging systems in use, and we just have to try and make the best out
of it. On a single Windows host you probably get the best overview by
using the GUI and viewing "Apps & features". In an infrastructure with
many hosts we cannot run around studying GUIs, we have to try to get an
overview from the command line, and our best efforts are probably a
combination of the following:

1.  List installed Roles & features on a server:
    
    ``` 
    Get-WindowsFeature | 
      Where-Object {$_.InstallState -eq 'Installed'} | 
      Format-Table -Property Name,FeatureType    
    ```

2.  List install Capabilities on a Windows 10/11:
    
        Get-WindowsCapability -Online |
          Where-Object {$_.State -eq 'Installed'}

3.  Query Windows Management Instrumentation (WMI/CIM):
    
        Get-CimInstance Win32_Product |
          Format-Table -Property Name,Version

4.  Query the Chocolatey package manager:
    
        choco list --local-only

5.  Query with PowerShell’s PackageManagement:
    
        Get-Package | Format-Table -Property Name,Version,Providername

6.  ``` 
    Get-HotFix
    ```

7.  Query registry, probably the best one to use combined with any third
    party package manager you use like chocolatey (inspired by [How to
    use PowerShell to List installed
    Software](https://adamtheautomator.com/powershell-list-installed-software/))
    
    ``` 
    $registrylocations = (
      "HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall",
      "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall",
      "HKCU:\Software\Microsoft\Windows\CurrentVersion\Uninstall"
      )
    $registrylocations | Get-ChildItem | ForEach-Object {
    Write-Output "$($_.GetValue('DisplayName')) $($_.GetValue('DisplayVersion'))"
    } | Where-Object { $_.Trim() -ne '' }    
    ```

8.  The new command line tool that is currently only available as a
    preview (install from Microsoft Store) on Windows 10/11 looks
    promising (when this enters a final version available for server as
    well, and gets PowerShell cmdlets, it will be nice):
    
        winget list

9.  The most complete list on both Windows Server and Windows 10 seems
    to come from the following (but requires special compatibility
    loading and apparently does not work in Windows 11):
    
        Import-Module -UseWindowsPowerShell Appx
        Get-AppxPackage -AllUsers | 
          Format-Table -Property Name,PackageFullName

Installers and Package Formats in
figure [\[fig:software:4f73567b46174c9d95844789345659d5\]](#fig:software:4f73567b46174c9d95844789345659d5).

  - Microsoft package file formats: msi, msix, msu, appx

  - Installers/Package Managers: choco, scoop, appget, winget, ninite

  - High level package managers: npm, pypi, ppm, rubygems

Normally we talk about an *installer* and *package manager* as two
different things. The installer is the software that installs, updates,
uninstalls, etc. the software package, while the package manager is the
software that downloads software packages from repositories on the
Internet (or local servers), resolve dependencies, checks signatures,
etc. before handing the software over to the installer. E.g. in
Debian-based Linux (like Ubuntu) apt is the package manager while dpkg
is the installer. Most of the time this distinction is not visible to us
on Windows-based systems, but having an overview of where software comes
from in the installation process is important. Here is a brief list of
some installers/package managers and their default repositories:

  - Chocolatey.
    
      - Installer  
        <https://chocolatey.org/install>
    
      - Default repo  
        <https://chocolatey.org/packages>

  - Scoop.
    
      - Installer  
        <https://scoop.sh>
    
      - Default repo  
        [https://github.com...](https://github.com/ScoopInstaller/Main/tree/master/bucket)

  - Ninite. Create a mix of packages, can be updated as a unit, see
    [https://ninite.com](https://ninite.com/)

  - WinGet. See [Use the winget tool to install and manage
    applications](https://docs.microsoft.com/en-us/windows/package-manager/winget/)
    
      - Installer  
        [https://github.com...](https://github.com/microsoft/winget-cli/releases)
    
      - Default repo  
        <https://winget.azureedge.net/cache>

  - AppGet.
    
      - Installer  
        <https://appget.net/download>
    
      - Default repo  
        <https://appget.net/packages>

Microsoft probably got many ideas from AppGet when they created WinGet,
the story [The Day AppGet Died](https://keivan.io/the-day-appget-died/)
is an interesting read.

### Updates

Fresh Install vs Update in
figure [\[fig:software:04eb6aad89684a698b3f7a717ee51deb\]](#fig:software:04eb6aad89684a698b3f7a717ee51deb).

  - Where are all the files to be updated?

  - What if the update fails?

  - *Why are updates different from fresh installs?*
    
      - no physical access required?
    
      - host may not be in a “known state”
    
      - host may have “live” users
    
      - host may be gone
    
      - host may be dual-boot

*Installations/updates have to be transactional/atomic in behaviour for
both install and uninstall.*

There has to be a way to locate the files installed from previous
versions of the package, and during an update process everything have to
be carefully backed up, so it can be restored if something fails (e.g. a
power shutdown while copying files). This is one of the reasons Windows
Update takes so much time. An update process is quite complicated and
involves many changes to files and registry settings. All these changes
have to be transactional/atomic, so Windows makes a backup copy of
everything it does before moving on to allow for a full restore if
something goes wrong. These backups copies are stored in the
WinSxS-folder ("side-by-side"), which typically takes up quite a bit of
disk space:

    PS> du C:\Windows\WinSxS
    
    DU v1.62 - Directory disk usage reporter
    Copyright (C) 2005-2018 Mark Russinovich
    Sysinternals - www.sysinternals.com
    
    Files:        61355
    Directories:  24452
    Size:         7,022,037,443 bytes
    Size on disk: 7,425,290,240 bytes

Not Only Security Challenges\! in
figure [8.2](#fig:software:ba6a9bdb00984f5e826e8df8b4f35796).

![Not Only Security
Challenges\!.<span label="fig:software:ba6a9bdb00984f5e826e8df8b4f35796"></span>](/home/erikhje/office/kurs/secsrv/08-software/tex/../img/windows-update-problems.png)

In today’s world filled with cybersecurity challenges, we must never
forget that reliability is also something we need to achieve.
Unfortunately software installs and updates are risky business.
Microsoft have routines for releasing a collection of updates (aka
patches) the second Tuesday of every month: *patch Tuesday*. Even though
Microsoft does thorough testing, errors are still possible, so we should
always follow the *One, Some, Many*-technique of Limoncelli, Hogan and
Chalup : start by installing/updating/upgrading a single test-host, then
do it on your own host and your colleagues, then on an entire lab, then
an entire user group, etc. In other words, test carefully and never
update all at once.

## Threats

Supply Chain Attacks in
figure [\[fig:software:6ba6c098ec54494481539a10b145cfec\]](#fig:software:6ba6c098ec54494481539a10b145cfec).

  - event-stream (2018)  
    [Handing over responsibility for a widely used library to someone
    else...](https://github.com/dominictarr/event-stream/issues/116)

  - SolarWinds (2020)  
    ["They gained access to victims via trojanized updates to
    SolarWind’s Orion IT monitoring and management
    software"](https://www.fireeye.com/blog/threat-research/2020/12/evasive-attacker-leverages-solarwinds-supply-chain-compromises-with-sunburst-backdoor.html)

Attacks on the supply chain of software packages typically comes in one
of three ways:

  - Typesquatting  
    where the attacker creates software packages with names similar to
    the names of popular packages, hoping that users who install the
    package will misspell the name when installing it, e.g.
    `coffe-script` instead of `coffee-script` .

  - Account hijacking  
    by credential stuffing or credential theft.

  - Social engineering  
    by tricking someone to hand-over maintainer status for a package,
    like what happened with `event-stream`.

The malicious payload attackers put in these packages is commonly
focused on credential stealing, ransomware, sabotage, deploying
backdoors or cryptomining.

Duan et al  performed an extensive study during 2018-2020 where they
analyzed over one million packages from the PyPi, Npm and RubyGems
ecosystems. They found 339 new malicous packages of which three had more
than 100K downloads.

Samuel and Cappos  did an interesting study on security of package
managers back in 2009. These attacks probably does not work anymore, but
how to think about attacks on package management is still useful:

  - Replay Attack  
    If the attacker is MITM, they can serve an old version of the
    repository even though the root metadata is signed ... *Protect by
    making sure you don’t accept metadata older than what you already
    have*.

  - Freeze Attack  
    If the attacker is MITM, they can avoid updating the repository ...
    *Protect by limiting how long signed root metadata is valid*.

  - Metadata Manipulation Attack  
    If metadata is not signed (not root nor package metadata), MITM can
    easily offer newer versions of packages which are actually older
    version (with vulnerabilities the attacker know how to exploit) ...
    *Protect by requiring signed metadata*.

  - Endless Data (DOS) Attack  
    As root metadata the MITM attacker will just serve an endless file
    ... *Protect by monitoring system resources, setting hard limits or
    possibly by keeping package management cache on a separate
    partition*.

Terminology in
figure [\[fig:software:22f0333c5fb34cd7a04c50100e20feaa\]](#fig:software:22f0333c5fb34cd7a04c50100e20feaa).

  - CVE  
    *Common Vulnerabilities and Exposures*  
    [https://cve.mitre.org](https://cve.mitre.org/)

  - (NVD)  
    (National Vulnerability Database)  
    [https://nvd.nist.gov/](https://nvd.nist.gov)

  - CVSS  
    *Common Vuln. Scoring System*  
    <https://www.cvedetails.com>

[NVD gets its data from
CVE](https://cve.mitre.org/about/cve_and_nvd_relationship.html).

Terminology in
figure [\[fig:software:746889552a914a4e9f194f5e09d816ce\]](#fig:software:746889552a914a4e9f194f5e09d816ce).

Mitre also has a nice overview for Attacks:  
[Windows Matrix](https://attack.mitre.org/matrices/enterprise/windows/)

Package Management Security in
figure [\[fig:software:3626c38e43364447ae37b5ac4696585a\]](#fig:software:3626c38e43364447ae37b5ac4696585a).

*Installations are performed with high (root/administrator) privileges:
We really need to trust the packages (and the source they come from) we
are installing\!*

And of course, this also means using cryptographic hashes and digital
signatures, hashing and signing maybe repository index metadata (root
metadata), package metadata and the package data itself.

## Framework and Process

We Don’t Want This\! in
figure [8.3](#fig:software:f52fc5301d8e4a2798d0a4154837f42b).

![We Don’t Want
This\!.<span label="fig:software:f52fc5301d8e4a2798d0a4154837f42b"></span>](/home/erikhje/office/kurs/secsrv/08-software/tex/../img/update-screenshot.png)

Unfortunately many applications have their own updating mechanisms, and
keeping all software updated is much harder than one might think. Things
are getting better with less software installed locally and more
standardization of software package formats and mechanisms.

Framework in
figure [8.4](#fig:software:c142f5900f8b4f03a422240a8638051a).

![Framework.<span label="fig:software:c142f5900f8b4f03a422240a8638051a"></span>](/home/erikhje/office/kurs/secsrv/08-software/tex/../img/drawing.pdf)

Software is created by developers who write code. The first step of
*trust* is to trust the developers. Code always involves additional
third-party code, typically libraries that are dependencies in the code.
The flow of code from developers and to installed/updated software is
approximately:

  - Source  
    Source code compiled to binary *with dependencies*

  - Package  
    All files incl description of other *dependencies* zipped together
    in some standardized format

  - Remote Package Repo  
    A repository of packages somewhere on the Internet

  - Local Package Repo  
    Company internal repository of packages mostly for *testing*

  - Download/Install Mechanism  
    Install software packages

  - Ready Software Application  
    Client software (chrome, edge, ...) and server software (Exchange,
    nginx, ...)

Protection in
figure [\[fig:software:22d8a09ae0be45e589dfc31911c7d2d1\]](#fig:software:22d8a09ae0be45e589dfc31911c7d2d1).

*We must trust our repositories...*

  - Be aware of the software repositories you use and who maintains them

  - Maintain your own software repositories for your infrastructure

The problem is that its too easy to become a repository maintainer
(mirror a repository) for many software package systems, and as soon as
you are a mirror you can initiate the mentioned attacks if the
distribution is vulnerable.

## Internal Repos

Windows Server Update Services in
figure [\[fig:software:2f52bbb0806346ab8666813547916ad0\]](#fig:software:2f52bbb0806346ab8666813547916ad0).

[Deploy Windows Server Update
Services](https://docs.microsoft.com/en-us/windows-server/administration/windows-server-update-services/deploy/deploy-windows-server-update-services)

  - `Get-WsusUpdate`

  - `Get-WsusProduct`

  - `Get-WsusClassification`

  - `$WSUSServer.GetSubscription()`

  - `$WSUSSub.GetSynchronizationStatus()`

  - `$WSUSServer.GetComputerTargetGroups()`

  - `$WSUSServer.GetInstallApprovalRules()`

  - `Get-WsusComputer`

With WSUS, you don’t get access to everything you might need from the
cmdlets, so many times you will use the methods of the wsuserver-object
instead of cmdlets, e.g.

    $WSUSServer = Get-WsusServer
    $WSUSServer.GetConfiguration()

The downside to this is that these methods are not as well documented as
cmdlets. There is another good PowerShell module for WSUS at
<https://github.com/proxb/PoshWSUS>

Internal Chocolatey Repo in
figure [\[fig:software:35b0c124f70841acbb2c2c68c05c745e\]](#fig:software:35b0c124f70841acbb2c2c68c05c745e).

[See the Chocolatey
Architecture](https://docs.chocolatey.org/en-us/guides/organizations/automate-package-internalization#architecture)

From Chocolatey :

> Chocolatey allows you to create packages easily using the package
> builder but it also allows you to take packages from the Chocolatey
> Community Repository and recompile them for internal use - this is a
> process known as *package internalization*.

PackageManagement in
figure [8.5](#fig:software:f2f0e38bb8b042c3acc6763f39c44867).

PowerShell’s package management architecture

![PackageManagement.<span label="fig:software:f2f0e38bb8b042c3acc6763f39c44867"></span>](/home/erikhje/office/kurs/secsrv/08-software/tex/../img/OneGetArchitecture.png)  
<span> from [PackageManagement (aka
OneGet)](https://github.com/oneget/oneget)</span>

PowerShell has a nice framework for dealing with packages, but unified
package management is not easy, e.g updates are still problematic with
PowerShell’s high level framework. But it is a good reference to
understand the different packages and sources we have to deal with on
Windows. PowerShell’s package management can be used in some cases for
software applications, but it is mostly used for installing PowerShell
modules (which of course is also code that we need to trust):

    Get-Command -Module PackageManagement
    Get-Command -Module PowerShellGet

PackageManagement in
figure [8.6](#fig:software:c59b0060f3ba4c5093a178ec93548099).

PowerShell’s package management architecture

![PackageManagement.<span label="fig:software:c59b0060f3ba4c5093a178ec93548099"></span>](/home/erikhje/office/kurs/secsrv/08-software/tex/../img/OneGetArchitecture.png)  
<span> from [PackageManagement (aka
OneGet)](https://github.com/oneget/oneget)</span>

## Review questions and problems

1.  Describe the typical content of a software application (in other
    words: what kind of components have to be part of a typical software
    package).

2.  Why is security such a big issue for software package management
    systems?

3.  What is "typesquatting"?

4.  Why would you want to maintain your own software repository in your
    infrastructure (and direct all your clients package managers to this
    repository)?

5.  Describe the "one, some, many" technique of software updates.

6.  What do you have to consider / "be concerned about" when doing
    software updates on a host compared to doing fresh installation of a
    host?

7.  Describe the "flow of code" from developers (source code) to
    installed/updated software on its destination host.

## Lab tutorials

1.  No lab tutorials this week.

# Logging and Monitoring

## Introduction

From [NRK
Innlandet](https://www.nrk.no/innlandet/kan-ta-et-halvt-ar-for-ostre-toten-a-rette-opp-dataangrep-1.15364106):

> Østre Toten har vært uten datasystemer en måned etter hacking ØSTRE
> TOTEN (NRK): PST mener dataangrep er en av de største truslene i 2021.
> I Østre Toten innrømmer ordføreren at sikkerheten ikke var god nok.  
> …Ordføreren erkjenner at datasikkerheten ikke har vært god nok, men
> sier at de ennå ikke har konklusjonen på hvorfor det skjedde.

Several "famous" people (search the internet for reference) have stated
some variation of *there are two types of companies, those who have been
compromised and those who don’t know they have been compromised*. We
should always assume that our infrastructure will be attacked, and the
attack will be successful. We try to avoid it with preventive measures
of course, but we have to be prepared for dealing with the incident when
it happens:

  - How long did they have access?

  - Are they out now?

  - Are the systems/backups "clean"?

An important element in "Get the basics right" is a proper setup of
logging and monitoring. We need to detect if we are under attack, and if
we have been compromised, and we need to have the data necessary in
place to investigate (do the forensics) after an incident has happened.
In NSM’s "Grunnprinsipper for IKT-sikkerhet 2.0"  this is covered in
chapter 3.2 "Etabler sikkerhetsovervåkning".

## Counters

Terminology in
figure [\[fig:logmon:fcd32bd25b7f47f08fe837070a3e10eb\]](#fig:logmon:fcd32bd25b7f47f08fe837070a3e10eb).

Two categories of data:

  - Counters (numeric values: *periodic* or *accumulating*)
    
      - *CounterSet* e.g. `Process`
    
      - *Counter* e.g. `Working Set`
    
      - *Instance* e.g. `pwsh#1` (instances are numbered if there are
        more than one)
    
      - *Path* e.g. `\Process(pwsh#1)\Working Set`

  - Log events (text messages)

*Sometimes log events are generated from counters*

For logging and monitoring we have two categories of data to work with:
*counters* (numeric values) and *log events* (text messages). We will
address counters first. Counters can either be *periodic* (e.g. memory
usage) or *accumulating* (e.g. uptime). A periodic counter reports a
value for a point in time, typically a value based on the last second or
last milliseconds. A periodic counter will be reset to zero between
every measurement, so its observed behavior will be that it sometimes
increase and sometimes decrease its value. An accumulating counter is a
counter that will only increase until something specifically resets it
to zero, e.g. when a host reboots the uptime will be reset to zero, or
if a service process is restarted its total CPU-usage will be reset to
zero. Windows contains a subsystem known as Performance Logging and
Alerting (PLA). It contains *counter sets* with *counters*. A counter
can be *single-instance* (a single value) or *multi-instance* (e.g. one
for each CPU and one called `_total`). A counter is specified with a
*path*. When we have multiple instances of a counter (e.g. you have two
CPUs, thus two instances of the counter "Processor"), we will have a
special instance *\_Total* that represents the aggregate of the
individual instances.

### Implementation

Implementation in
figure [\[fig:logmon:c0f1c5ed11564240aa66a046a28dc1fe\]](#fig:logmon:c0f1c5ed11564240aa66a046a28dc1fe).

  - `Get-Counter`

  - `Get-CimInstance`

  - Use .NET directly
    
    ``` 
          New-Object System.Diagnostics.PerformanceCounter
           ("Processor Information", "% Processor Time",
            "_Total")
    ```

We have three options for accessing counters from the command line:

  - Get-Counter  
    e.g. `(Get-Counter -ListSet Processor).Counter`

  - Get-CimInstance  
    “Windows Management Instrumentation (WMI) consists of a set of
    extensions to the Windows Driver Model that provides an operating
    system interface through which instrumented components provide
    information and notification. WMI is Microsoft’s implementation of
    the Web-Based Enterprise Management (WBEM) and Common Information
    Model (CIM) standards from the Distributed Management Task Force
    (DMTF). WMI allows scripting languages (such as VBScript or Windows
    PowerShell) to manage Microsoft Windows personal computers and
    servers, both locally and remotely.” 

  - Performance Logs and Alerts (PLA) directly  
    no cmdlets, use .NET directly

Some examples (note the use of `ExpandProperty`, since a property can be
an object itself sometimes it is necessary to use `ExpandProperty` to
see all available properties/"sub-values"):

  - All CounterSets
    
    ``` 
        Get-Counter -ListSet * |
         Sort-Object CounterSetName | 
         Select-Object -Property CounterSetName |
         Out-GridView    
    ```

  - How many Counters are there?
    
    ``` 
        Get-Counter -ListSet * | 
         Select-Object -ExpandProperty Counter |
         Measure-Object
    ```

  - All Counters in a specific CounterSet
    
    ``` 
        Get-Counter -ListSet Process | 
         Select-Object -ExpandProperty Counter
    ```

We want the value called `CookedValue`. There is a raw value and a
secondary value as well, but these are combined into something that
makes sense to us, and that is the CookedValue. Think of this as the raw
value being e.g. "how many full seconds, e.g. 2 seconds" and the
secondary value being "how many milliseconds in addition to the full
seconds, e.g. 205 milliseconds" while the CookedValue will then be 2.205
seconds which is the value that makes sense to us. Some examples of
retrieving values:

  - Show the counter
    
    ``` 
      Get-Counter -Counter '\Process(_Total)\Working Set'
    ```

  - The CounterSamples-object
    
    ``` 
      (Get-Counter -Counter `
        '\Process(_Total)\Working Set').CounterSamples
    ```

  - The values
    
    ``` 
      (Get-Counter -MaxSamples 3 -Counter `
        '\Process(_Total)\Working Set').CounterSamples.CookedValue
    ```

If we want to retrieve counters from remote hosts we should do this:

``` 
  # Do this:
  $scriptblock = { 
    Get-Counter '\Processor(_total)\% Processor Time'
    }
  Invoke-Command -ComputerName dc1,srv1 `
                 -ScriptBlock $scriptblock
```

You will see many posts on the Internet referring to the old way of
retrieving from remote hosts, which is deprecated and not something we
should do but we mention it here since you might run into it and wonder
why we don’t do this:

``` 
  Get-Counter -ComputerName dc1,srv1 `
    '\Processor(_total)\% Processor Time'
```

If you want to see all cmdlets that support this old-style deprecated
way of accessing remote hosts you can run the following command line:

``` 
  Get-Command | 
    Where-Object { 
      $_.Parameters.Keys -contains "ComputerName" -and 
      $_.Parameters.Keys -notcontains "Session"}
```

We should only access remote hosts using PowerShell’s remoting which is
what we use when we use `Invoke-Command`. See also chapter seven about
remoting.

Sometimes it is necessary to analyze counter values with an application
like Excel or similar spreadsheet app. To export counter values for use
in a spreadsheet we can do:

``` 
  $counters = '\Processor(0)\% Processor Time',
              '\Process(_Total)\Working Set'

  # Export-Counter is only in Windows PowerShell (5.1)
  Get-Counter -Counter $counters -MaxSamples 10 |
   Export-Counter -Path C:\PerfLogs\cap.csv -FileFormat csv

  # Need to do this in PowerShell Core instead
  Get-Counter -Counter $Counters -MaxSamples 10 | 
  ForEach-Object {
    $_.CounterSamples | ForEach-Object {
      [pscustomobject]@{
        TimeStamp = $_.TimeStamp
        Path = $_.Path
        Value = $_.CookedValue
      }
    }
  } | Export-Csv -Path $home\out.csv -NoTypeInformation
    
  Get-Content out.csv | Set-Clipboard
  # paste in Excel, Data, Text to Columns)
```

### GUI Tools

GUI Tools in figure [9.1](#fig:logmon:9089b2a9e5e54e45a3af2a6d9c285937).

Task Manager etc., we use *Windows Admin Center*

![GUI
Tools.<span label="fig:logmon:9089b2a9e5e54e45a3af2a6d9c285937"></span>](/home/erikhje/office/kurs/secsrv/09-logmon/tex/../img/windowsadmincenter.png)

On Windows we can view counters graphically with tools like Task
Manager, Performance Monitor, Resource Monitor and probably others. We
use Windows Admin Center since we want to introduce this modern official
tool from Microsoft for managing small Windows environments. On a larger
scale we would collect counter values into a bigger monitoring system,
and probably use something like [Grafana](https://grafana.com/) to
visualize the data in real-time. For monitoring the servers that make up
the private cloud SkyHiGh, NTNU uses Munin and if you are inside the
NTNU network you can see [an example here of the data being written to
SkyHiGhs storage cluster
Ceph](https://munin.skyhigh.iik.ntnu.no/ceph-week.html).

## Log Events

### Terminology

Terminology in
figure [\[fig:logmon:d0a9d9c87c534d9aa41ed343f89f1a40\]](#fig:logmon:d0a9d9c87c534d9aa41ed343f89f1a40).

  - Event provider  
    An application that generates events

  - Event  
    A log entry

  - Event log  
    A file containing events (Application, Security, System plus many
    app specific)

  - Event type  
    Critical, Error, Warning, Information, Verbose, Debug, Success
    Audit, Failure Audit

  - Event ID  
    A number representing specific event (*only unique per
    source/provider*)

  - Log mode  
    Autobackup, Circular, Retain

A log entry (aka a log record) is composed of many fields. Unfortunately
there is no standard for log entries, but most of the time there are
some fields that appear to be quite standard such as timestamp,
hostname, process name/source and message. *On Windows (as opposed to
Linux), there is also the event-ID and the event-type*. Some event logs
(categories) like Application, Security and System receive events from
many sources while many applications have their own event logs, e.g.  
’Microsoft-Windows-WindowsUpdateClient/Operational’

### Log Files and Mode

Eventlogs can be in [one of three
different](https://docs.microsoft.com/en-us/dotnet/api/system.diagnostics.eventing.reader.eventlogmode)
`EventLogMode` (sometimes represented as the name e.g. "AutoBackup" and
sometimes represented with a value corresponding to "AutoBackup" which
is "1"):

  - AutoBackup (`1`)  
    Archive the log when full, do not overwrite events. The log is
    automatically archived when necessary. No events are overwritten.

  - Circular (`0`)  
    New events continue to be stored when the log file is full. Each new
    incoming event replaces the oldest event in the log.

  - Retain (`2`)  
    Do not overwrite events. Clear the log manually rather than
    automatically.

Being aware of this is useful because log files can easily fill up a
disk partition if we increase logging from frequently used services
(e.g. a web server) and let the log mode be "Retain". Logging all the
details related to any kind of processing the web server does use a lot
of resources (CPU time and disk space). At the same time, if we use
"Circular" logs we might not have old log entries available for forensic
investigation when an incident happens. We can see which log is in which
mode with  
`Get-WinEvent -ListLog * | Format-Table -Property LogName,LogMode`  
and we can see how many are in each mode with  
`Get-WinEvent -ListLog * | Group-Object -NoElement -Property LogMode`

Log entries are records in log files, and the log files are stored in  
`C:\Windows\System32\winevt\Logs\`:

    # How many logfiles?
    Get-ChildItem C:\Windows\System32\winevt\Logs\ | 
     Measure-Object
    
    # How many of each size?
    Get-ChildItem C:\Windows\System32\winevt\Logs\ | 
       Group-Object -Property Length
    
    # Which one most recently written to?
    Get-ChildItem C:\Windows\System32\winevt\Logs\ | 
     Sort-Object -Property LastWriteTime

The log files are managed and written to by the EventLog service which
several other services depend on.

    PS> (Get-Service EventLog).DependentServices
    
    Status   Name               DisplayName
    ------   ----               -----------
    Stopped  Wecsvc             Windows Event Collector
    Stopped  NcdAutoSetup       Network Connected Devices Auto-Setup
    Stopped  AppVClient         Microsoft App-V Client
    Running  netprofm           Network List Service
    Running  NlaSvc             Network Location Awareness

### Three cmdlets

Three cmdlets in
figure [\[fig:logmon:6aaba572f2e344fcbf2b491b309a5f2b\]](#fig:logmon:6aaba572f2e344fcbf2b491b309a5f2b).

``` 
  $id=(Get-WinEvent -LogName Security -MaxEvents 1).RecordId
  Get-EventLog -LogName Security | 
    Where-Object {$_.Index -eq $id}
  
  Get-WinEvent -LogName Security | 
    Where-Object {$_.RecordId -eq $id}
  
  Get-CimInstance Win32_NTLogEvent | 
    Where-Object {$_.RecordNumber -eq $id}
```

*We will use Get-WinEvent*

There are three approaches in PowerShell to retrieving log events:
`Get-EventLog`, `Get-WinEvent` and `Get-CimInstance Win32_NTLogEvent`.
Sometimes it faster and/or easier to use one or the other. The newest
one and probably the one you would try first is `Get-WinEvent` since it
supports retrieving all logs as opposed to the others:

``` 
  (Get-WinEvent -ListLog * | 
     Where-Object {$_.RecordCount -gt 0} | Measure-Object).Count

  (Get-CimInstance Win32_NTLogEvent | 
     Select-Object -Property Logfile | 
      Sort-Object -Property Logfile | 
       Get-Unique -AsString | Measure-Object).Count

# Get-EventLog is only in Windows PowerShell (5.1)
  (Get-EventLog -LogName * | Measure-Object).Count     
```

`Get-WinEvent` is the recommended cmdlet to use in PowerShell Core.

The three cmdlets return different types of objects, which means
differences in property-names as well, so `Get-Member` and
`Select-Object -Property *` comes in handy to understand the
differences. E.g. if you want to retrieve a specific log entry we have
to use different property-names for each of the three cmdlets:

    Get-EventLog -LogName Security | 
      Where-Object {$_.Index -eq 20905}
    
    Get-WinEvent -LogName Security | 
      Where-Object {$_.RecordId -eq 20905}
    
    Get-CimInstance Win32_NTLogEvent | 
      Where-Object {$_.RecordNumber -eq 20905}

Here is an overview of differences in the most important property names:

| **Field**  | **Get-EventLog** | **Get-WinEvent**   | **Get-CimInstance** |
| :--------- | :--------------- | :----------------- | :------------------ |
| Time stamp | `TimeGenerated`  | `TimeCreated`      | `TimeGenerated`     |
| Host       | `MachineName`    | `MachineName`      | (missing)           |
| Source     | `Source`         | `ProviderName`     | `SourceName`        |
| Message    | `Message`        | `Message`          | `Message`           |
| Type       | `EntryType`      | `LevelDisplayName` | `Type`              |
| Event ID   | `InstanceId`     | `Id`               | `EventCode`         |

### Using Get-WinEvent

Typical Usage in
figure [\[fig:logmon:eed6e29242994184a0cd2446901f499c\]](#fig:logmon:eed6e29242994184a0cd2446901f499c).

  - Most recent entries

  - Find specific EventIDs

  - Search all logs

  - Ignore "Information" level

  - Search a specific time period

#### Most Recent

Most recent in the common logs:

``` 
  Get-WinEvent -MaxEvents 10 -LogName `
    Application,System,Security,"Windows PowerShell" |
    Format-Table -Property `
     LogName,TimeCreated,ID,LevelDisplayName,Message
```

#### Specific EventIDs

Finding specific EventIDs:

    # Note the use of 'ExpandProperty'
    # to see entire message
    
    Get-WinEvent  -MaxEvents 10 -FilterHashtable `
      @{ LogName='Security'; Id='4672','4624' } |
      Select-Object -Last 1 -ExpandProperty Message

Levels of "seriousness":

|               |   |
| :------------ | :- |
| Verbose       | 5 |
| Informational | 4 |
| Warning       | 3 |
| Error         | 2 |
| Critical      | 1 |
| LogAlways     | 0 |

You can find lists of event IDs that matter many places:

  - See Event IDs that matter at <https://adsecurity.org/?p=3299>

  - [www.malwarearchaeology.com/cheat-sheets](www.malwarearchaeology.com/cheat-sheets)

  - [Threat Detection with Windows Event
    Logs](https://medium.com/adarma-tech-blog/threat-detection-with-windows-event-logs-59548f4f7b85)

If you find a resource which has a list of event IDs that you want to
search your logs for, you can copy and paste the list into a file e.g.
`a.txt` and extract all the event IDs into an array like this

    $IDs = Select-String -Pattern '\d{4}' .\a.txt -AllMatches |
     Select-Object -ExpandProperty Matches |
      Select-Object -Property Value

Later in this chapter you will learn about *regular expressions* like
`\d{4}`.

#### Search all

Query all logs:

    # This fails due to 256 max
    Get-WinEvent -MaxEvents 10
    
    $logs = (Get-WinEvent -ListLog * |
      Where-Object {$_.RecordCount} | 
      Select-Object -ExpandProperty Logname)
    
    # 25 most recent Warning and Information
    Get-WinEvent -MaxEvents 25 `
      -FilterHashtable @{Logname=$logs;Level=3,4}

#### Ignore

Ignore Information-level:

    $Date = (Get-Date).AddDays(-2)
    
    # Ignore "Information" by suppress level 4
    $filter = @{
      LogName='Application'
      StartTime=$Date
      SuppressHashFilter=@{Level=4}
    }
    
    Get-WinEvent -FilterHashtable $filter

#### Time period

Find entries in a specific time period (remember *Filter left, Format
right*):

    $start = (Get-Date).AddDays(-1)
    $end   = Get-Date
    
    # DO THIS
    Get-WinEvent -FilterHashtable `
     @{ logname="System"; starttime=$start; endtime=$end }
    
    # DO NOT DO THIS (INEFFICIENT!)
    Get-WinEvent -LogName System | 
     Where-Object { $_.TimeCreated -gt $start `
      -and $_.TimeCreated -lt $end }

NSM’s "Grunnprinsipper for IKT-sikkerhet 2.0"  chapter 3.2.4 is worth
reciting in full:

> Beslutt hvilke data som er sikkerhetsrelevant og bør samles inn. For
> delene nevnte i 3.2.3 bør man som minimum samle inn a) data relatert
> til tilgangskontroll (vellykkede og mislykkede pålogginger) på enheter
> og tjenester og b) administrasjons- og sikkerhetslogger fra enheter og
> tjenester i IKT-systemene. For klienter bør man i tillegg som minimum
> registrere c) forsøk på kjøring av ukjent programvare (ref. 2.3.2) og
> d) forsøk på å få forhøyede systemrettigheter ("privilege
> escalation").

## Regular Expressions

Regular Expressions in
figure [9.2](#fig:logmon:390137a43e4c48caa0f37a926e570e12).

![Regular
Expressions.<span label="fig:logmon:390137a43e4c48caa0f37a926e570e12"></span>](/home/erikhje/office/kurs/secsrv/09-logmon/tex/../img/regular_expressions.png)

You might have noticed that the author sometimes has mentioned *regular
expressions* (RegEx) without actually explaining what they are. It is
time to do so.

### RegEx vs Wildcards

A regular expression is a combination of normal characters and special
characters that we use for searching text. It is similar to the use of
*wildcards*, but more general. Let’s illustrate with an example: we
would like to search for all file names that end with the character `l`
followed by exactly two characters\[4\]. In other words all files that
end with `lnk`, `lds`, `lnt`, etc. If we were to use wildcards we could
to this:  
`Get-ChildItem -Recurse -File | Where-Object {$_.Name -like '*l??'}`  
The only wildcards we actually use are

  - \*  
    Match any character zero or more times.

  - ?  
    Match one character in that position.

If we were to use regular expressions we would do this:  
`Get-ChildItem -Recurse -File | Where-Object {$_.Name -match
'.*l.{2}$'}`  
Wildcards are sort-of a very simple version of the more general
technology regular expressions. We can interpret the regular expression
`.*l.{2}$`:

  - `.`  
    The special character `.` matches any character (but just one
    character).

  - `.*`  
    The special character `*` is a *quantifier*, it means "the previous
    character zero or more times". This means that the regular
    expression `.*` is the same as the wildcard `*`

  - `.*l`  
    This means the character `l` with anything in front of it.

  - `.*l.{2}`  
    The special characters `{n}` is also a quantifier, it means "the
    previous character exactly `n` times".

  - `.*l.{2}$`  
    The special character `$` is an *anchor*, it means "end of the
    line". There is also an anchor for "beginning of the line" of
    course, that is the special charater `^`

You can read all the details [about
wildcards](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_wildcards)
and [about regular
expressions](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_regular_expressions)
in the documentation at Microsoft. But note the following important
difference when it comes to which *operator* to use when applying
wildcards or regular expressions:

> When using wildcards we use the operator `-like` (and the
> corresponding case-sensitive `-clike`), but if we use regular
> expressions we use the operator `-match` (and the corresponding
> case-sensitive `-cmatch`).

Single Characters in
figure [\[fig:logmon:10a76318ca3c45478594d900b07c5346\]](#fig:logmon:10a76318ca3c45478594d900b07c5346).

Describing *a single* character:

|   Operator    | Meaning                      |
| :-----------: | :--------------------------- |
|      `.`      | Any single character         |
|   `[abcd]`    | One of these characters      |
|   `[^abcd]`   | Any one but these characters |
| `[a-zA-Z0-9]` | A character in these ranges  |

This is how we can describe a single character. Say we want to describe
a sequence of four characters:

1.  The first character should be `A` or `B`.

2.  The second character should not be `C`.

3.  The third character should be a digit.

4.  The fourth character can be anything but there must be a fourth
    character.

The regular expression `[AB][^C][0-9].` fulfills these requirements.
Let’s have some fun data to work with. Here is a list of ten million
typical passwords:

    # combine the URL to one line of course:
    curl -o pw.txt https://raw.githubusercontent.com/danielmiessler/SecLists/
                   master/Passwords/xato-net-10-million-passwords-1000000.txt
    # Let's count how many lines there are by using 
    # a RegEx that matches anything:
    PS> (Select-String '.*' pw.txt | Measure-Object).Count
    1000000

If we were on Linux we would use `grep` (which of course also supports
regular expressions). In PowerShell the parallell to grep is
`Select-String` (if we need to use the old `cmd` command line, we can
use the command `findstr`), so the above example we can apply like this:

    Select-String -Pattern '[AB][^C][0-9].' -Path pw.txt
    
    # but we typically omit the parameters and just type
    Select-String '[AB][^C][0-9].' pw.txt
    
    # if we want the search to be casesensitive we have to do
    Select-String '[AB][^C][0-9].' pw.txt -CaseSensitive

Since regular expressions contain special characters, we put them in
*single quotes* (`'`) and not *double quotes* (`"`) to make sure none of
the special characters are interpreted and expanded as part of the
PowerShell command line.

Sometimes you will see some aliases for describing characters. We say
"sometimes" because different languages support different dialects of
regular expressions, and we only include what PowerShell supports. E.g.
you can use `\d` ("decimal digit") instead of `[0-9]` and `\w` instead
of `[a-zA-Z_0-9]` ("word character"). If you want to match the opposite
of these, you can use the uppercase versions `\D` ("not a decimal
digit") and `\W` ("not a word character"). `\s` matches whitespace, `\S`
matches non-whitespace and `\t` matches the tab-character.

Anchoring and Grouping in
figure [\[fig:logmon:0ad7c4ac68ef4899aedf3909afc54535\]](#fig:logmon:0ad7c4ac68ef4899aedf3909afc54535).

Anchoring:

| Operator | Meaning           |
| :------: | :---------------- |
|   `^`    | Beginning of line |
|   `$`    | End of line       |

Grouping:

| Operator | Meaning |
| :------: | :------ |
|   `()`   | Group   |
|   `\|`   | OR      |

The previous example `[AB][^C][0-9].` generated numerous matched lines,
so maybe we need to narrow our search:

  - The match should be at the beginning of the line:  
    `^[AB][^C][0-9].`

  - The match should be at the end of the line:  
    `[AB][^C][0-9].$`

  - The entire line should be only these four characters:  
    `^[AB][^C][0-9].$`

Note that `^` at the beginning of a regular expression means "beginning
of the line" while `^` after `[` means "none of these characters".

Let’s modify our example slightly: instead of the first character being
`A` or `B`, we want there to be three characters that should be either
`ABC` or `DEF`:  
`(ABC|DEF)[^C][0-9].`

Quantifiers in
figure [\[fig:logmon:18b54fb60bff45749f126060ac1ec18e\]](#fig:logmon:18b54fb60bff45749f126060ac1ec18e).

Repetition operators/Modifiers/Quantifiers:

| Operator | Meaning                        |
| :------: | :----------------------------- |
|   `?`    | 0 or 1 time                    |
|   `*`    | 0 or more times                |
|   `+`    | 1 or more times                |
|  `{N}`   | N times                        |
|  `{N,}`  | At least N times               |
| `{N,M}`  | At least N but not more than M |

e.g. finding URLs:  
`http[s]?://[^"]+`

e.g. each line should be an email address:  
`^[A-Za-z0-9._-]+@[A-Za-z0-9.-]+$`

We have already seen the quantifier (sometimes called repetition
operator or modifier) `*` used in `.*` meaning "any character zero or
more times". Alternatives to `*` is `?` ("zero or one time") and `+`
("one or more times"). E.g. in the example with matching a valid email
address, there has to be at least one character in front and after the
at sign (`@`), so we use `+` instead of `*`.

The braces `{}` can be used to match an exact number of occurrences. In
our context this can be useful for checking for password length:

  - Any passwords that are at least twelve characters long:  
    `^.{12,}$`  
    or  
    `.{12}`

  - Any passwords that are at exactly twelve characters long:  
    `^.{12}$`

  - Any empty passwords:  
    `^.{0}$`  
    or  
    `^$`

  - Any passwords with seven characters or fewer:  
    `^.{0,7}$`

One pitfall when using regular expressions is trying to match everything
that is allowed in a string instead of *inverting* the match: describing
what not to match. An example of this is matching URLs in a webpage. A
URL starts with `http://` or `https://` followed by many characters
which are hard to describe. But what we know is that after the end of
the URL there will be a double quote, so let’s simply say that after
`http[s]?://` there will be one or more of any character except double
quote, hence a simple regex for matching URLs is `http[s]?://[^"]+`

Sometimes it is very useful to be able to extract parts of the match. We
can do this by specifying the part with `(part)` and refer to it later
using `$matches[1]`, `$matches[2]`, etc. `$matches[0]` matches the
entire expression:

    'zalo@oppvask.com','a@b@' | ForEach-Object {
    if ($_ -match
      '^[A-Za-z0-9._-]+@([A-Za-z0-9.-]+)$') {
        Write-Output "Valid email: $($matches[0])"
        Write-Output "Domain is $($matches[1])"
      } else {
        Write-Output "Invalid email address!"
      }
    }
    # or
    'zalo@oppvask.com','a@b@' -match '^[A-Za-z0-9._-]+@([A-Za-z0-9.-]+)$'
    $Matches[1]

Regular expressions can also be used with the operators
[replace](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_comparison_operators#replacement-operator)
and
[split](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_split).
A final general note about regular expressions in PowerShell:

> If you want a literal match for one of the special characters, e.g.
> you want to search for passwords that contain `$` or `*`, you have to
> protect the special character with `\` or `` ` ``: `[\$\*]` or
> ``[`$`*]``

### RegExp and Logs

Regular expressions are extremely useful for searching in logs. They are
especially useful on Linux and when collecting logs that do not have a
proper structure, but they are also very useful for eventlog on Windows.
We know that eventlog entries on Windows can be retrieved in PowerShell
as an object with properties which we can mostly search without having
to use regular expressions, but the message-property is a bit different
from other properties and a good candidate for use of regular
expressions. But the message-property is slightly different from the
typical property, it is a *NoteProperty*, meaning it is created by
PowerShell, and in this case it means it has some values that we can
extract without using a regular expression on the entire message.
Consider the following PowerShell-session:

    PS> $LogEntry = Get-WinEvent -LogName Security -MaxEvents 1
    
    PS> ($LogEntry | Get-Member -Name Message).MemberType
    NoteProperty
    
    PS> $LogEntry.Message
    An account was logged off.
    
    Subject:
            Security ID:            S-1-5-18
            Account Name:           DC1$
            Account Domain:         SEC
            Logon ID:               0x48B853A
    
    Logon Type:                     3
    
    This event is generated when a logon session is destroyed.
    It may be positively correlated with a logon event using 
    the Logon ID value. Logon IDs are only unique between
    reboots on the same computer.
    
    PS> $LogEntry.Properties.Value[2]
    SEC

The message seems to have some internal structure, and indeed it has.
The values for Security ID, Account Name, Account Domain and Login ID
can be retrieved from the `Value`-array of the `Properties`-property.
Which values exist depends on the EventID (the author is still looking
for exact information on this, there should be a table in a Microsoft
reference somewhere describing which EventIDs map to which values):

    PS> Get-WinEvent -LogName Security -MaxEvents 10 |
        Select-Object -Property `
        Id,@{Name="NumValues";Expression={$_.Properties.Value.Length}}
    
      Id NumValues
      -- ---------
    4672         5
    4672         5
    4624        27
    4624        27
    4672         5
    4624        27
    5379        11
    5379        11
    5379        11
    5379        11

This means that sometimes we need to search the entire message-property:

    # e.g.
    Get-WinEvent -MaxEvents 100 -LogName Security | 
      ForEach-Object {
        if ($_.Message -match ".*was successfully.*") 
          { Write-Output "$($_.Id): $($Matches[0])" }
      }
    # or
    Get-WinEvent -MaxEvents 100 -LogName Security |
       ForEach-Object {
         if ($_.Message -match ".*Account Name:\s+Administrator.*")
           { Write-Output "$($_.Id): $($Matches[0])" }
       }

but the last example we could do much simpler if we know the EventID:

    Get-WinEvent -MaxEvents 100 -LogName Security | 
      Where-Object {$_.Id -eq '5379'} | 
      Select-Object -Property `
        @{Name="Account";Expression={$_.Properties.Value[1]}}

## Monitoring

Monitoring is about getting an overview of the most important values and
messages you have learned about in this chapter. A good rule number one
for a general monitoring system is to start your monitoring from users
perspective. E.g. "Test if product can be bought in the webshop". Any
monitoring system starts by retrieving data from hosts. This is done by
having "agents" (typically service processes) on the hosts that collect
and send data to a central service. Examples of central services are
Splunk and Elasticsearch. Note how counters and log messages are
related. If a counter exceeds a certain threshold an agent-plugin might
generate a log message which is the collected by a central service. An
agent-plugin will either read counter values directly or do tests by
itself, e.g. to check if a port is reachable (and generate a log message
if it is not).

You will learn more about monitoring in the course "DCSG2003 - Robuste
og skalerbare tjenester". In this course we will simply use Windows
Admin Center as our "monitoring interface" to counters and log events.

## Review questions and problems

1.  Use `Get-Counter -ListSet` to list all the counters in the
    CounterSet `Event Log`

2.  Use `Get-Counter` to show the value in the counter `Events/Sec`.

3.  Use `Get-Counter` to show only the `CookedValue` in the counter
    `Events/Sec` every second for 10 seconds. The output should be like
    this (where xxxxx is your command line):
    
        PS> xxxxx
        
        CookedValue
        -----------
        10.9904350243983
        6.76204890854735
        9.88813063407143
        8.87613268082581
        12.8631527765561
                       0
        7.89924590836342
                       0
        15.6938306276679
        7.77853121495407

4.  (**KEY PROBLEM**) Write a PowerShell script or function `Get-IOPS`
    which outputs the sum of disk reads and writes for the last second.
    You can find these two Counters in the CounterSet `PhysicalDisk`. If
    you want to know more about what IOPS is see
    [https://en.wikipedia.org/wiki/IOPS](https://en.wikipedia.org/w/index.php?title=IOPS&oldid=997276912)

5.  Use `Get-WinEvent` to list the 25 newest Security events. Choose one
    of them and output all properties in full text from that event.

6.  Use `Get-WinEvent` to find all Security events that was logged the
    first hour after midnight on March 22. Hint: you can create a date
    object with  
    `[datetime]$x = "03/22/2022"`

7.  Use `Invoke-Command` to execute a search on `dc1` for System events
    during the last five days with `LevelDisplayName` Warning.

8.  (**KEY PROBLEM**) We know from [Event Log Analysis Part 2 — Windows
    Forensics Manual
    2018](https://medium.com/@lucideus/event-log-analysis-part-2-windows-forensics-manual-2018-75710851e323)
    that interesting events for Windows updates are Event IDs 19, 20, 43
    and 44.
    
    1.  Create an array with the these Event IDs.
    
    2.  Use Get-WinEvent with the parameter -FilterHashtable to search
        the System eventlog for these Event IDs.

9.  Download the file with ten million passwords mentioned in the
    chapter. Use `Select-String` to find passwords that
    
    1.  have more than 24 characters
    
    2.  have either the sequence `$$` or `!!`
    
    3.  have at least two `$`
    
    4.  only contain alphabetic characters (a..z and A..Z)
    
    5.  only contain alphabetic characters (a..z and A..Z) and are six
        characters or less long

10. (**KEY PROBLEM**) On DC1, use `Get-WinEvent` to search for all
    EventID 4624 in the Security eventlog (restrict to the last 200 log
    entries to avoid too long output), then add the following pipeline
    to the command:
    
      - use `Where-Object` to select only those log events that have
        `$_.Properties.Value[5]` that starts with `Adm`,
    
      - expand the Message property of those log events,
    
      - pipe to `Select-String` which should search for the text "Logon
        process:" followed by one or more white space followed by either
        the text "Kerberos" or "NtLmSsp", add the parameter
        `-AllMatches` to Select-String
    
      - finally use ForEach-Object to print `$_.Matches.Value`
    
    The output should be something like:
    
    ``` 
      Logon Process:          Kerberos
      Logon Process:          Kerberos
      Logon Process:          Kerberos
      Logon Process:          NtLmSsp
      Logon Process:          Kerberos
      Logon Process:          Kerberos
      Logon Process:          Kerberos
      Logon Process:          Kerberos
      Logon Process:          Kerberos
      Logon Process:          Kerberos
    ```

## Lab tutorials

1.  **NOW LET’S USE A GUI\!** Do this on MGR. Log in as domain
    administrator.
    
    1.  Install and launch Windows Admin Center
        
            choco install windows-admin-center
            & 'C:\Program Files\Windows Admin Center\SmeDesktop.exe'
            # note that first time it will search for updates
            # to extensions and install them, this takes
            # a few minutes
    
    2.  Add all the Windows hosts
        
        1.  Add
        
        2.  Search Active Directory
        
        3.  Enter `*` in the search box (for servers) and DC1 and SRV1
            should show up in the search, add both of them
    
    3.  Choose one of the hosts you have in Windows Admin Center, go to
        Performance Monitor
        
        1.  Blank workspace
        
        2.  Add counter
        
        3.  Select object, `Processor`
        
        4.  Select instance, `_Total`
        
        5.  Select counter, `% Processor Time`
    
    4.  Choose one of the hosts you have in Windows Admin Center, go to
        Events
        
        1.  Choose "System" under "Windows Logs"
        
        2.  Sort by Level, can you find any log event at Error-level
        
        3.  View the details of an Error log event
    
    5.  Install the PowerShell module and check out which cmdlets you
        can use with Windows Admin Center
        
        ``` 
        Install-Module 'PSWindowsAdminCenter'
        Get-Command -Module 'PSWindowsAdminCenter'         
        ```

# Security: Attacks

## Introduction

### Asset Value

The starting point of addressing security is the question of *which
assets are we trying to protect, and what is the value of each asset?*
We have previously mentioned that one of the goals of active directory
is asset management, so this is a good place to start. What will happen
to our company if active directory is not available? Its data is stolen?
Its data is deleted? The Active Directory service is an asset itself
that we need to protect, and probably an asset of high value. This also
captures an interesting point: we need to protect data (both in *rest*
and in *transit*) and services/processes. Technical assets (e.g. an
operating system process like the services of active directory) we need
to protect can have high value themselves, or they can have high value
because they can be used to reach other assets of high value (e.g. water
supply). Some examples of assets we are trying to protect in our context
are:

  - Accounts.

  - Services/processes.

  - User and company data (including their backups).

### Risk Assessment

When you know which assets you need to protect and the value of those
assets, you do a risk assessment to see where you need to invest in
protecting your systems. An *information security risk* is a product of
the three factors: *threat*, *vulnerability* and *asset*. More generally
it is often said that risk is likelihood multiplied by consequence.
Based on your risk analyses you create an information security
management system. You will learn more about this in later courses, but
we mention it now, so you know the context. We will study our part of
the bigger picture, and our part is to understand the threats and how to
mitigate them in a Windows infrastructure.

## Threats

### Cyber Kill Chain

The Unified Kill Chain in
figure [10.1](#fig:attack:6ba01bb566c449739ac774bc90cd7625).

![The Unified Kill
Chain.<span label="fig:attack:6ba01bb566c449739ac774bc90cd7625"></span>](/home/erikhje/office/kurs/secsrv/10-attack/tex/../img/The_Unified_Kill_Chain.png)

[Fox-IT](https://commons.wikimedia.org/wiki/File:The_Unified_Kill_Chain.png),
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode)

For us to be good at protecting a Windows infrastructure, we need to
know about how attacks are performed. A well-know model for this is the
*Cyber Kill Chain*, originally developed på Lockheed Martin in 2011,
extended and combined with Mitre Att\&ck in the Unified Kill Chain by
Paul Pols . The kill chain provides us with an overview of the typical
sequence of an attack. To defend against these attacks we need to know
more details about how they are actually implemented, and for this we
have Mitre Att\&ck which is a model at a lower level than the cyber kill
chain, and shows us what is "behind" each step in the cyber kill chain.

In practice what we are talking about is that a typical attack would go
something like this (in chronological sequence, and very simplified):

1.  Get access to a user account on a computer (maybe just a local
    account without administrator privileges).

2.  Escalate privileges to become local administrator.

3.  As local administrator install software to run as a service waiting
    for a domain user to log in, when a domain user logs in, steal those
    credentials.

4.  When you have a domain user you can access other machines (move
    laterally).

5.  Find a way from your domain user to become domain administrator.

6.  As domain administrator, deploy malware with group policy or other
    remoting mechanism (or just steal data).

### Mitre Att\&ck

Mitre Att\&ck Matrix in
figure [10.2](#fig:attack:34f8a1c995b74838b81327c6c33301db).

![Mitre Att\&ck
Matrix.<span label="fig:attack:34f8a1c995b74838b81327c6c33301db"></span>](/home/erikhje/office/kurs/secsrv/10-attack/tex/../img/attack-matrix.pdf)

To understand and get an overview of the threat landscape we can use the
Mitre Att\&ck knowledge base . Mitre Att\&ck is based on the actual
activities from known threat actors (groups of people), including those
actors known as *APTs (Advanced Persistant Threats)*. Someone who might
attack us, like an APT-group or other actors (individuals, groups,
companies, organizations, countries) is referred to as an *adversery* (a
synonmym for "opponent" or "enemy"). Mitre gathers information from
known attacks and integrates that information into the knowledge base.
We can use Mitre Att\&ck to learn about typical attack patterns that
have been used against infrastructures similar to ours. Mitre separates
between different *technology domains*: enterprise, mobile or ICS
(industrial control systems). We will stick to focusing on the
enterprise technology domain (which is the domain for our Windows
infrastructure). The knowledge base is structured based on *TTPs
(Tactics, Techniques, Procedures)* (note: the example texts are quotes
copied directly from the [Mitre Att\&ck knowledge
base](https://attack.mitre.org)):

  - Tactic  
    represents the reason for performing an action, as of March 2022
    there are 14 enterprise tactics in Mitre Att\&ck, e.g. [TA0001
    Initial Access](https://attack.mitre.org/tactics/TA0001):
    
    > Initial Access consists of techniques that use various entry
    > vectors *to gain their initial foothold within a network*.
    > Techniques used to gain a foothold include targeted spearphishing
    > and exploiting weaknesses on public-facing web servers. Footholds
    > gained through initial access may allow for continued access, like
    > valid accounts and use of external remote services, or may be
    > limited-use due to changing passwords.

  - Technique  
    represents how to achieve a tactical objective, as of March 2022
    there are 188 enterprise techniques in Mitre Att\&ck, e.g. [T1195
    Supply Chain Compromise](https://attack.mitre.org/techniques/T1195):
    
    > Adversaries may manipulate products or product delivery mechanisms
    > prior to receipt by a final consumer for the purpose of data or
    > system compromise.Supply chain compromise can take place at any
    > stage of the supply chain including:
    > 
    >   - Manipulation of development tools
    > 
    >   - Manipulation of a development environment
    > 
    >   - Manipulation of source code repositories (public or private)
    > 
    >   - Manipulation of source code in open-source dependencies
    > 
    >   - Manipulation of software update/distribution mechanisms
    > 
    >   - Compromised/infected system images (multiple cases of
    >     removable media infected at the factory)
    > 
    >   - Replacement of legitimate software with modified versions
    > 
    >   - Sales of modified/counterfeit products to legitimate
    >     distributors
    > 
    >   - Shipment interdiction

  - (Sub-technique)  
    are present when there are multiple ways of performing a specific
    technique, as of March 2022 there are 379 enterprise sub-techniques
    in Mitre Att\&ck, e.g. [T1195.001 Supply Chain Compromise:
    Compromise Software Dependencies and Development
    Tools](https://attack.mitre.org/techniques/T1195/001):
    
    > Adversaries may manipulate software dependencies and development
    > tools prior to receipt by a final consumer for the purpose of data
    > or system compromise. Applications often depend on external
    > software to function properly. Popular open source projects that
    > are used as dependencies in many applications may be targeted as a
    > means to add malicious code to users of the dependency.
    > 
    > Targeting may be specific to a desired victim set or may be
    > distributed to a broad set of consumers but only move on to
    > additional tactics on specific victims.

  - Procedure  
    is the implemention of techniques/sub-techniques that have been used
    by threat actors, e.g. for supply chain attack:
    
    > XCSSET adds malicious code to a host’s Xcode projects by
    > enumerating CocoaPods `target_integrator.rb` files under the
    > `/Library/Ruby/Gems` folder or enumerates all `.xcodeproj` folders
    > under a given directory. XCSSET then downloads a script and Mach-O
    > file into the Xcode project folder.

Tactics have an ID number `TAxxxx`, techniques have an ID number `Txxxx`
and sub-techniques (which may or may not exist for a technique) have an
ID number `Txxxx.xxx`. Tactics and technique/sub-technique are objects
in the Mitre Att\&ck data model. Procedures do not have a separate
ID-number, they are just optional attributes of the
technique/sub-technique object.

#### All the Tactics

From [Enterprise tactics](https://attack.mitre.org/tactics/enterprise)
we can read that the tactics are described as:

1.  Reconnaissance: *The adversary is trying to gather information they
    can use to plan future operations.*

2.  Resource Development: *The adversary is trying to establish
    resources they can use to support operations.*

3.  Initial Access: *The adversary is trying to get into your network.*

4.  Execution: *The adversary is trying to run malicious code.*

5.  Persistence: *The adversary is trying to maintain their foothold.*

6.  Privilege Escalation: *The adversary is trying to gain higher-level
    permissions.*

7.  Defense Evasion: *The adversary is trying to avoid being detected.*

8.  Credential Access: *The adversary is trying to steal account names
    and passwords.*

9.  Discovery: *The adversary is trying to figure out your environment.*

10. Lateral Movement: *The adversary is trying to move through your
    environment.*

11. Collection: *The adversary is trying to gather data of interest to
    their goal.*

12. Command and Control: *The adversary is trying to communicate with
    compromised systems to control them.*

13. Exfiltration: *The adversary is trying to steal data.*

14. Impact: *The adversary is trying to manipulate, interrupt, or
    destroy your systems and data.*

#### Att\&ck Model

Mitre Att\&ck Data Model in
figure [10.3](#fig:attack:9bf88223329c4329a4aa7e8660cfe8fd).

![Mitre Att\&ck Data
Model.<span label="fig:attack:9bf88223329c4329a4aa7e8660cfe8fd"></span>](/home/erikhje/office/kurs/secsrv/10-attack/tex/../img/attack-model-generic.pdf)

The figure is a slightly modified version of figure 3 in Strom et. al. .
In addtion to the tactics, techniques/sub-techniques and procedures
(TTPs), we have four more objects in the model (note again that the
example texts are quotes copied directly from the [Mitre Att\&ck
knowledge base](https://attack.mitre.org)):

  - Adversary group  
    is a threat actor known by a common name in the information security
    community, as of March 2022 there are 129 adversary groups in Mitre
    Att\&ck, e.g. [G0032 Lazarus
    group](https://attack.mitre.org/groups/G0032):
    
    > Lazarus Group is a North Korean state-sponsored cyber threat group
    > that has been attributed to the Reconnaissance General Bureau. The
    > group has been active since at least 2009 and was reportedly
    > responsible for the November 2014 destructive wiper attack against
    > Sony Pictures Entertainment as part of a campaign named Operation
    > Blockbuster by Novetta. Malware used by Lazarus Group correlates
    > to other reported campaigns, including Operation Flame, Operation
    > 1Mission, Operation Troy, DarkSeoul, and Ten Days of Rain. North
    > Korean group definitions are known to have significant overlap,
    > and some security researchers report all North Korean
    > state-sponsored cyber activity under the name Lazarus Group
    > instead of tracking clusters or subgroups, such as Andariel,
    > APT37, APT38, and Kimsuky.

  - Software  
    is a tool or malware that is used to conduct behaviour modeled in
    the TTPs, as of March 2022 there are 637 software tools and malware
    in Mitre Att\&ck, e.g. [S0154 Cobalt
    Strike](https://attack.mitre.org/software/S0154):
    
    > Cobalt Strike is a commercial, full-featured, remote access tool
    > that bills itself as "adversary simulation software designed to
    > execute targeted attacks and emulate the post-exploitation actions
    > of advanced threat actors". Cobalt Strike’s interactive
    > post-exploit capabilities cover the full range of ATT\&CK tactics,
    > all executed within a single, integrated system. In addition to
    > its own capabilities, Cobalt Strike leverages the capabilities of
    > other well-known tools such as Metasploit and Mimikatz.

  - Mitigation  
    is a security concept or technology that can be used to protect
    against techniques/sub-techniques, as of March 2022 there are 43
    enterprise mitigations in Mitre Att\&ck, e.g. [M1032 Multi-factor
    Authentication](https://attack.mitre.org/mitigations/M1032/):
    
    > Use two or more pieces of evidence to authenticate to a system;
    > such as username and password in addition to a token from a
    > physical smart card or token generator.

  - Data source  
    (including the sub-category "Data Components") is a source of
    information related to techniques/sub-techniques that can be
    collected by sensors/logs, as of March 2022 there are 38 data
    sources in Mitre Att\&ck, e.g. [DS0028 Logon
    Session](https://attack.mitre.org/datasources/DS0028):
    
    > Logon occurring on a system or resource (local, domain, or cloud)
    > to which a user/device is gaining access after successful
    > authentication and authorizaton. Data Components are 1. *Logon
    > Session: Logon Session Creation*: Initial construction of a new
    > user logon session (ex: Windows EID 4624, /var/log/utmp, or
    > /var/log/wmtp) and 2. *Logon Session: Logon Session Metadata*:
    > Contextual data about a logon session, such as username, logon
    > type, access tokens (security context, user SIDs, logon
    > identifiers, and logon SID), and any activity associated within
    > it.

Similarly to TTPs, adversary groups have an ID number `Gxxxx`, software
have an ID number `Sxxxx`, mitigations have an ID number `Mxxxx` and
data sources have an ID number `DSxxxx`. To be precise, *data source* is
formally a required attribute in the technique-object, so it’s not an
object by itself in Mitre Att\&ck’s data model, but we treat it as a
separate object in our slightly modified version of the model to
simplify and visualize the relation between technique and data source.

#### Att\&ck Example

Mitre Att\&ck Model Example in
figure [10.4](#fig:attack:aa10a269b5194814a29c0fe7910fd0de).

![Mitre Att\&ck Model
Example.<span label="fig:attack:aa10a269b5194814a29c0fe7910fd0de"></span>](/home/erikhje/office/kurs/secsrv/10-attack/tex/../img/attack-model-example.pdf)

The Mitre Att\&ck model can represent attack behaviour with detailed
information. E.g. we can model a situation where adversary group APT32
uses the pass-the-ticket sub-technique to achieve the tactic lateral
movement. They do this by using the tool Mimikatz with the exact
procedure including the Mimikatz modules `LSADUMP::DCSync` and
`KERBEROS::PTT`. We can maybe prevent this with mitigations described in
M1026 "Privileges account management", and we can maybe detect the
behaviour with EventID 4769 or other events related to the data sources
active directory, logon sessions and user account. In other words, we
can see how these concepts relate to each other which makes it easier
for us to understand and learn about threats and how to think about
protecting our infrastructures.

#### Mitre CAPEC

When you browse the Mitre Att\&ck matric and view techniques, you will
see that sometimes a technique links to a CAPEC ID number, e.g. for the
sub-techniques "Boot or Logon Autostart Execution: Winlogon Helper DLL"
there is a link to CAPEC-579 which is "Replace Winlogon Helper DLL".
CAPEC is an acronym for *Common Attack Pattern Enumeration and
Classification*. Mitre describes [the differences between CAPEC and
Att\&ck](https://capec.mitre.org/about/attack_comparison.html) as the
following:

  - CAPEC  
    "CAPEC is focused on application security and describes the common
    attributes and techniques employed by adversaries to exploit known
    weaknesses in cyber-enabled capabilities. (e.g., SQL Injection, XSS,
    Session Fixation, Clickjacking)"

  - Att\&ck  
    "ATT\&CK is focused on network defense and describes the operational
    phases in an adversary’s lifecycle, pre and post-exploit (e.g.,
    Persistence, Lateral Movement, Exfiltration), and details the
    specific tactics, techniques, and procedures (TTPs) that advanced
    persistent threats (APT) use to execute their objectives while
    targeting, compromising, and operating inside a network."

Att\&ck is for us, we work with infrastructure and security, while CAPEC
is for developers who work with application security. Sometimes a CAPEC
attack pattern is used in an Att\&ck technique/sub-technique, so they
are related, but serve different purposes.

#### Critique

Mitre Att\&ck is very useful and wellknown in industry. The best use of
Mitre Att\&ck is as a common language: we can talk about the same attack
techniques by referring to technique IDs, we can view it as a menu
attackers will choose a set of elements from and combine them into an
attack. It is very good for learning the big picture of all kinds of
attack behaviour that we need to be aware of, and at the same time
realize that attack behaviour is many times the same as normal behaviour
(e.g. checking your IP address). Mitre Att\&ck helps us focus on the
techniques instead of signatures and exact procedures, and this is
important for the security industry: we need to detect higher-level
behaviour more than just low-level detailed signatures (it is "easy" for
attackers to bypass signature detection like standard antivirus
software).

Mitre Att\&ck is not a checklist you should use to state that you have
e.g. 90% protection since you have run a test suite that tests
everything in the matrix. Threat actors are more advanced than what you
are able to compose yourself in the Att\&ck matrix. It is a good
starting point, but be aware that attackers do more than just the
techniques listed in the matrix. And even if we try to detect the
behaviour described in the techniques, be aware that this is very hard
to do in practice. In other words, Mitre Att\&ck is great for learning,
getting an overview and as a common language, but be aware of the limits
of the model (same thing for all models, they do have limits).

### Using Att\&ck Navigator

Difficulty Levels in
figure [10.5](#fig:attack:40d41907a49448caa5eaa2edac42ae1f).

![Difficulty
Levels.<span label="fig:attack:40d41907a49448caa5eaa2edac42ae1f"></span>](/home/erikhje/office/kurs/secsrv/10-attack/tex/../img/mitre-teach.pdf)

Mitre Att\&ck comes with a very nice tool called the [Attack
Navigator](https://mitre-attack.github.io/attack-navigator) which we can
you to browse the matrix and explore the data model. This can also be a
bit overwhelming. Where do we start to learn some of this? Fortunately
Travis Smith, a principal security researcher at a company called
Tripwire, has done the very useful exercise of categorizing the
techniques into difficulty levels for us. The colors of the matrix
represents (quote from
[TravisFSmith/mitre\_attack](https://github.com/TravisFSmith/mitre_attack)):

1.  *Blue* These are techniques which are not really exploitable, rather
    they use other techniques to be viable.

2.  *Green* These are the easiest techniques to exploit, there is no
    need for POC malware, scripts, or other tools.

3.  *Yellow* These techniques usually need some sort of tool, such as
    Metasploit.

4.  *Orange* These techniques require some level of infrastructure to
    setup. Once setup, some are easy and some are more advanced.

5.  *Red* These are the most advanced techniques which require an
    in-depth understanding of the OS or custom DLL/EXE files for
    exploitation.

6.  *Purple* These are high level techniques which include
    sub-techniques of varying levels.

7.  (White are not categorized as they have appeared after Travis did
    his work)

## Testing and Analysis

### Atomic Red Team

Atomic Red Team in
figure [10.6](#fig:attack:3b962435d66a45d487dbf90ed9ab2d5c).

![Atomic Red
Team.<span label="fig:attack:3b962435d66a45d487dbf90ed9ab2d5c"></span>](/home/erikhje/office/kurs/secsrv/10-attack/tex/../img/atomic.png)

[Atomic Red Team](https://atomicredteam.io) is a collection of tests
that we can use to try the attack behaviour defined in the
techniques/sub-techniques in the Mitre Att\&ck matrix. Note that these
are not "exploits" by themselves, but they do try to make changes to our
system, so we need to be careful when we try them. The benefit and the
reason why we should be aware of this collection of tests is that it is
created based on the Mitre Att\&ck and therefore the tests map directly
to each technique/sub-technique. Atomic Red Team has two components: it
is a collection of tests, and it is an execution framework. We will of
course use the PowerShell framework, so we can execute the tests with
the cmdlet `Invoke-AtomicTest`

### BloodHound

BloodHound in
figure [10.8](#fig:attack:147b5a21efcc414b996aacbd08f20afc).

![BloodHound.<span label="fig:attack:147b5a21efcc414b996aacbd08f20afc"></span>](/home/erikhje/office/kurs/secsrv/10-attack/tex/../img/goodblood.png)
![BloodHound.<span label="fig:attack:147b5a21efcc414b996aacbd08f20afc"></span>](/home/erikhje/office/kurs/secsrv/10-attack/tex/../img/bloodhound-mitre.pdf)

BloodHound  is popular analysis tool for active directory environments.
To use BloodHound for analysis, you will first run SharpHound which
collects data from active directory, and generates json-files zipped
into an archive. BloodHound can load this zip-archive and will then draw
a graph of your active directory environment with *nodes* and *edges*.
Nodes are one of users, groups, computers, domains, GPOs and OUs (if
used in an Azure-environment even more nodes are supported, e.g.
AzTenant). The set of possible edges is much larger that the set of
possible nodes. Some examples of edges are MemberOf, GPLink, Contains,
WriteDACL, Owns, CanRDP and CanPSRemote. You can see all of them in [the
BloodHound
documentation](https://bloodhound.readthedocs.io/en/latest/data-analysis/edges.html).

### Other Tools

There is a plethora of tools available for security analysis when it
comes to penetration testing. Two tools that the author know are in use
by leading security companies are [Burp suite]() and [Nessus](). But
these are just two examples and most professionals working with security
analysis have their own toolbox which might include Nessus and/or Burp,
but search the internet for cheat sheets to see what else is in their
toolboxes, e.g.
[Pentest-Cheat-Sheets](https://github.com/Kitsun3Sec/Pentest-Cheat-Sheets).

## Review questions and problems

1.  Name and describe at least five of the Mitre Att\&ck Tactics.

2.  Visit the [Attack
    Navigator](https://mitre-attack.github.io/attack-navigator), create
    a "New empty layer", choose "Enterprise". Use the coloured version
    of the Mitre Att\&ck Matrix in the chapter, choose at least one blue
    and one green technique, right click on them and find examples of
    exact procedures of how they have been used.

## Lab tutorials

1.  **Atomic Red Teams.** Log into DC1 as domain administrator. This lab
    follows [the documentation in the
    Wiki](https://github.com/redcanaryco/invoke-atomicredteam/wiki).
    Let’s see if we can test for technique T1047 "Windows Management
    Instrumentation" which belongs to tactic TA0002 Execution.
    
        Uninstall-WindowsFeature Windows-Defender
        Restart-Computer -Force
          
        IEX (IWR 'https://raw.githubusercontent.com/redcanaryco/
                  invoke-atomicredteam/master/install-atomicredteam.ps1'
                  -UseBasicParsing);
        Install-AtomicRedTeam -getAtomics
        
        # Import the module (maybe this is not needed)
        Import-Module `
          "C:\AtomicRedTeam\invoke-atomicredteam\Invoke-AtomicRedTeam.psd1" `
          -Force
        
        # List all available tests with the Att&ck ID number
        Invoke-AtomicTest All -ShowDetailsBrief
        
        # Show info for all tests in technique T1047
        # READ THE OUTPUT FROM THIS COMMAND.
        Invoke-AtomicTest T1047 -ShowDetailsBrief
        
        # Check prereqs for T1047
        Invoke-AtomicTest T1047 -CheckPrereqs
        
        # Eight tests seems ok, last two needs prereqs
        Invoke-AtomicTest T1047 -GetPrereqs
        
        # Run tests for local and remote execution
        Invoke-AtomicTest T1047 -TestNumbers 5,6
        
        # Run all ten tests for WMI
        Invoke-AtomicTest T1047
        
        # MAKE SURE YOU READ AND STUDY THE OUTPUT GENERATED
        
        # Cleanup
        Invoke-AtomicTest T1047 -Cleanup

2.  **BloodHound.** Let’s do some reconnaissance. This lab assumes you
    start with your infrastructure with a fresh domain install, meaning
    the way it looks after chapter six (AD installed and all hosts
    joined to the domain, but no OUs or users/groups created yet).
    
    1.  Login as local Admin on SRV1 and start an elevated PowerShell
        (“PowerShell as administrator”)
        
            # Install neo4j (includes java), git and wget
            choco install -y neo4j-community git wget 7zip
            
            # Close and reopen PowerShell as administrator, 
            # download BloodHound-repo
            cd $home
            git clone https://github.com/BloodHoundAD/BloodHound.git
        
        Visit localhost:7474 in a browser, login: username/password is
        `neo4j`/`neo4j`, you will be asked to change password, remember
        to write it down.
    
    2.  Let’s start BloodHound and see that we can log in with the same
        username and password.
        
            cd $home
            wget  https://github.com/BloodHoundAD/BloodHound/releases/
                  download/4.1.0/BloodHound-win32-x64.zip
            7z x .\BloodHound-win32-x64.zip
            .\BloodHound-win32-x64\BloodHound.exe
    
    3.  To gather data for BloohHound we need some tools that Windows
        will think is malware, but we know what we are doing (I hope) so
        let’s remove Windows Defender and gather data
        
            Uninstall-WindowsFeature Windows-Defender
            Restart-Computer -Force
            # login as Domain Administrator
            cd \Users\Admin\BloodHound\Collectors
            .\SharpHound.exe
        
        This should have been possible to do without being a domain user
        with something like (the author have not tried to troubleshoot
        why this does not work)
        
            #.\SharpHound.exe 
            # --ldapusername "SEC\mysil"
            # --ldappassword "Pa$$w0rdMy"
            # --disablekerberossigning
            # --Domain sec.core
            # --Domaincontroller dc1.sec.core
    
    4.  What does our domain look like? Log out and log back in as local
        Admin, then do
        
            cd $home
            .\BloodHound-win32-x64\BloodHound.exe
            # Log in
            # "Upload data" (4th button from the top in right side)
            # choose the zip file created by SharpHound
        
        Run the query "Find Shortest Paths to Domain Admins" and you
        should see something like the screenshot from BloodHound in the
        chapter text.
    
    5.  Log in as domain administrator and run the following commands
        (change values if your users are different from these):
        
            Add-ADGroupMember -Identity 'Enterprise admins' -Members felix
        
        Run SharpHound again and redo the previous item with uploading
        data and "Find Shortest Paths to Domain Admins". Notice how
        BloodHound shows you that `felix` is now a path to domain
        administrator.

# Security: Defenses

## Mitre D3fend

Mitre D3fend 0.10.0-BETA-2 in
figure [11.1](#fig:defense:b6f4a4b7de66499b8f527baa8fbd4d96).

![Mitre D3fend
0.10.0-BETA-2.<span label="fig:defense:b6f4a4b7de66499b8f527baa8fbd4d96"></span>](/home/erikhje/office/kurs/secsrv/11-defense/tex/../img/defend.pdf)

Mitre D3fend  is an ongoing research effort to produce a countermeasure
knowledge base that describes tactics used to defend against attacks in
practice. D3fend can be used to link against the TTPs (Tactics,
Techniques and Procedures) of Mitre Att\&ck.

#### All the Tactics

From [D3FEND: A knowledge graph of cybersecurity
countermeasures](https://d3fend.mitre.org) we can read that the tactics
are described as:

1.  Harden: *The harden tactic is used to increase the opportunity cost
    of computer network exploitation. Hardening differs from Detection
    in that it generally is conducted before a system is online and
    operational.*

2.  Detect: *The detect tactic is used to identify adversary access to
    or unauthorized activity on computer networks.*

3.  Isolate: *The isolate tactic creates logical or physical barriers in
    a system which reduces opportunities for adversaries to create
    further accesses.*

4.  Deceive: *The deceive tactic is used to advertise, entice, and allow
    potential attackers access to an observed or controlled
    environment.*

5.  Evict: *The eviction tactic is used to remove an adversary from a
    computer network.*

Maybe you can remember H-DIDE as an acronym? (Harden, Detect, Isolate,
Deceive, Evict).

## Hardening

Trying to have secure Windows clients and servers is not some kind of
magic. It is realizing that we can never be 100% protected, but trying
to protect the best we can, and be prepared when incidents happen. We
have already stated that the basics are

  - Backup and restore

  - Identity management and access control

  - Configuration management

  - Software package management

  - Logging and monitoring

  - Fast (automatic) redeploy/installation/provisioning

The process of configuring a server or client to be as secure as
possible is called *hardening* ("herding" in Norwegian). Related to our
list of basics, hardening is mostly about configuration management, but
this touches of course also access control, software package management
and logging. A good start is to work with the Microsoft Security
Baselines which we covered in chapter seven. In addition, one should
read official documentation and recommendations like [Best Practices for
Securing Active
Directory](https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/security-best-practices/best-practices-for-securing-active-directory).

### Defender

Defender in
figure [11.2](#fig:defense:bb907e0b32d84df0a3317dac187eec50).

![Defender.<span label="fig:defense:bb907e0b32d84df0a3317dac187eec50"></span>](/home/erikhje/office/kurs/secsrv/11-defense/tex/../img/defender.png)

As part of a hardening process, we should configure some anti-malware
software like Microsoft Defender. Defender can detect malware and stop
us from installing software that it suspect might be used for malicous
purposes (we will ses this in the lab exercises). We can interact with
Defender from PowerShell of course:  
`$Preferences = Get-MpPreference`  
`Preferences.ScanScheduleDay`  
If this says `0` [it means "every
day"](https://docs.microsoft.com/en-us/previous-versions/windows/desktop/legacy/dn455323\(v=vs.85\)),
meaning the host will scan for malware every day (as opposed to only one
of the seven days of the week).

## Review questions and problems

1.  Name and describe all the five of the tactics in Mitre D3fend.

## Lab tutorials

1.  No lab tutorials this week.

# Infrastructure Orchestration

## Evolution

Evolution in figure [12.1](#fig:orch:bac92f0a88f64bbbaa46cdbf9763c0c6).

![Evolution.<span label="fig:orch:bac92f0a88f64bbbaa46cdbf9763c0c6"></span>](/home/erikhje/office/kurs/secsrv/12-orch/tex/../img/orch-evol.pdf)

The simplest way to use a public or private cloud is to log in to a GUI
(like OpenStack Horizon that we have used throughout this course) and
create resources (keypair, network, router, server, etc) manually by
point-and-click and maybe provide some additional input values every now
and then. Such manual tasks are easy to do, but takes time and are
error-prone. Always remember: *humans make errors, computers do not
(unless we program them to)*. So we should move to a command line
interface (CLI) since command lines can be more easily documented and
command line interface allows for scripting.

Since we all love scripting in Bash and PowerShell, we would then start
putting all the openstack-commands into a script with additional
variables, loops and conditions. After doing this for a while, you would
see that this also creates its own set of problems: what if a command
fails? you need to deal with all situation where something can fail. You
need to have separate functionality for creating and deleting resources.
If you try to write such a script, you will quickly realize that there
must be a better way. And there is. While regular programming and
scripting is what we call *imperative programming*, we should turn to
what we call *declarative programming*. Imperative programming means to
focus on how to do something, while declarative programming means to
focus on the end state that we want to reach.

When professionals use public or private clouds to create
infrastructures, they use a declarative domain-specific language and
write "code" that describes the infrastructure that want to create. We
say *domain-specific language* because we are talking about a
"programming language" that is created for a specific purpose, as
opposed to a generic programming language like C or C++.

Infrastructure orchestration is a subtopic of *Infrastructure as Code
(IaC)*. In Infrastructure as Code we always try to keep to following two
principles in mind :

  - Repeatability  
    Recreating (deleting and creating) the same infrastructure or
    creating variations of the same basic infrastructure ("reusability")
    should be fast and easy.

  - Consistency  
    Whenever we create an infrastructure the same things should happen:
    the end result (the "end state") should always be the same, there
    should be nu surprises.

Since this is a course in "Secure Core Services", these principles are
particularly important to us since we need the ability to recreate
services fast when incidents occur.

## Orchestration Tools

Orchestration Tools in
figure [\[fig:orch:70fcf501408c418c9452d3f1f63a00a8\]](#fig:orch:70fcf501408c418c9452d3f1f63a00a8).

  - OpenStack: Heat (templates in yaml)

  - Hashicorp: Terraform (HashiCorp Configuration Language (HCL))

  - Amazon: Cloudformation (templates in yaml/json)

  - Azure: Azure Resource Manager, (ARM templates in json)

  - Google Compute Engine: Cloud Deployment Manager (templates in yaml)

  - ...

*YAML* and *JSON*

Since we use OpenStack, we will use OpenStack declarative
domain-specific orchestration language/tool which is called Heat. Each
of the public cloud providers Google, Azure and Amazon have their own
similar languages/tools. Heat was originally a "clone" of Amazon
Cloudformation. All these languages/tools are quite similar and try to
do the same things, so when you know one of them you can easily switch
to the other. Terraform is a bit different from the others since it is
meant to be cross-platform, meaning if you want to write infrastructure
code for deploying on e.g. both Azure and Amazon, Terraform is probably
a good choice. Terraform has its own language called HCL, while most
other tools use a language based on YAML or JSON:

  - YAML  
    is short for "YAML Ain’t Markup Language". It is a human-friendly
    (meaning "easy-to-read") language for storing data. The most
    important property to be aware of with YAML is that it is based on
    *indentation*. The main page [yaml.org](https://yaml.org) is valid
    YAML itself. If you want to look up exact details of YAML you can
    look in the [specification](https://yaml.org/spec/1.2.2/), but its
    probably better to browse through a cheat sheet like [YAML
    cheatsheet](https://quickref.me/yaml) (where you can also see how
    YAML maps to JSON).

  - JSON  
    is short for "JavaScript Object Notation" and is similar to YAML but
    is more focused on data interchange than readability. While YAML is
    based on indentation, JSON is based on enclosing everything in
    blocks with braces (`{` and `}`) similar to what you know from C and
    C++ programming. If you want to look up exact details of JSON you
    can look in the
    [reference](https://www.ecma-international.org/publications-and-standards/standards/ecma-404/),
    but its probably better to browse through a cheat sheet like [JSON
    cheatsheet](https://quickref.me/json).

In general, we can say that YAML is used when readability is important,
while JSON is used when speed/performance is important. System
administrators tend to prefer YAML while programmers tend to prefer
JSON.

## OpenStack Heat Basics

Heat Template Syntax in
figure [\[fig:orch:259974fec20c428baea1fc82ba69ffa1\]](#fig:orch:259974fec20c428baea1fc82ba69ffa1).

``` 
  heat_template_version: ...

  description: >
    HOT template to create ...
  
  parameter_groups:
    ...

  parameters:
    ...

  resources:
    ...

  outputs:  
    ...
```

A Heat template  starts with a version number, which is a date that
corresponds to the date of an OpenStack release (which happens every six
months). The version number can optionally be followed by a description,
which typically uses the folded multiline YAML construction that starts
with the character `>` ("folded" means that any newline that follows are
replaced with a space). The parameter\_groups is optional and not widely
used, but it allows for grouping of parameters which can be useful for
application that interface with Heat. *Parameters* is where you define
all the input parameters to you template, e.g. which version of server
your stack should use, or which ssh keypair to use. An important feature
of these parameters is that you can *set default values*, thereby
creating flexible templates that are still easy to use. With default
values the template can be used without providing values for the
parameters, so it is easy to get going with using the template.

The most important section of the template is the *resources* part. This
is what you can think of as "the actual code" of the template. In this
part you describe all the resources that make up your stack (remember:
"stack" is the same as an "infrastructure" in this context). A resource 
is anything you can create in OpenStack, e.g. `OS::Nova::Server` or
`OS::Neutron::Router`.

Finally the outputs section is used to define information that should be
available after a stack has been created, e.g. IP-addresses of servers,
URLs to web applications, or other values that have been created as part
of the stack.

Study [the template we used in the beginning of the
course](https://gitlab.com/erikhje/heat-mono/-/blob/master/single_windows_server.yaml).

## OpenStack Heat Advanced

Advanced Topics in
figure [\[fig:orch:7290b983572049dea839ccef2d31ed8c\]](#fig:orch:7290b983572049dea839ccef2d31ed8c).

  - Resources dependencies

  - Conditions

  - Iteration

  - Boot scripts

  - Nested stacks

A Heat template is what we call an *configuration definition file* .
When we start working with this template we will see that we will start
looking for more functionality. It is important to realize that there is
a clear difference between domain specific languages like Heat and real
programming languages. A Heat template is a configuration definition
file (we can also refer to it as an infrastructure definition file), it
is not program source code like a C-file. However, *it is OK to refer to
configuration definition files as "code" since we should follow the same
process as programmers do when we write or modify them: using tools for
catching errors and checking quality, and storing our "code" in
git-repositories*. Heat is not a generic purpose programming language,
but it does have some features that makes it look like a programming
language, and some useful features that will solve some typical problems
we run into:

  - Resources dependencies  
    study the use of `depends_on` in the file
    [iac\_top.yaml](https://github.com/githubgossin/IaC-heat/blob/master/iac_top.yaml)

  - Conditions  
    Heat supports creating
    [conditions](https://docs.openstack.org/heat/latest/template_guide/hot_spec.html#conditions-section)
    based on the input parameters provided to the stack, and then use
    this/these condition(s) to dynamically create resources or make
    other changes to the stack.

  - Iteration  
    study the use of `OS::Heat::ResourceGroup`, `count` and
    `server_name` in
    [iac\_rest.yaml](https://github.com/githubgossin/IaC-heat/blob/master/iac_rest.yaml).
    Heat also have support for the function
    [repeat](https://docs.openstack.org/heat/latest/template_guide/hot_spec.html#repeat)
    to enable iteration over lists.

  - Boot scripts  
    study the `user_data` section in
    [cl\_dc\_srv\_basic.yaml](https://gitlab.com/erikhje/heat-mono/-/blob/master/cl_dc_srv_basic.yaml)
    and also how the boot script can be an external file in
    [managed\_windows\_server.yaml](https://github.com/githubgossin/IaC-heat/blob/master/lib/managed_windows_server.yaml)

  - Nested stacks  
    study the file structure in the git-repo
    [IaC-heat](https://github.com/githubgossin/IaC-heat)

## Review questions and problems

1.  Describe the Heat component of OpenStack (in other words: What is
    OpenStack Heat?).

2.  Briefly describe the syntax of a Heat template.

3.  How does a Heat resource retrieve/make use of a value passed as a
    parameter to the template? How does a Heat resource reference
    another Heat resource?

4.  Download [the template we used in the beginning of the
    course](https://gitlab.com/erikhje/heat-mono/-/blob/master/single_windows_server.yaml).
    Study how a boot script is used to change hostname in
    [cl\_dc\_srv\_basic.yaml](https://gitlab.com/erikhje/heat-mono/-/blob/master/cl_dc_srv_basic.yaml)
    and modify the template you have downloaded in such a way that it
    will install PowerShell Core and SysInternals in a boot script.

5.  (**KEY PROBLEM**) After you have completed this chapters lab
    tutorial, download and modify the template
    [servers\_in\_new\_neutron\_net.yaml](https://github.com/openstack/heat-templates/blob/master/hot/servers_in_new_neutron_net.yaml)
    in such a way that the two servers in the template can be different
    (e.g. an Ubuntu and a Windows instance). You can do this by changing
    the parameter list and the corresponding references to the
    parameters in the resources. Also add a security group to both
    servers, use the security group you find in [the template we used in
    the beginning of the
    course](https://gitlab.com/erikhje/heat-mono/-/blob/master/single_windows_server.yaml).
    Verify that the template works after you have modified it by
    creating a stack. Tip: remember from the lab tutorial that you can
    get information about what is failing in your stack with  
    `openstack stack event list STACK_NAME --nested-depth 3`

## Lab tutorials

1.  **OpenStack CLI**. Install the OpenStack command line client (do
    this on your laptop or any other host you prefer to use, it is
    preinstalled on login.stud.ntnu.no if you prefer to use that
    server):
    
      - [Instructions for
        Windows](https://www.ntnu.no/wiki/display/skyhigh/Openstack+CLI+on+Windows)
    
      - [Instructions for Linux using login.stud.ntnu.no or other Linux
        host](https://www.ntnu.no/wiki/display/skyhigh/Using+the+commandline+clients)
        (if installing on other Linux host do  
        `sudo apt install python3-openstackclient python3-heatclient`).
        You only have to read and do the first part of this page, stop
        when you see the headline "Creating an initial network topology"
    
    Also read about [Authentication
    alternatives](https://www.ntnu.no/wiki/display/skyhigh/Authentication+alternatives)
    (remember that we used [Application
    credentials](https://www.ntnu.no/wiki/display/skyhigh/Application+credentials)
    when we set up the backup system with `restic`). Note that if you
    use Application credentials created from Horizon (like we did with
    backups) you have to check the box "Unrestricted (dangerous)". When
    you have completed your setup test that it works with the command:  
    `openstack flavor list`

2.  **Create and delete a stack using CLI**. Download the Heat template
    [Single Windows
    Client](https://gitlab.com/erikhje/heat-mono/-/blob/master/single_windows_client.yaml),
    create the stack from the template, view that it has been created in
    Horizon (the OpenStack web-interface), finally delete the stack.
    
    ``` 
      openstack stack create -t single_windows_client.yaml \
        --parameter key_name=dcsg1005 mysilstack
      # view in GUI and you can also verify with
      openstack stack list
      # finally delete the stack
      openstack stack delete mysilstack
    ```

3.  **Template from developers and environment file**. Create a new
    network with two Ubuntu instances by using the Heat template
    `servers_in_new_neutron_net.yaml` from (remember to always examine
    code that you reuse, but this is from the repo of the OpenStack Heat
    developers so this is probably good code, and reusing good code is a
    best practice we should enforce)
    [openstack/heat-templates](https://github.com/openstack/heat-templates/blob/master/hot)
    
        openstack stack create -t servers_in_new_neutron_net.yaml \
         -e heat_demo_env.yaml heat_demo
    
    Create an environment file `heat_demo_env.yaml` that looks like this
    (the environment file is for enforcing a good computer science
    principle: *separating code and data*)
    
        parameters:
          key_name: KEY_NAME
          image: Ubuntu Server 20.04 LTS (Focal Fossa) amd64
          flavor: m1.small
          public_net: ntnu-internal
          private_net_name: net1
          private_net_cidr: 192.168.125.0/24
          private_net_gateway: 192.168.125.1
          private_net_pool_start: 192.168.125.200
          private_net_pool_end: 192.168.125.250
    
    If you get an error immediately when trying to create a stack you
    probably have a syntax error (e.g. your Yaml file does not have
    correct indentation or similar). If syntax is OK, but the stack
    fails to create e.g. `CREATE_FAILED` message or similar, try
    something like  
    `openstack stack event list heat_demo --nested-depth 3`

<!-- end list -->

1.  SkyHiGh was born in 2011 as a cloud ("Sky") at Høgskolen i Gjøvik
    (HiG).

2.  Note: Process Explorer is widely used, see [the discovery of this
    bug](https://bugs.chromium.org/p/chromium/issues/detail?id=1254631)
    (rewarded $3000).

3.  Maybe you can get it to work? See the exercises, maybe try the
    `Network Service` account with added seBackupPrivilege? You will
    receive an award if you are able to solve it.

4.  Note that this exact example would be most efficiently solved with
    `Get-ChildItem -Recurse -File -Filter ’*l??’` but efficiency is not
    the point here.

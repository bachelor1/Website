---
custom-date: 11/04/2022
---

The latest lecture has now been uploaded to the lectures folder. Those of you who couldn't be there yesterday better watch it before the next lab or you'll be sorry!


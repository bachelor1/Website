#!/bin/sh

# Get baseurl and announcement directory from _config.yml
baseurl=($(grep '^baseurl: ' _config.yml | awk '{ print $2 }' | cut -d '"' -f 2))
announce_dir=($(grep '^announcedir: ' _config.yml | awk '{ print $2 }' | cut -d '"' -f 2))

# $1 - Path
# $2 - True when currently in _posts or a sub-directory of _posts, else false
# $3 - Effective web path - same as path, but ignores _posts and its sub-directories

recurse() {
    # Get names of all files with .md or .markdown extension
    files=($(ls -l $1 | grep -E '^-.*(.md|.markdown)$' | grep -o '[^ ]*$'))

    for file in ${files[@]}; do
        # Remove file extension from file name
        fileurl=($(echo $file | grep -o '^[^.]*'))

        # Set path to lower case to match generated web paths
        if [[ -n $3 ]]; then
            filepath=($(echo "$3/$fileurl" | awk '{print tolower($0)}'))
        else
            filepath=($(echo "$fileurl" | awk '{print tolower($0)}'))
        fi

        # Replace dashes and underscores with spaces
        filename=($(echo $fileurl | tr '-' ' ' | tr '_' ' '))
        
        # Create html list item with capitalized filename
        file_link="$baseurl/content/$filepath"
        html+=$"<li><a href=\"$file_link\">${filename[@]^}</a></li>"
        echo "<li><a href=\"$file_link\">${filename[@]^}</a></li>"
        html+=$'\n'
    done

    # Get names of all directories
    directories=($(ls -l $1 | grep '^d' | grep -o '[^ ]*$' | tr -d '/'))

    for dir in ${directories[@]}
    do
        # Skips printing of _posts folders and their sub-directories, only prints contents
        if [[ $dir == '_posts' ]] || [[ $2 == true ]]; then
            recurse "$1/$dir" true "$3"
        else 
            if [[ $dir != $announce_dir ]] && [[ $dir != "nav" ]]; then
                # Stitch path
                if [[ -n $3 ]]; then
                    path="$3/$dir"
                else
                    path="$dir"
                fi

                # Create html list item for directory
                nav_link=($(echo "$path-page" | tr '/' '-'))
                html+="<li><a class=\"dir\" href=\"$baseurl/content/nav/$nav_link\">$dir</li>"
                html+=$'\n'

                # Recurse
                html+=$'<ul>\n'
                recurse "$1/$dir" false $path
                html+=$'</ul>\n'
            fi
        fi
    done
}

echo "Creating dynamic table of contents for index page..."

html=$'<div>\n<ul class="index-toplevel-ul">\n'
recurse 'webpage/content' false
html+=$'</ul>\n</div>'

echo "$html" > webpage/_includes/index-toc.html
echo "Success!"
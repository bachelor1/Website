#!/bin/sh

echo "Fetching announcements..."

# Get baseurl and announcement directory from _config.yml
baseurl=($(grep '^baseurl: ' _config.yml | awk '{ print $2 }' | cut -d '"' -f 2))
announce_dir=($(grep '^announcedir: ' _config.yml | awk '{ print $2 }' | cut -d '"' -f 2))

files=($(find content/$announce_dir | grep -oE "$announce_dir/.*(md|markdown)" | grep -o "^[^.]*"))

html=""

for file in ${files[@]}; do
    filename=($(echo $file | grep -o "[^/]*$" | grep -o "^[^.]*" | tr "_" " "))

    # Create html list item with capitalized filename
    html+=$"<li><a href=\"$baseurl/content/$file\">${filename[@]^}</a></li>"
    html+=$'\n'

done

echo "$html" > webpage/_includes/announcements.html

echo "Success!"
#!/bin/bash

echo "Shaping sidenav..."

# Make nav folder if it doesn't already exist
[ ! -d content/nav ] && mkdir content/nav
# Remove all contents from existing nav folder
rm -rf content/nav/*

# Get baseurl and announcement directory from _config.yml
baseurl=($(grep '^baseurl: ' _config.yml | awk '{ print $2 }' | cut -d '"' -f 2))
announce_dir=($(grep '^announcedir: ' _config.yml | awk '{ print $2 }' | cut -d '"' -f 2))

# 1 - directory name
# 2 - directory name w/ path
# 3 - formatted page name
create-nav() {
    # Set post title
    nav_page_title=($(echo $1 | tr '-' ' ' | tr '_' ' '))
    nav_page_content=$'---\n'
    nav_page_content+="title: ${nav_page_title[@]^}"
    nav_page_content+=$'\n---\n'

    # DIRECTORIES
    directories=($(ls -l content/$2 | grep '^d' | grep -o '[^ ]*$'))

    for dir2 in ${directories[@]}
    do
        if [[ $dir2 != $announce_dir ]] && [[ $dir2 != "nav" ]]; then
            dirname=($(echo $dir2 | tr '-' ' ' | tr '_' ' '))
            nav_page_content+="<i class=\"fa fa-folder\" aria-hidden=\"true\"></i>[ ${dirname[@]^}]($baseurl/content/nav/$3-$dir2-page)<br>"
            nav_page_content+=$'\n'
        fi
    done

    # FILES
    files=($(ls -l content/$2 | grep -E '^-.*(.md|.markdown)$' | grep -o '[^ ]*$' | grep -o '^[^\.]*'))
    
    for file in ${files[@]}; do
        fileurl=($(echo "$2/$file" | awk '{print tolower($0)}'))
        filename=($(echo $file | tr '-' ' '))
        nav_page_content+="[${filename[@]^}]($baseurl/content/$fileurl)<br>"
        nav_page_content+=$'\n'
    done

    # Save nav page markdown file
    echo "$nav_page_content" > content/nav/$3-page.md
}

# 1 - directory name
# 2 - directory name w/ path
recurse() {
    directories=($(ls -l content/$2 | grep '^d' | grep -o '[^ ]*$' | grep -o '[^/]*'))

    for dir in ${directories[@]}
    do 
        if [[ $dir != $announce_dir ]] && [[ $dir != "nav" ]]; then
            # Set directory path; ignores preceding slash for direct sub-directories of /content/
            if [[ -n $2 ]]; then
                path="$2/$dir"
            else
                path="$dir"
            fi

            # Set name of nav page 
            # E.g. content/category/this-dir -> category-this-dir
            name=($(echo $path | tr '/' '-'))

            # Create navigation page for directory
            create-nav $dir $path $name

            # Recurse into directory
            recurse $dir $path
        fi
    done
}

# SIDENAV
html=$'<div class="sidenav">\n'

# Create sidenav entries with links for all direct sub-directories of /content/
directories=($(ls -l content | grep '^d' | grep -o '[^ ]*$' | grep -o '[^/]*'))
for dir in ${directories[@]}
do 
    if [[ $dir != $announce_dir ]] && [[ $dir != "nav" ]]; then
        sidenav_entry_text=($(echo $dir | tr '-' ' ' | tr '_' ' '))

        html+="<a href=\"$baseurl/content/nav/$dir-page\">"
        html+=${sidenav_entry_text[@]^}
        html+=$'</a>\n'
    fi
done

html+=$'</div>'

# Save sidenav html
echo "$html" > webpage/_includes/sidenav.html

# Recurse and create nav pages for all directories
recurse

echo "Success!"
